package com.padel.eproject;

import com.padel.eproject.EmailMessage;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class EServicesApplication {
	@Value("${gmail.username}")
	private String username;

	@Value("${gmail.password}")
	private String password;

	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(EServicesApplication.class, args);
	}

	public void sendmail(EmailMessage emailmessage) throws AddressException, MessagingException, IOException {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		//Session session = Session.getInstance(props, new javax.mail.Authenticator(this, emailmessage));
		//Session session = Session.getInstance(props, (Authenticator) new Object());
		
		Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(emailmessage.getSenderEmail(), emailmessage.getSenderPassword());
                    }
                });
		
		MimeMessage mimeMessage = new MimeMessage(session);
		mimeMessage.setFrom((Address) new InternetAddress(this.username, false));
		mimeMessage.setRecipients(Message.RecipientType.TO,
				(Address[]) InternetAddress.parse(emailmessage.getTo_address()));
		mimeMessage.setSubject(emailmessage.getSubject());
		mimeMessage.setContent(emailmessage.getBody(), "text/html");
		mimeMessage.setSentDate(new Date());
		Transport.send((Message) mimeMessage);
	}
}
