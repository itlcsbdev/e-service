package com.padel.eproject;

import com.padel.eproject.EmailController;
import com.padel.eproject.EmailMessage;
import com.padel.eproject.EServicesApplication;
import java.io.IOException;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = {"http://localhost:4200"}, maxAge = 3600L)
@RestController
@RequestMapping({"/"})
public class EmailController {
	@Autowired
	private EServicesApplication sendService;
	
	@PostMapping(path = {"/send"})
	public String sendEmail(@RequestBody EmailMessage emailmessage) throws AddressException, MessagingException, IOException {
		this.sendService.sendmail(emailmessage);
		return "Email sent successfully";
	}
}

