package com.padel.eproject.service;

import com.padel.eproject.model.LeaveMasterEntity;
import java.util.List;

public interface LeaveService {
	List<LeaveMasterEntity> findAll();
	
	List<LeaveMasterEntity> findByStaffId(String paramString);
	
	LeaveMasterEntity findByLeaveId(String paramString);
	
	List<LeaveMasterEntity> findLeaveNotApproved(String paramString);
	
	List<LeaveMasterEntity> getNewLeave(String paramString);
	
	double getTotalLeaveOfTheYear(String paramString1, String paramString2);
	
	double getTotalSickLeaveUseOfTheYear(String paramString1, String paramString2);

	double getTotalHospLeaveUseOfTheYear(String paramString1, String paramString2);
	
	List<LeaveMasterEntity> getAllLeaveExcludeYou(String paramString);
	
	List<LeaveMasterEntity> getLeaveWithStatus(String paramString1, String paramString2);
	
	List<LeaveMasterEntity> getUnapproveLeave();
	
	List<LeaveMasterEntity> getTodaysLeave(String paramString);

	double getTotalLeaveUseOfTheYear(String paramString1, String paramString2);
}

