package com.padel.eproject.service;

import com.padel.eproject.model.SuperviseInfoEntity;
import java.util.List;

public interface SuperviseService {
	List<String> getSuperviseeIDs(String paramString, int paramInt);
	
	SuperviseInfoEntity getSuperViseInfo(String paramString);
}
