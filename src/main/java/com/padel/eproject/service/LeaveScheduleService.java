package com.padel.eproject.service;

import java.util.List;

import com.padel.eproject.model.LeaveScheduleEntity;


public interface LeaveScheduleService {
	
	List<LeaveScheduleEntity> findAll();
	
	LeaveScheduleEntity findByPostId(String paramString);

}
