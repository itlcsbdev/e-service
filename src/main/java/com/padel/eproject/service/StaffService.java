package com.padel.eproject.service;

import com.padel.eproject.model.CoStaffEntity;
import com.padel.eproject.model.StaffModel;
import org.springframework.lang.Nullable;

import java.util.List;

public interface StaffService {
	List<CoStaffEntity> findAll();

	@Nullable
	CoStaffEntity findByStaffid(String paramString);
	
	CoStaffEntity findByEmail(String paramString);
	
	StaffModel getStaffInfoWithUserAccess(String paramString);
}
