package com.padel.eproject.service;

import com.padel.eproject.model.UserAccessEntity;

public interface UserAccessService {
	UserAccessEntity findByStaffId(String paramString);
}
