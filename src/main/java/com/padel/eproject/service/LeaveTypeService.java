package com.padel.eproject.service;

import com.padel.eproject.model.LeaveTypeEntity;

public interface LeaveTypeService {

    LeaveTypeEntity getLeaveTypeData(String code);
}
