package com.padel.eproject.service;

import com.padel.eproject.model.CheckmasterEntity;
import com.padel.eproject.model.CoStaffEntity;
import java.util.List;

public interface CheckService {
	List<CoStaffEntity> getAllStaff();

	List<CoStaffEntity> getAllHQStaff();
	
	List<CoStaffEntity> getAllStaffNonHQ();
	
	CheckmasterEntity getCheckInfo(String paramString1, String paramString2);
	
	String getStaffID(int paramInt);
	
	void update(double paramDouble, String paramString1, String paramString2, String paramString3, String paramString4);
	
	CheckmasterEntity create(CheckmasterEntity paramCheckmasterEntity);
	
	String getMaxID();
}
