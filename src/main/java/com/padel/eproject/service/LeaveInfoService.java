package com.padel.eproject.service;

import com.padel.eproject.model.LeaveInfoEntity;

public interface LeaveInfoService {
	LeaveInfoEntity findByStaffId(String paramString);
	
	LeaveInfoEntity findByStaffIdAndYear(String paramString1, String paramString2);
	
	double getBalanceEligibleLeave(String paramString1, String paramString2);
	
	double getEligibleLeaveForTheMonth(String paramString1, String paramString2, String paramString3);
	
	void updateNewBalance(int paramInt, String paramString1, String paramString2);
}

