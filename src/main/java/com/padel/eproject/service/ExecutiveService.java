package com.padel.eproject.service;

import com.padel.eproject.model.ExecutiveEntity;
import com.padel.eproject.model.ExecutiveSpouseEntity;

import java.util.List;


public interface ExecutiveService {
    ExecutiveEntity findByStaffid(String paramString);
    ExecutiveSpouseEntity findSpouseByStaffid(String paramString);
}
