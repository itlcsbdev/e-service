package com.padel.eproject.service;

public interface LeaveInfoUpdateService {
	
	void insertTransactionBalance(String paramString1, String paramString2, String paramString3, int oldbal, int newbal, int yearof);
	void insertNewLeaveOfNewYear(String staffId,String name, String year, int eligibleleavenew, int eligibleleaveold, int bf, int halfofeligible, String datejoined, int yearservice, int balprev, int mc);
	void insertNewLeaveOfNewYearLeaveInfo(String staffId,int eligibleleave, String year, int bf, int mc, int hosp, int maternity);

}
