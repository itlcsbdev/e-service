package com.padel.eproject.service;

import java.util.List;

import com.padel.eproject.model.StaffJoinedEntity;

public interface StaffJoinedService {
	
	List<StaffJoinedEntity> findAll();

}
