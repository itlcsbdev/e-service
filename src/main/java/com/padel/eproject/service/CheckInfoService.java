package com.padel.eproject.service;

import com.padel.eproject.model.CheckformEntity;

public interface CheckInfoService {
	CheckformEntity getCheckFormInfo(String paramString);
	
	CheckformEntity create(CheckformEntity paramCheckformEntity);
}
