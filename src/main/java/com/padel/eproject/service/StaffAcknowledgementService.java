package com.padel.eproject.service;

import com.padel.eproject.model.StaffAcknowledgementEntity;

public interface StaffAcknowledgementService {
    //StaffAcknowledgementEntity getLatestAcknowledge(String id);
    boolean isLatestAcknowledge(String id);
}
