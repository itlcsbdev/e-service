package com.padel.eproject;

import com.padel.eproject.EmailMessage;
import com.padel.eproject.ScheduledTask;
import com.padel.eproject.EServicesApplication;
import com.padel.eproject.model.*;
import com.padel.eproject.service.CheckService;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import com.padel.eproject.service.ExecutiveService;
import com.padel.eproject.service.StaffAcknowledgementService;
import com.padel.eproject.template.EmailTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTask {
    @Autowired
    private EServicesApplication sendService;

    @Autowired
    private CheckService checkService;

    @Autowired
    private ExecutiveService executiveService;

    @Autowired
    private StaffAcknowledgementService ackService;



    private static final Logger log = LoggerFactory.getLogger(ScheduledTask.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    private static final SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");

    @Scheduled(cron = "0 0 10 * * MON")
    public void sendScheduleEmail() throws AddressException, MessagingException, IOException, ParseException {
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setFirstDayOfWeek(2);
        c.setTime(date);
        int i = c.get(7) - c.getFirstDayOfWeek();
        c.add(5, -i - 7);
        Date start = c.getTime();
        c.add(5, 6);
        Date end = c.getTime();
        System.out.println(String.valueOf(dateFormat1.format(start)) + " - " + dateFormat1.format(end));
        List<CoStaffEntity> sv = this.checkService.getAllHQStaff();
        int in = 0;
        for (CoStaffEntity d : sv) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate = formatter.parse(dateFormat1.format(start));
            Date endDate = formatter.parse(dateFormat1.format(end));
            Calendar start2 = Calendar.getInstance();
            start2.setTime(startDate);
            Calendar end2 = Calendar.getInstance();
            end2.setTime(endDate);
            IOSummary ios = new IOSummary();
            List<IOData> CV = new ArrayList<>();
            for (Date datex = start2.getTime(); start2.before(end2); start2.add(5, 1), datex = start2.getTime()) {
                IOData iod = new IOData();
                String workingDate = String.valueOf(dateFormat1.format(datex));
                CheckmasterEntity cm = this.checkService.getCheckInfo(d.getStaffid(), workingDate);
                if (cm != null) {
                    iod.setTimein(cm.getTimevalid());
                    iod.setTimout(cm.getTimeout());
                    iod.setDate(cm.getDate());
                    IOData iod1 = workingHour(cm.getTimevalid(), cm.getTimeout(), cm.getDate(), d.getLocId());
                    iod.setDuration(iod1.getDuration());
                    iod.setColorduration(iod1.getColorduration());
                    iod.setColortimein(iod1.getColortimein());
                    iod.setColortimout(iod1.getColortimout());
                    CV.add(iod);
                    log.info("rekod date :  {}", cm.getDate());
                    log.info("Nama :  {}", d.getName());
                }
            }
            ios.setIo(CV);
            String body = emailBody(ios, d);
            EmailMessage em = new EmailMessage();
            em.setSenderEmail("edaftar@lcsb.com.my");
            em.setSenderPassword("@dminEdaftar");
            em.setSubject("Laporan Mingguan Rekod Kehadiran");
            em.setTo_address(d.getEmail());
            em.setBody(body);
            this.sendService.sendmail(em);
            log.info("Email sent @ {}", dateFormat.format(new Date()));
        }
        log.info("All Staff :  {}", Integer.valueOf(sv.size()));
        log.info("In Staff :  {}", Integer.valueOf(in));
    }

    // @Scheduled(cron = "0 0 11 * * MON")
    //@Scheduled(fixedRate = 10000)
    @Scheduled(cron = "0 0 11 * * MON")
    public void sendScheduleEmailForStaffNonHQ()
            throws AddressException, MessagingException, IOException, ParseException {
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setFirstDayOfWeek(2);
        c.setTime(date);
        int i = c.get(7) - c.getFirstDayOfWeek();
        c.add(5, -i - 7);
        Date start = c.getTime();
        c.add(5, 6);
        Date end = c.getTime();
        System.out.println(String.valueOf(dateFormat1.format(start)) + " - " + dateFormat1.format(end));
        List<CoStaffEntity> sv = this.checkService.getAllStaffNonHQ();
        int in = 0;
        for (CoStaffEntity d : sv) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate = formatter.parse(dateFormat1.format(start));
            Date endDate = formatter.parse(dateFormat1.format(end));
            Calendar start2 = Calendar.getInstance();
            start2.setTime(startDate);
            Calendar end2 = Calendar.getInstance();
            end2.setTime(endDate);
            IOSummary ios = new IOSummary();

            List<IOData> CV = new ArrayList<>();
            for (Date datex = start2.getTime(); start2.before(end2); start2.add(5, 1), datex = start2.getTime()) {
                IOData iod = new IOData();
                String workingDate = String.valueOf(dateFormat1.format(datex));
                CheckmasterEntity cm = this.checkService.getCheckInfo(d.getStaffid(), workingDate);
                if (cm != null) {
                    iod.setTimein(cm.getTimevalid());
                    iod.setTimout(cm.getTimeout());
                    iod.setDate(cm.getDate());
                    IOData iod1 = workingHour(cm.getTimevalid(), cm.getTimeout(), cm.getDate(), d.getLocId());
                    iod.setDuration(iod1.getDuration());
                    iod.setColorduration(iod1.getColorduration());
                    iod.setColortimein(iod1.getColortimein());
                    iod.setColortimout(iod1.getColortimout());
                    CV.add(iod);
                    log.info("rekod date :  {}", cm.getDate());
                    log.info("Nama :  {}", d.getName());
                }
            }

            ios.setIo(CV);
            String body = emailBody(ios, d);
            EmailMessage em = new EmailMessage();
            em.setSenderEmail("edaftar@lcsb.com.my");
            em.setSenderPassword("@dminEdaftar");
            em.setSubject("Laporan Mingguan Rekod Kehadiran");
            em.setTo_address(d.getEmail());
            em.setBody(body);
            this.sendService.sendmail(em);
            log.info("Email sent @ {}", dateFormat.format(new Date()));

        }
        log.info("All Staff :  {}", Integer.valueOf(sv.size()));
        log.info("In Staff :  {}", Integer.valueOf(in));
    }

    @Scheduled(fixedRate = 50000L)
    public void reportCurrentTimeq() {
    }

    @Scheduled(cron = "0 0 10 * * MON,FRI,SAT")
    public void dailyScheduleJob() {
    }

    private static IOData workingHour(String timein, String timeout, String date, String locId) throws ParseException {
        IOData io = new IOData();
        Date timeIn = dateFormat.parse(timein);
        Date timeOut = dateFormat.parse(timeout);
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat formatD = new SimpleDateFormat("yyyy-MM-dd");
        Date timeInMax = null;
        Date timeInMaxRange = null;
        Date timeInMax1 = null;
        Date timeInMaxRange1 = null;
        Date timeInMax2 = null;
        Date timeOutMin1 = null;
        Date timeOutMin2 = null;
        Date lastFasting = formatD.parse("2020-05-24");
        Date thisDate = formatD.parse(date);
        Date normalWorkHour = formatD.parse("2020-06-10");
        if (thisDate.compareTo(lastFasting) > 0) {
            timeInMax = format.parse("08:31:00");
            timeInMaxRange = format.parse("10:00:00");
            timeInMax1 = format.parse("11:30:00");
            timeInMaxRange1 = format.parse("14:00:00");
            timeInMax2 = format.parse("13:01:00");
            timeOutMin1 = format.parse("12:30:00");
            timeOutMin2 = format.parse("17:00:00");
        }
        if (thisDate.compareTo(lastFasting) < 0) {
            timeInMax = format.parse("08:16:00");
            timeInMaxRange = format.parse("10:00:00");
            timeInMax1 = format.parse("11:30:00");
            timeInMaxRange1 = format.parse("14:00:00");
            timeInMax2 = format.parse("12:31:00");
            timeOutMin1 = format.parse("12:15:00");
            timeOutMin2 = format.parse("16:30:00");
        }
        if (thisDate.compareTo(normalWorkHour) > 0) {
            timeInMax = format.parse("08:16:00");
            timeOutMin1 = format.parse("17:00:00");
        }
        if (thisDate.compareTo(normalWorkHour) == 0) {
            timeInMax = format.parse("08:16:00");
            timeOutMin1 = format.parse("17:00:00");
        }
        Date timeOutZero = format.parse("00:00:00");
        if (thisDate.compareTo(normalWorkHour) < 0) {
            if ((timeIn.before(timeInMaxRange) && timeIn.after(timeInMax)) || timeIn.after(timeInMax2)) {
                log.info("Late In :  {}", timeIn);
                io.setColortimein("red");
            } else {
                io.setColortimein("green");
            }
            if (timeOut.before(timeOutMin1)
                    || (timeIn.after(timeInMax1) && timeIn.before(timeInMaxRange1) && timeOut.before(timeOutMin2))) {
                log.info("Early Out :  {}", timeOut);
                io.setColortimout("red");
            } else {
                io.setColortimout("green");
            }
            long difference = timeOut.getTime() - timeIn.getTime();
            long diffInMin = difference / 60000L;
            long diffMinutes = difference / 60000L % 60L;
            long diffHours = difference / 3600000L % 24L;
            io.setDuration(String.valueOf(diffHours + "j " + diffMinutes + "m"));
            if (diffInMin > 241L) {
                io.setCompleteHour(true);
                io.setColorduration("green");
            } else {
                io.setCompleteHour(false);
                io.setColorduration("red");
            }
            if (timeout.equals("00:00:00"))
                io.setDuration("N/A");
        } else {

            if (locId.equals("0000")) {

                if (timeIn.after(timeInMax)) {
                    io.setColortimein("red");
                } else {
                    io.setColortimein("green");
                }
                if (timeOut.before(timeOutMin1)) {
                    io.setColortimout("red");
                } else {
                    io.setColortimout("green");
                }
                long difference = timeOut.getTime() - timeIn.getTime();
                long diffInMin = difference / 60000L;
                long diffMinutes = difference / 60000L % 60L;
                long diffHours = difference / 3600000L % 24L;
                io.setDuration(String.valueOf(diffHours + "j " + diffMinutes + "m"));
                if (diffInMin > 526L) {
                    io.setCompleteHour(true);
                    io.setColorduration("green");
                } else {
                    io.setCompleteHour(false);
                    io.setColorduration("red");
                }

            } else {

                io.setColortimein("green");

                io.setColortimout("green");

                long difference = timeOut.getTime() - timeIn.getTime();
                long diffInMin = difference / 60000L;
                long diffMinutes = difference / 60000L % 60L;
                long diffHours = difference / 3600000L % 24L;
                io.setDuration(String.valueOf(diffHours + "j " + diffMinutes + "m"));

                io.setCompleteHour(true);
                io.setColorduration("green");

            }

            if (timeout.equals("00:00:00"))
                io.setDuration("N/A");
        }
        return io;
    }

    private static String emailBody(IOSummary cm, CoStaffEntity st) {
        String body = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns:v=\"urn:schemas-microsoft-com:vml\">\n\n<head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n    <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0;\" />\n    <!--[if !mso]--><!-- -->\n    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel=\"stylesheet\">\n    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel=\"stylesheet\">\n    <!--<![endif]-->\n\n    <title>Material Design for Bootstrap</title>\n\n    <style type=\"text/css\">\n        body {\n            width: 100%;\n            background-color: #ffffff;\n            margin: 0;\n            padding: 0;\n            -webkit-font-smoothing: antialiased;\n            mso-margin-top-alt: 0px;\n            mso-margin-bottom-alt: 0px;\n            mso-padding-alt: 0px 0px 0px 0px;\n        }\n\n        p,\n        h1,\n        h2,\n        h3,\n        h4 {\n            margin-top: 0;\n            margin-bottom: 0;\n            padding-top: 0;\n            padding-bottom: 0;\n        }\n\n        span.preheader {\n            display: none;\n            font-size: 1px;\n        }\n\n        html {\n            width: 100%;\n        }\n\n        table {\n            font-size: 14px;\n            border: 0;\n        }\n\n.img-circular{\n width: 60px;\n height: 60px;\n background-image: url('"
                + st.getImageUrl()
                + "');\n background-size: cover;\n display: block;\n border-radius: 100px;\n -webkit-border-radius: 100px;\n -moz-border-radius: 100px;\n}\n\n        /* ----------- responsivity ----------- */\n\n        @media only screen and (max-width: 640px) {\n            /*------ top header ------ */\n            .main-header {\n                font-size: 20px !important;\n            }\n            .main-section-header {\n                font-size: 28px !important;\n            }\n            .show {\n                display: block !important;\n            }\n            .hide {\n                display: none !important;\n            }\n            .align-center {\n                text-align: center !important;\n            }\n            .no-bg {\n                background: none !important;\n            }\n            /*----- main image -------*/\n            .main-image img {\n                width: 440px !important;\n                height: auto !important;\n            }\n            /* ====== divider ====== */\n            .divider img {\n                width: 440px !important;\n            }\n            /*-------- container --------*/\n            .container590 {\n                width: 440px !important;\n            }\n            .container580 {\n                width: 400px !important;\n            }\n            .main-button {\n                width: 220px !important;\n            }\n            /*-------- secions ----------*/\n            .section-img img {\n                width: 320px !important;\n                height: auto !important;\n            }\n            .team-img img {\n                width: 100% !important;\n                height: auto !important;\n            }\n        }\n\n        @media only screen and (max-width: 479px) {\n            /*------ top header ------ */\n            .main-header {\n                font-size: 18px !important;\n            }\n            .main-section-header {\n                font-size: 26px !important;\n            }\n            /* ====== divider ====== */\n            .divider img {\n                width: 280px !important;\n            }\n            /*-------- container --------*/\n            .container590 {\n                width: 280px !important;\n            }\n            .container590 {\n                width: 280px !important;\n            }\n            .container580 {\n                width: 260px !important;\n            }\n            /*-------- secions ----------*/\n            .section-img img {\n                width: 280px !important;\n                height: auto !important;\n            }\n        }\n    </style>\n    <!--[if gte mso 9]><style type=”text/css”>\n        body {\n        font-family: arial, sans-serif!important;\n        }\n        </style>\n    <![endif]-->\n</head>\n\n\n<body class=\"respond\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n    <!-- pre-header -->\n    <!--<table style=\"display:none!important;\">\n        <tr>\n            <td>\n                <div style=\"overflow:hidden;display:none;font-size:1px;color:#ffffff;line-height:1px;font-family:Arial;maxheight:0px;max-width:0px;opacity:0;\">\n                    Welcome to MDB!\n                </div>\n            </td>\n        </tr>\n    </table>-->\n    <!-- pre-header end -->\n    <!-- header -->\n    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\">\n\n        <tr>\n            <td align=\"center\">\n                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                    <tr>\n                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"center\">\n\n                            <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                                <tr>\n                        <td align=\"center\" style=\"color: #343434; font-size: 40px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n                            class=\"main-header\">\n                            <!-- section text ======-->\n\n                            <div style=\"line-height: 35px\">\n\n                                e<span style=\"color: #5caad2;\">Daftar</span>\n\n                            </div>\n                                    </td>\n                                </tr>\n\n                                <tr>\n                                    <td align=\"center\">\n                                        <table width=\"360 \" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;\"\n                                            class=\"container590 hide\">\n                                            <tr>\n                                                <td width=\"120\" align=\"center\" style=\"font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                                    Sistem Rekod Kehadiran & Saringan Kesihatan \n                                                </td>\n                                            </tr>\n                                        </table>\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n                    </tr>\n\n                </table>\n            </td>\n        </tr>\n    </table>\n    <!-- end header -->\n\n    <!-- big image section -->\n\n    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\" class=\"bg_color\">\n\n        <tr>\n            <td align=\"center\">\n                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                    <tr>\n                        <td align=\"center\" style=\"color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n                            class=\"main-header\">\n                            <!-- section text ======-->\n                            \n                            <div class=\"img-circular\"></div>\n                            <div style=\"line-height: 35px\">\n\n                                Assalamualaikum <span style=\"color: #5caad2;\">"
                + st.getName()
                + "</span>\n\n                            </div>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"center\">\n                            <table border=\"0\" width=\"40\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"eeeeee\">\n                                <tr>\n                                    <td height=\"2\" style=\"font-size: 2px; line-height: 2px;\">&nbsp;</td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"20\" style=\"font-size: 20px; line-height: 20px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"left\">\n                            <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n                                <tr>\n                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                        <!-- section text ======-->\n\n                                        <p style=\"line-height: 24px; margin-bottom:15px;\">\n\n                                            Tuan/Puan,\n\n                                        </p>\n                                        <p style=\"line-height: 24px;margin-bottom:15px;\">\n                                            Berikut adalah rekod kedatangan anda pada minggu lepas.\n                                        </p>\n                                       \n                                        \n                                        \n                                    </td>\n                                </tr>\n                                <tr>\n                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                        <!-- section text ======-->\n\n                                       <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n                                        <tr>\n                                            <th>Tarikh</th>\n                                            <th>Masuk</th>\n                                            <th>Keluar</th>\n                                            <th>Tempoh</th>\n                                        </tr>\n";
        List<IOData> io = cm.getIo();
        for (IOData d : io)
            body = body
                    + "                                \t\t\t<tr>\n                                \t\t\t\t<td align=\"center\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>"
                    + d.getDate()
                    + "</strong></p></td>\n                                \t\t\t\t<td align=\"center\" style=\"color: "
                    + d.getColortimein()
                    + "; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>"
                    + d.getTimein()
                    + "</strong></p></td>\n                                \t\t\t\t<td align=\"center\" style=\"color: "
                    + d.getColortimout()
                    + "; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>"
                    + d.getTimout()
                    + "</strong></p></td>\n                                \t\t\t\t<td align=\"center\" style=\"color: "
                    + d.getColorduration()
                    + "; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>"
                    + d.getDuration() + "</strong></p></td>\n\n                                \t\t\t</tr>\n";
        body = body
                + "                                \t\t</table>\n                                \t\t<p>&nbsp;</p>\n                                \t\t <p style=\"line-height: 24px; margin-bottom:20px;\">\n                                            Terima kasih atas kerjasama anda.\n                                        </p>\n                                        \n                                        \n                                    </td>\n                                </tr>\n                                <tr>\n                                \t<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                \t\t<br>\n                                \t\t<p style=\"line-height: 24px\">\n                                            Yang Benar,\n                                        </p>\n\t\t\t\t\t\t\t\t\t\t<p style=\"line-height: 24px\">\n                                            <strong>LCSB Bot</strong>\n                                        </p>\n                                        <p style=\"line-height: 24px\">\n                                            Pentadbir Maya Sistem eDaftar\n                                        </p>\n\n                                \t</td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n\n\n\n\n                </table>\n\n            </td>\n        </tr>\n\n        <tr>\n            <td height=\"40\" style=\"font-size: 40px; line-height: 40px;\">&nbsp;</td>\n        </tr>\n\n    </table>\n\n    <!-- end section -->\n\n\n    <!-- main section -->\n    \n\n    <!-- end section -->\n\n    \n    <!-- end section -->\n\n    <!-- footer ====== -->\n    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;\"> <tr> <td align=\"center\" valign=\"top\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td align=\"center\" valign=\"top\"> <div style=\"height: 34px; line-height: 34px; font-size: 32px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#868686\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #868686; font-size: 17px; line-height: 20px;\">Copyright &copy; 2019 LCSB. All&nbsp;Rights&nbsp;Reserved.</span> </font> <div style=\"height: 3px; line-height: 3px; font-size: 1px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#1a1a1a\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px;\"><a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">sysadmin@lcsb.com.my</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">09-5165180</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"http://www.lcsb.com.my\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">www.lcsb.com.my</a></span> </font> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"> <tr> <td align=\"center\" valign=\"top\"> <a href=\"#\" target=\"_blank\" style=\"display: block; max-width: 19px;\"> <img src=\"https://docs.google.com/uc?id=1NomvumBJQqjFi0WYxP45K_RLRSXfZXLP\" alt=\"img\" width=\"20\" border=\"0\" style=\"display: block; width: 20;\" /> </a> </td> <td><span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif;font-size: 12px\"> &nbsp; LKPP CORPORATION SDN BHD</span></td> </tr> </table> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> </td> </tr> </table>\n    <!-- end footer ====== -->\n\n</body>\n\n</html>";
        return body;
    }

    //@Scheduled(cron = "0 0 11 * * MON")
    //@Scheduled(fixedRate = 50000L)
    //@Scheduled(cron = "0 33 10 * * MON")
    public void sendEmailBorangMaklumatPekerjaForStaffNonHQ()
            throws AddressException, MessagingException, IOException, ParseException {
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setFirstDayOfWeek(2);
        c.setTime(date);
        int i = c.get(7) - c.getFirstDayOfWeek();
        c.add(5, -i - 7);
        Date start = c.getTime();
        c.add(5, 6);
        Date end = c.getTime();
        System.out.println(String.valueOf(dateFormat1.format(start)) + " - " + dateFormat1.format(end));
        List<CoStaffEntity> sv = this.checkService.getAllStaffNonHQ();
        int in = 0;
        for (CoStaffEntity d : sv) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate = formatter.parse(dateFormat1.format(start));
            Date endDate = formatter.parse(dateFormat1.format(end));
            Calendar start2 = Calendar.getInstance();
            start2.setTime(startDate);
            Calendar end2 = Calendar.getInstance();
            end2.setTime(endDate);
            IOSummary ios = new IOSummary();
            List<IOData> CV = new ArrayList<>();

            ios.setIo(CV);
            String body = EmailTemplate.emailBodyBorangMaklumat(ios, d);
            EmailMessage em = new EmailMessage();
            em.setSenderEmail("edaftar@lcsb.com.my");
            em.setSenderPassword("@dminEdaftar");
            em.setSubject("Kemaskini Borang Maklumat Pekerja");
            em.setTo_address(d.getEmail());
            //em.setTo_address("fadhilfahmi@lcsb.com.my");
            em.setBody(body);
            this.sendService.sendmail(em);
            log.info("Email sent @ {}", dateFormat.format(new Date()));

        }
        log.info("All Staff :  {}", Integer.valueOf(sv.size()));
        log.info("In Staff :  {}", Integer.valueOf(in));
    }

    //@Scheduled(fixedRate = 50000L)
    //@Scheduled(cron = "0 11 10 * * MON")
    public void sendEmailBorangMaklumatPekerjaForStaffHQ()
            throws AddressException, MessagingException, IOException, ParseException {
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setFirstDayOfWeek(2);
        c.setTime(date);
        int i = c.get(7) - c.getFirstDayOfWeek();
        c.add(5, -i - 7);
        Date start = c.getTime();
        c.add(5, 6);
        Date end = c.getTime();
        System.out.println(String.valueOf(dateFormat1.format(start)) + " - " + dateFormat1.format(end));
        List<CoStaffEntity> sv = this.checkService.getAllHQStaff();
        int in = 0;
        for (CoStaffEntity d : sv) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate = formatter.parse(dateFormat1.format(start));
            Date endDate = formatter.parse(dateFormat1.format(end));
            Calendar start2 = Calendar.getInstance();
            start2.setTime(startDate);
            Calendar end2 = Calendar.getInstance();
            end2.setTime(endDate);
            IOSummary ios = new IOSummary();
            List<IOData> CV = new ArrayList<>();

            ios.setIo(CV);
            String body = EmailTemplate.emailBodyBorangMaklumat(ios, d);
            EmailMessage em = new EmailMessage();
            em.setSenderEmail("edaftar@lcsb.com.my");
            em.setSenderPassword("@dminEdaftar");
            em.setSubject("Kemaskini Borang Maklumat Pekerja");
            em.setTo_address(d.getEmail());
            //em.setTo_address("fadhilfahmi@lcsb.com.my");
            em.setBody(body);
            this.sendService.sendmail(em);
            log.info("Email sent @ {}", dateFormat.format(new Date()));

        }
        log.info("All Staff :  {}", Integer.valueOf(sv.size()));
        log.info("In Staff :  {}", Integer.valueOf(in));
    }

    //@Scheduled(fixedRate = 50000L)
    @Scheduled(cron = "0 0 8 * * FRI")
    public void sendEmailBorangMaklumatPekerjaNotCompletedForStaffHQ()
            throws AddressException, MessagingException, IOException, ParseException {

        List<CoStaffEntity> sv = this.checkService.getAllHQStaff();
        int in = 0;
        for (CoStaffEntity d : sv) {
            log.info(" Staff :  {}", d.getStaffid());
            int percent = getPercentComplete(d.getStaffid());
            log.info(" Percent :  {}", percent);

            if (percent < 100) {

                String body = emailBodyBorangMaklumatNotComplete(percent, d);
                EmailMessage em = new EmailMessage();
                em.setSenderEmail("edaftar@lcsb.com.my");
                em.setSenderPassword("@dminEdaftar");
                em.setSubject("("+percent + "%) Kemaskini Borang Maklumat Pekerja");
                em.setTo_address(d.getEmail());
                //em.setTo_address("fadhilfahmi@lcsb.com.my");
                em.setBody(body);
                this.sendService.sendmail(em);
                log.info("Email sent @ {}", dateFormat.format(new Date()));

            }

        }
        log.info("All Staff :  {}", Integer.valueOf(sv.size()));
        log.info("In Staff :  {}", Integer.valueOf(in));
    }

    @Scheduled(cron = "0 30 8 * * FRI")
    public void sendEmailBorangMaklumatPekerjaNotCompletedForStaffNonHQ()
            throws AddressException, MessagingException, IOException, ParseException {

        List<CoStaffEntity> sv = this.checkService.getAllStaffNonHQ();
        int in = 0;
        for (CoStaffEntity d : sv) {
            log.info(" Staff :  {}", d.getStaffid());
            int percent = getPercentComplete(d.getStaffid());
            log.info(" Percent :  {}", percent);

            if (percent < 100) {

                String body = emailBodyBorangMaklumatNotComplete(percent, d);
                EmailMessage em = new EmailMessage();
                em.setSenderEmail("edaftar@lcsb.com.my");
                em.setSenderPassword("@dminEdaftar");
                em.setSubject("("+percent + "%) Kemaskini Borang Maklumat Pekerja");
                em.setTo_address(d.getEmail());
                //em.setTo_address("fadhilfahmi@lcsb.com.my");
                em.setBody(body);
                this.sendService.sendmail(em);
                log.info("Email sent @ {}", dateFormat.format(new Date()));

            }

        }
        log.info("All Staff :  {}", Integer.valueOf(sv.size()));
        log.info("In Staff :  {}", Integer.valueOf(in));
    }

    public int getPercentComplete(String staffID) {
        int percent = 0;

        int c = 27;
        int total = 27;

        ExecutiveEntity e = executiveService.findByStaffid(staffID);

        if (e != null) {


            if (e.getDob() == null || e.getDob().equals("0000-00-00")) {
                c -= 1;
            }
            if (e.getNokp() == null || e.getNokp().equals("")) {
                c -= 1;
            }
            if (e.getBirthplace() == null || e.getBirthplace().equals("")) {
                c -= 1;
            }
            if (e.getMarital() == null || e.getMarital().equals("None")) {
                c -= 1;
            }
            if (e.getBlood() == null || e.getBlood().equals("None")) {
                c -= 1;
            }
            if (e.getDepartment() == null || e.getDepartment().equals("")) {
                c -= 1;
            }
            if (e.getAddress() == null || e.getAddress().equals("")) {
                c -= 1;
            }
            if (e.getFixaddress() == null || e.getFixaddress().equals("")) {
                c -= 1;
            }
            if (e.getRace() == null || e.getRace().equals("None")) {
                c -= 1;
            }
            if (e.getReligion() == null || e.getReligion().equals("None")) {
                c -= 1;
            }
            if (e.getTaxno() == null || e.getTaxno().equals("") || e.getTaxno().equals("null")) {
                c -= 1;
            }
            if (e.getNoepf() == null || e.getNoepf().equals("")) {
                c -= 1;
            }
            if (e.getNosocso() == null || e.getNosocso().equals("")) {
                c -= 1;
            }
            if (e.getPosition() == null || e.getPosition().equals("")) {
                c -= 1;
            }
            if (e.getWorkdate() == null || e.getWorkdate().equals("0000-00-00") || e.getWorkdate().equals("")) {
                c -= 1;
            }
            if (e.getNamawaris1() == null || e.getWorkdate().equals("")) {
                c -= 1;
            }
            if (e.getHubunganwaris1() == null || e.getHubunganwaris1().equals("")) {
                c -= 1;
            }
            if (e.getNotelwaris1() == null || e.getNotelwaris1().equals("")) {
                c -= 1;
            }
            if (e.getAlamatwaris1() == null || e.getAlamatwaris1().equals("")) {
                c -= 1;
            }
            if (e.getNamawaris2() == null || e.getNamawaris2().equals("")) {
                c -= 1;
            }
            if (e.getHubunganwaris2() == null || e.getHubunganwaris2().equals("")) {
                c -= 1;
            }
            if (e.getNotelwaris2() == null || e.getNotelwaris2().equals("")) {
                c -= 1;
            }
            if (e.getAlamatwaris2() == null || e.getAlamatwaris2().equals("")) {
                c -= 1;
            }
            if (e.getEducationlevel() == null || e.getEducationlevel().equals("")) {
                c -= 1;
            }
            if (e.getBank() == null || e.getBank().equals("")) {
                c -= 1;
            }
            if (e.getNoakaun() == null || e.getNoakaun().equals("") || e.getNoakaun().equals("null")) {
                c -= 1;
            }

            if (e.getMarital().equals("Berkahwin")) {
                c += 3;
                total += 3;

                ExecutiveSpouseEntity es = executiveService.findSpouseByStaffid(staffID);

                if (es != null) {
                    if (es.getNamespouse() == null || es.getNamespouse().equals("")) {
                        c -= 1;
                    }
                    if (es.getNokpspouse() == null || es.getNokpspouse().equals("")) {
                        c -= 1;
                    }
                    if (es.getSpousework() == null || es.getSpousework().equals("")) {
                        c -= 1;
                    }
                }

            }

        }

        percent = c * 100 / total;

        return percent;
    }

    @Scheduled(cron = "0 28 9 * * TUE")//@Scheduled(fixedRate = 50000L)
    public void sendEmailPerakuanBorangMaklumatPekerjaForStaffNonHQ()
            throws AddressException, MessagingException, IOException, ParseException {

        List<CoStaffEntity> sv = this.checkService.getAllStaffNonHQ();
        int in = 0;
        for (CoStaffEntity d : sv) {
            log.info(" Staff :  {}", d.getStaffid());


            if (!ackService.isLatestAcknowledge(d.getStaffid())) {

                in++;

                log.info(" Staff :  {}", d.getStaffid());

                String body = emailBodyPerakuanBorangMaklumat(d);
                EmailMessage em = new EmailMessage();
                em.setSenderEmail("edaftar@lcsb.com.my");
                em.setSenderPassword("@dminEdaftar");
                em.setSubject("Perakuan Borang Maklumat Pekerja");
                em.setTo_address(d.getEmail());
                //em.setTo_address("fadhilfahmi@lcsb.com.my");
                em.setBody(body);
                this.sendService.sendmail(em);
                log.info("Email sent @ {}", dateFormat.format(new Date()));

            }

        }
        log.info("All Staff :  {}", Integer.valueOf(sv.size()));
        log.info("In Staff :  {}", Integer.valueOf(in));
    }

    //@Scheduled(cron = "0 0 8 * * FRI")
    //@Scheduled(fixedRate = 50000L)
    @Scheduled(cron = "0 11 11 * * WED")//@Scheduled(fixedRate = 50000L)
    public void sendEmailPerakuanBorangMaklumatPekerjaForAllStaff()
            throws AddressException, MessagingException, IOException, ParseException {

        List<CoStaffEntity> sv = this.checkService.getAllStaff();
        int in = 0;
        for (CoStaffEntity d : sv) {
            log.info(" Staff :  {}", d.getStaffid());


            if (!ackService.isLatestAcknowledge(d.getStaffid())) {

                in++;

                log.info(" Staff :  {}", d.getStaffid());
                log.info(" StaffName :  {}", d.getName());

                String body = emailBodyPerakuanBorangMaklumat(d);
                EmailMessage em = new EmailMessage();
                em.setSenderEmail("edaftar@lcsb.com.my");
                em.setSenderPassword("@dminEdaftar");
                em.setSubject("Perakuan Borang Maklumat Pekerja");
                em.setTo_address(d.getEmail());
                //em.setTo_address("fadhilfahmi@lcsb.com.my");
                em.setBody(body);
                this.sendService.sendmail(em);
                log.info("Email sent @ {}", dateFormat.format(new Date()));

            }

        }
        log.info("All Staff :  {}", Integer.valueOf(sv.size()));
        log.info("In Staff :  {}", Integer.valueOf(in));
    }

    //@Scheduled(cron = "0 0 8 * * FRI")
    //@Scheduled(fixedRate = 50000L)
    public void sendEmailPerakuanBorangMaklumatPekerjaForStaffHQ()
            throws AddressException, MessagingException, IOException, ParseException {

        List<CoStaffEntity> sv = this.checkService.getAllHQStaff();
        int in = 0;
        for (CoStaffEntity d : sv) {
            log.info(" Staff :  {}", d.getStaffid());


            if (!ackService.isLatestAcknowledge(d.getStaffid())) {

                in++;

                log.info(" Staff :  {}", d.getStaffid());
                log.info(" StaffName :  {}", d.getName());

                String body = emailBodyPerakuanBorangMaklumat(d);
                EmailMessage em = new EmailMessage();
                em.setSenderEmail("edaftar@lcsb.com.my");
                em.setSenderPassword("@dminEdaftar");
                em.setSubject("Perakuan Borang Maklumat Pekerja");
                em.setTo_address(d.getEmail());
                //em.setTo_address("fadhilfahmi@lcsb.com.my");
                em.setBody(body);
                //this.sendService.sendmail(em);
                log.info("Email sent @ {}", dateFormat.format(new Date()));

            }

        }
        log.info("All Staff :  {}", Integer.valueOf(sv.size()));
        log.info("In Staff :  {}", Integer.valueOf(in));
    }



    private static String emailBodyBorangMaklumatNotComplete(int percent, CoStaffEntity st) {
        String body = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns:v=\"urn:schemas-microsoft-com:vml\">\n\n<head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n    <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0;\" />\n    <!--[if !mso]--><!-- -->\n    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel=\"stylesheet\">\n    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel=\"stylesheet\">\n    <!--<![endif]-->\n\n    <title>Material Design for Bootstrap</title>\n\n    <style type=\"text/css\">\n        body {\n            width: 100%;\n            background-color: #ffffff;\n            margin: 0;\n            padding: 0;\n            -webkit-font-smoothing: antialiased;\n            mso-margin-top-alt: 0px;\n            mso-margin-bottom-alt: 0px;\n            mso-padding-alt: 0px 0px 0px 0px;\n        }\n\n        p,\n        h1,\n        h2,\n        h3,\n        h4 {\n            margin-top: 0;\n            margin-bottom: 0;\n            padding-top: 0;\n            padding-bottom: 0;\n        }\n\n        span.preheader {\n            display: none;\n            font-size: 1px;\n        }\n\n        html {\n            width: 100%;\n        }\n\n        table {\n            font-size: 14px;\n            border: 0;\n        }\n\n.img-circular{\n width: 60px;\n height: 60px;\n background-image: url('"
                + st.getImageUrl()
                + "');\n background-size: cover;\n display: block;\n border-radius: 100px;\n -webkit-border-radius: 100px;\n -moz-border-radius: 100px;\n}\n\n        /* ----------- responsivity ----------- */\n\n        @media only screen and (max-width: 640px) {\n            /*------ top header ------ */\n            .main-header {\n                font-size: 20px !important;\n            }\n            .main-section-header {\n                font-size: 28px !important;\n            }\n            .show {\n                display: block !important;\n            }\n            .hide {\n                display: none !important;\n            }\n            .align-center {\n                text-align: center !important;\n            }\n            .no-bg {\n                background: none !important;\n            }\n            /*----- main image -------*/\n            .main-image img {\n                width: 440px !important;\n                height: auto !important;\n            }\n            /* ====== divider ====== */\n            .divider img {\n                width: 440px !important;\n            }\n            /*-------- container --------*/\n            .container590 {\n                width: 440px !important;\n            }\n            .container580 {\n                width: 400px !important;\n            }\n            .main-button {\n                width: 220px !important;\n            }\n            /*-------- secions ----------*/\n            .section-img img {\n                width: 320px !important;\n                height: auto !important;\n            }\n            .team-img img {\n                width: 100% !important;\n                height: auto !important;\n            }\n        }\n\n        @media only screen and (max-width: 479px) {\n            /*------ top header ------ */\n            .main-header {\n                font-size: 18px !important;\n            }\n            .main-section-header {\n                font-size: 26px !important;\n            }\n            /* ====== divider ====== */\n            .divider img {\n                width: 280px !important;\n            }\n            /*-------- container --------*/\n            .container590 {\n                width: 280px !important;\n            }\n            .container590 {\n                width: 280px !important;\n            }\n            .container580 {\n                width: 260px !important;\n            }\n            /*-------- secions ----------*/\n            .section-img img {\n                width: 280px !important;\n                height: auto !important;\n            }\n        }\n    </style>\n    <!--[if gte mso 9]><style type=”text/css”>\n        body {\n        font-family: arial, sans-serif!important;\n        }\n        </style>\n    <![endif]-->\n</head>\n\n\n<body class=\"respond\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n    <!-- pre-header -->\n    <!--<table style=\"display:none!important;\">\n        <tr>\n            <td>\n                <div style=\"overflow:hidden;display:none;font-size:1px;color:#ffffff;line-height:1px;font-family:Arial;maxheight:0px;max-width:0px;opacity:0;\">\n                    Welcome to MDB!\n                </div>\n            </td>\n        </tr>\n    </table>-->\n    <!-- pre-header end -->\n    <!-- header -->\n    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\">\n\n        <tr>\n            <td align=\"center\">\n                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                    <tr>\n                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"center\">\n\n                            <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                                <tr>\n                        <td align=\"center\" style=\"color: #343434; font-size: 40px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n                            class=\"main-header\">\n                            <!-- section text ======-->\n\n                            <div style=\"line-height: 35px\">\n\n                                e<span style=\"color: #5caad2;\">Daftar</span>\n\n                            </div>\n                                    </td>\n                                </tr>\n\n                                <tr>\n                                    <td align=\"center\">\n                                        <table width=\"360 \" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;\"\n                                            class=\"container590 hide\">\n                                            <tr>\n                                                <td width=\"120\" align=\"center\" style=\"font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                                    Sistem Rekod Kehadiran & Saringan Kesihatan \n                                                </td>\n                                            </tr>\n                                        </table>\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n                    </tr>\n\n                </table>\n            </td>\n        </tr>\n    </table>\n    <!-- end header -->\n\n    <!-- big image section -->\n\n    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\" class=\"bg_color\">\n\n        <tr>\n            <td align=\"center\">\n                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                    <tr>\n                        <td align=\"center\" style=\"color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n                            class=\"main-header\">\n                            <!-- section text ======-->\n                            \n                            <div class=\"img-circular\"></div>\n                            <div style=\"line-height: 35px\">\n\n                                Assalamualaikum <span style=\"color: #5caad2;\">"
                + st.getName()
                + "</span>\n\n                            </div>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"center\">\n                            <table border=\"0\" width=\"40\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"eeeeee\">\n                                <tr>\n                                    <td height=\"2\" style=\"font-size: 2px; line-height: 2px;\">&nbsp;</td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"20\" style=\"font-size: 20px; line-height: 20px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"left\">\n                            <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n                                <tr>\n                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                        <!-- section text ======-->\n\n                                        <p style=\"line-height: 24px; margin-bottom:15px;\">\n\n                                            Tuan/Puan,\n\n                                        </p>\n                                        <p style=\"line-height: 24px;margin-bottom:15px;\">\n                                            <span style=\"color: #FF0000; font-size: 20px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><strong></span>Peratus Selesai Kemaskini : "+percent+"%</strong><br><br>Maklumat Pekerja anda masih belum lengkap dikemaskini. <br><br>Anda dikehendaki untuk mengemaskini maklumat peribadi dengan menggunakan borang yang disediakan di dalam Sistem eDaftar. Klik butang di bawah untuk mengisi borang tersebut. \n                                        </p>\n                                       \n                                        \n                                        \n                                    </td>\n                                </tr>\n                                <tr>\n                                    <td align=\"center\" style=\"color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 22px; letter-spacing: 2px;\"><table border=\"0\" align=\"center\" width=\"180\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"5caad2\" style=\"margin-bottom:20px;\">\n" +
                "\n" +
                "                                            <tr>\n" +
                "                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n" +
                "                                            </tr>\n" +
                "\n" +
                "                                            <tr>\n" +
                "                                                <td align=\"center\" style=\"color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 22px; letter-spacing: 2px;\">\n" +
                "                                                    <!-- main section button -->\n" +
                "\n" +
                "                                                    <div style=\"line-height: 22px;\">\n" +
                "                                                        <a href=\"https://edaftar.lcsb.com.my/borangmaklumatpekerja\" style=\"color: #ffffff; text-decoration: none;\">Borang Maklumat Pekerja</a>\n" +
                "                                                    </div>\n" +
                "                                                </td>\n" +
                "                                            </tr>\n" +
                "\n" +
                "                                            <tr>\n" +
                "                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n" +
                "                                            </tr>\n" +
                "\n" +
                "                                        </table></td>\n                                </tr>\n                                <tr>\n                                \t<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                \t\t<br>\n                                \t\t<p style=\"line-height: 24px\">\n                                            \n                                        </p>\n\t\t\t\t\t\t\t\t\t\t<p style=\"line-height: 24px\">\n                                            <strong>LCSB Bot</strong>\n                                        </p>\n                                        <p style=\"line-height: 24px\">\n                                            Pentadbir Maya Sistem eDaftar\n                                        </p>\n\n                                \t</td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n\n\n\n\n                </table>\n\n            </td>\n        </tr>\n\n        <tr>\n            <td height=\"40\" style=\"font-size: 40px; line-height: 40px;\">&nbsp;</td>\n        </tr>\n\n    </table>\n\n    <!-- end section -->\n\n\n    <!-- main section -->\n    \n\n    <!-- end section -->\n\n    \n    <!-- end section -->\n\n    <!-- footer ====== -->\n    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;\"> <tr> <td align=\"center\" valign=\"top\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td align=\"center\" valign=\"top\"> <div style=\"height: 34px; line-height: 34px; font-size: 32px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#868686\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #868686; font-size: 17px; line-height: 20px;\">Copyright &copy; 2019 LCSB. All&nbsp;Rights&nbsp;Reserved.</span> </font> <div style=\"height: 3px; line-height: 3px; font-size: 1px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#1a1a1a\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px;\"><a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">sysadmin@lcsb.com.my</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">09-5165180</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"http://www.lcsb.com.my\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">www.lcsb.com.my</a></span> </font> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"> <tr> <td align=\"center\" valign=\"top\"> <a href=\"#\" target=\"_blank\" style=\"display: block; max-width: 19px;\"> <img src=\"https://docs.google.com/uc?id=1NomvumBJQqjFi0WYxP45K_RLRSXfZXLP\" alt=\"img\" width=\"20\" border=\"0\" style=\"display: block; width: 20;\" /> </a> </td> <td><span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif;font-size: 12px\"> &nbsp; LKPP CORPORATION SDN BHD</span></td> </tr> </table> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> </td> </tr> </table>\n    <!-- end footer ====== -->\n\n</body>\n\n</html>";
        return body;
    }

    private static String emailBodyPerakuanBorangMaklumat(CoStaffEntity st) {
        String body = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns:v=\"urn:schemas-microsoft-com:vml\">\n\n<head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n    <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0;\" />\n    <!--[if !mso]--><!-- -->\n    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel=\"stylesheet\">\n    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel=\"stylesheet\">\n    <!--<![endif]-->\n\n    <title>Material Design for Bootstrap</title>\n\n    <style type=\"text/css\">\n        body {\n            width: 100%;\n            background-color: #ffffff;\n            margin: 0;\n            padding: 0;\n            -webkit-font-smoothing: antialiased;\n            mso-margin-top-alt: 0px;\n            mso-margin-bottom-alt: 0px;\n            mso-padding-alt: 0px 0px 0px 0px;\n        }\n\n        p,\n        h1,\n        h2,\n        h3,\n        h4 {\n            margin-top: 0;\n            margin-bottom: 0;\n            padding-top: 0;\n            padding-bottom: 0;\n        }\n\n        span.preheader {\n            display: none;\n            font-size: 1px;\n        }\n\n        html {\n            width: 100%;\n        }\n\n        table {\n            font-size: 14px;\n            border: 0;\n        }\n\n.img-circular{\n width: 60px;\n height: 60px;\n background-image: url('"
                + st.getImageUrl()
                + "');\n background-size: cover;\n display: block;\n border-radius: 100px;\n -webkit-border-radius: 100px;\n -moz-border-radius: 100px;\n}\n\n        /* ----------- responsivity ----------- */\n\n        @media only screen and (max-width: 640px) {\n            /*------ top header ------ */\n            .main-header {\n                font-size: 20px !important;\n            }\n            .main-section-header {\n                font-size: 28px !important;\n            }\n            .show {\n                display: block !important;\n            }\n            .hide {\n                display: none !important;\n            }\n            .align-center {\n                text-align: center !important;\n            }\n            .no-bg {\n                background: none !important;\n            }\n            /*----- main image -------*/\n            .main-image img {\n                width: 440px !important;\n                height: auto !important;\n            }\n            /* ====== divider ====== */\n            .divider img {\n                width: 440px !important;\n            }\n            /*-------- container --------*/\n            .container590 {\n                width: 440px !important;\n            }\n            .container580 {\n                width: 400px !important;\n            }\n            .main-button {\n                width: 220px !important;\n            }\n            /*-------- secions ----------*/\n            .section-img img {\n                width: 320px !important;\n                height: auto !important;\n            }\n            .team-img img {\n                width: 100% !important;\n                height: auto !important;\n            }\n        }\n\n        @media only screen and (max-width: 479px) {\n            /*------ top header ------ */\n            .main-header {\n                font-size: 18px !important;\n            }\n            .main-section-header {\n                font-size: 26px !important;\n            }\n            /* ====== divider ====== */\n            .divider img {\n                width: 280px !important;\n            }\n            /*-------- container --------*/\n            .container590 {\n                width: 280px !important;\n            }\n            .container590 {\n                width: 280px !important;\n            }\n            .container580 {\n                width: 260px !important;\n            }\n            /*-------- secions ----------*/\n            .section-img img {\n                width: 280px !important;\n                height: auto !important;\n            }\n        }\n    </style>\n    <!--[if gte mso 9]><style type=”text/css”>\n        body {\n        font-family: arial, sans-serif!important;\n        }\n        </style>\n    <![endif]-->\n</head>\n\n\n<body class=\"respond\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n    <!-- pre-header -->\n    <!--<table style=\"display:none!important;\">\n        <tr>\n            <td>\n                <div style=\"overflow:hidden;display:none;font-size:1px;color:#ffffff;line-height:1px;font-family:Arial;maxheight:0px;max-width:0px;opacity:0;\">\n                    Welcome to MDB!\n                </div>\n            </td>\n        </tr>\n    </table>-->\n    <!-- pre-header end -->\n    <!-- header -->\n    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\">\n\n        <tr>\n            <td align=\"center\">\n                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                    <tr>\n                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"center\">\n\n                            <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                                <tr>\n                        <td align=\"center\" style=\"color: #343434; font-size: 40px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n                            class=\"main-header\">\n                            <!-- section text ======-->\n\n                            <div style=\"line-height: 35px\">\n\n                                e<span style=\"color: #5caad2;\">Daftar</span>\n\n                            </div>\n                                    </td>\n                                </tr>\n\n                                <tr>\n                                    <td align=\"center\">\n                                        <table width=\"360 \" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;\"\n                                            class=\"container590 hide\">\n                                            <tr>\n                                                <td width=\"120\" align=\"center\" style=\"font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                                    Sistem Rekod Kehadiran & Saringan Kesihatan \n                                                </td>\n                                            </tr>\n                                        </table>\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n                    </tr>\n\n                </table>\n            </td>\n        </tr>\n    </table>\n    <!-- end header -->\n\n    <!-- big image section -->\n\n    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\" class=\"bg_color\">\n\n        <tr>\n            <td align=\"center\">\n                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                    <tr>\n                        <td align=\"center\" style=\"color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n                            class=\"main-header\">\n                            <!-- section text ======-->\n                            \n                            <div class=\"img-circular\"></div>\n                            <div style=\"line-height: 35px\">\n\n                                Assalamualaikum <span style=\"color: #5caad2;\">"
                + st.getName()
                + "</span>\n\n                            </div>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"center\">\n                            <table border=\"0\" width=\"40\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"eeeeee\">\n                                <tr>\n                                    <td height=\"2\" style=\"font-size: 2px; line-height: 2px;\">&nbsp;</td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"20\" style=\"font-size: 20px; line-height: 20px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"left\">\n                            <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n                                <tr>\n                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                        <!-- section text ======-->\n\n                                        <p style=\"line-height: 24px; margin-bottom:15px;\">\n\n                                            Tuan/Puan,\n\n                                        </p>\n                                        <p style=\"line-height: 24px;margin-bottom:15px;\">\n                                            <span style=\"color: #FF0000; font-size: 20px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><strong></span>Maklumat pekerja anda masih belum diperakui.</strong><br><br>Sila buat perakuan maklumat yang telah anda isikan. <br><br>Anda dikehendaki untuk memperakui maklumat peribadi di dalam Sistem eDaftar. Klik butang di bawah untuk memperakui maklumat tersebut. \n                                        </p>\n                                       \n                                        \n                                        \n                                    </td>\n                                </tr>\n                                <tr>\n                                    <td align=\"center\" style=\"color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 22px; letter-spacing: 2px;\"><table border=\"0\" align=\"center\" width=\"180\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"5caad2\" style=\"margin-bottom:20px;\">\n" +
                "\n" +
                "                                            <tr>\n" +
                "                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n" +
                "                                            </tr>\n" +
                "\n" +
                "                                            <tr>\n" +
                "                                                <td align=\"center\" style=\"color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 22px; letter-spacing: 2px;\">\n" +
                "                                                    <!-- main section button -->\n" +
                "\n" +
                "                                                    <div style=\"line-height: 22px;\">\n" +
                "                                                        <a href=\"https://edaftar.lcsb.com.my/borangmaklumatpekerja\" style=\"color: #ffffff; text-decoration: none;\">Borang Maklumat Pekerja</a>\n" +
                "                                                    </div>\n" +
                "                                                </td>\n" +
                "                                            </tr>\n" +
                "\n" +
                "                                            <tr>\n" +
                "                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n" +
                "                                            </tr>\n" +
                "\n" +
                "                                        </table></td>\n                                </tr>\n                                <tr>\n                                \t<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                \t\t<br>\n                                \t\t<p style=\"line-height: 24px\">\n                                            \n                                        </p>\n\t\t\t\t\t\t\t\t\t\t<p style=\"line-height: 24px\">\n                                            <strong>LCSB Bot</strong>\n                                        </p>\n                                        <p style=\"line-height: 24px\">\n                                            Pentadbir Maya Sistem eDaftar\n                                        </p>\n\n                                \t</td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n\n\n\n\n                </table>\n\n            </td>\n        </tr>\n\n        <tr>\n            <td height=\"40\" style=\"font-size: 40px; line-height: 40px;\">&nbsp;</td>\n        </tr>\n\n    </table>\n\n    <!-- end section -->\n\n\n    <!-- main section -->\n    \n\n    <!-- end section -->\n\n    \n    <!-- end section -->\n\n    <!-- footer ====== -->\n    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;\"> <tr> <td align=\"center\" valign=\"top\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td align=\"center\" valign=\"top\"> <div style=\"height: 34px; line-height: 34px; font-size: 32px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#868686\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #868686; font-size: 17px; line-height: 20px;\">Copyright &copy; 2019 LCSB. All&nbsp;Rights&nbsp;Reserved.</span> </font> <div style=\"height: 3px; line-height: 3px; font-size: 1px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#1a1a1a\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px;\"><a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">sysadmin@lcsb.com.my</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">09-5165180</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"http://www.lcsb.com.my\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">www.lcsb.com.my</a></span> </font> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"> <tr> <td align=\"center\" valign=\"top\"> <a href=\"#\" target=\"_blank\" style=\"display: block; max-width: 19px;\"> <img src=\"https://docs.google.com/uc?id=1NomvumBJQqjFi0WYxP45K_RLRSXfZXLP\" alt=\"img\" width=\"20\" border=\"0\" style=\"display: block; width: 20;\" /> </a> </td> <td><span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif;font-size: 12px\"> &nbsp; LKPP CORPORATION SDN BHD</span></td> </tr> </table> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> </td> </tr> </table>\n    <!-- end footer ====== -->\n\n</body>\n\n</html>";
        return body;
    }
}