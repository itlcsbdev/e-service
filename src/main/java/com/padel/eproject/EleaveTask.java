package com.padel.eproject;

import com.padel.eproject.EleaveTask;
import com.padel.eproject.EmailMessage;
import com.padel.eproject.EServicesApplication;
import com.padel.eproject.model.*;
import com.padel.eproject.repository.LeaveInfoUpdateRepository;
import com.padel.eproject.service.*;
import com.padel.eproject.service.LeaveTypeService;
import com.padel.eproject.serviceImpl.LeaveInfoUpdateServiceImpl;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class EleaveTask {
    private static final Logger log = LoggerFactory.getLogger(EleaveTask.class);

    private static DecimalFormat df2 = new DecimalFormat("#.##");
    private static DecimalFormat df3 = new DecimalFormat("#");

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    private LeaveService ls;

    @Autowired
    private StaffService st;

    @Autowired
    private CheckService ck;

    @Autowired
    private EServicesApplication sendService;

    @Autowired
    private SuperviseService spService;

    @Autowired
    private StaffJoinedService sj;

    @Autowired
    private LeaveScheduleService lch;

    @Autowired
    private LeaveInfoService leaveinfoservice;

    @Autowired
    private LeaveInfoUpdateServiceImpl leaveinfoupdate;

    @Autowired
    private LeaveTypeService leaveTypeService;

    //@Scheduled(fixedRate = 50000)
    @Scheduled(cron = "0 0 9 * * *")
    public void checkUnResponseLeave() throws ParseException, AddressException, MessagingException, IOException {
        Date today = new Date();
        List<LeaveMasterEntity> sv = this.ls.getUnapproveLeave();
        int in = 0;
        for (LeaveMasterEntity d : sv) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date dateApply = formatter.parse(d.getDateapply());
            if (dateApply.compareTo(today) < 0 && !dateFormat.format(dateApply).equals(dateFormat.format(today))) {
                log.info("id -- {}", d.getLeaveId());
                CoStaffEntity co = this.st.findByStaffid(d.getStaffId());
                String toReceiverID = "";
                SuperviseInfoEntity sp = this.spService.getSuperViseInfo(d.getStaffId());

                if (d.getStatus().equals("Checked")) {
                    toReceiverID = sp.getSupervisorId();
                } else if (d.getStatus().equals("Supported")) {
                    toReceiverID = sp.getHeadId();
                }
                log.info("toReceiverID -- {}", toReceiverID);
                if (!d.getStatus().equals("Preparing")) {
                    if(toReceiverID != null) {
                        if(!toReceiverID.equals("")) {
                            CoStaffEntity coReceiver = this.st.findByStaffid(toReceiverID);
                            String body = emailTemplate(d, co, coReceiver, d.getStatus());
                            EmailMessage em = new EmailMessage();
                            em.setSenderEmail("ecuti@lcsb.com.my");
                            em.setSenderPassword("@dminEcuti");
                            em.setSubject("[Peringatan] Permohonan Cuti");
                            em.setTo_address(coReceiver.getEmail());
                            //em.setTo_address("fadhilfahmi@lcsb.com.my");
                            em.setBody(body);
                            this.sendService.sendmail(em);
                            log.info("status -- {}", d.getStatus());
                            log.info("Pemohon @ {}", co.getName());
                            log.info("Superivise @ {}", coReceiver.getName());
                            log.info("Email sent @ {}", coReceiver.getEmail());
                        }
                    }
                }
            }
        }
    }

    public String emailTemplate(LeaveMasterEntity lm, CoStaffEntity co, CoStaffEntity coReceiver, String action) {
        String ayattindakan = "Anda mempunyai beberapa perkara yang perlu diambil tindakan :";
        String buttonTitle = "";
        String actiontext = "";
        if (action.equals("Approved")) {
            ayattindakan = "Cuti anda telah DILULUSKAN!";
            buttonTitle = "Lihat Cuti";
        } else if (action.equals("Preparing")) {
            buttonTitle = "Semak Cuti";
            actiontext = "DISEMAK";
        } else if (action.equals("Checked")) {
            buttonTitle = "Sokong Cuti";
            actiontext = "DISOKONG";
        } else if (action.equals("Supported")) {
            buttonTitle = "Lulus Cuti";
            actiontext = "DILULUSKAN";
        } else if (action.equals("Rejected")) {
            ayattindakan = "Cuti anda TIDAK DILULUSKAN";
            buttonTitle = "Lihat Cuti";
        }
        String template = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns:v=\"urn:schemas-microsoft-com:vml\">\n\n<head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n    <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0;\" />\n    <!--[if !mso]--><!-- -->\n    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel=\"stylesheet\">\n    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel=\"stylesheet\">\n    <!--<![endif]-->\n\n    <title>Material Design for Bootstrap</title>\n\n    <style type=\"text/css\">\n        body {\n            width: 100%;\n            background-color: #ffffff;\n            margin: 0;\n            padding: 0;\n            -webkit-font-smoothing: antialiased;\n            mso-margin-top-alt: 0px;\n            mso-margin-bottom-alt: 0px;\n            mso-padding-alt: 0px 0px 0px 0px;\n        }\n\n        p,\n        h1,\n        h2,\n        h3,\n        h4 {\n            margin-top: 0;\n            margin-bottom: 0;\n            padding-top: 0;\n            padding-bottom: 0;\n            margin-left: 20px;\n            margin-right: 0;\n\n        }\n\n        span.preheader {\n            display: none;\n            font-size: 1px;\n        }\n\n        html {\n            width: 100%;\n        }\n\n        table {\n            font-size: 14px;\n            border: 0;\n        }\n\n.img-circular{\n width: 60px;\n height: 60px;\n background-image: url('https://lh3.googleusercontent.com/a-/AOh14GhhkFS82FtQpzPRV4_qRpHLn39S3ftJ8z62Ehn2Nw=s96-c');\n background-size: cover;\n display: block;\n border-radius: 100px;\n -webkit-border-radius: 100px;\n -moz-border-radius: 100px;\n}\n\n.img-circular2{\n width: 60px;\n height: 60px;\n background-image: url('"
                + co.getImageUrl()
                + "');\n background-size: cover;\n display: block;\n border-radius: 100px;\n -webkit-border-radius: 100px;\n -moz-border-radius: 100px;\n}\n\n        /* ----------- responsivity ----------- */\n\n        @media only screen and (max-width: 640px) {\n            /*------ top header ------ */\n            .main-header {\n                font-size: 20px !important;\n            }\n            .main-section-header {\n                font-size: 28px !important;\n            }\n            .show {\n                display: block !important;\n            }\n            .hide {\n                display: none !important;\n            }\n            .align-center {\n                text-align: center !important;\n            }\n            .no-bg {\n                background: none !important;\n            }\n            /*----- main image -------*/\n            .main-image img {\n                width: 440px !important;\n                height: auto !important;\n            }\n            /* ====== divider ====== */\n            .divider img {\n                width: 440px !important;\n            }\n            /*-------- container --------*/\n            .container590 {\n                width: 440px !important;\n            }\n            .container580 {\n                width: 400px !important;\n            }\n            .main-button {\n                width: 220px !important;\n            }\n            /*-------- secions ----------*/\n            .section-img img {\n                width: 320px !important;\n                height: auto !important;\n            }\n            .team-img img {\n                width: 100% !important;\n                height: auto !important;\n            }\n        }\n\n        @media only screen and (max-width: 479px) {\n            /*------ top header ------ */\n            .main-header {\n                font-size: 18px !important;\n            }\n            .main-section-header {\n                font-size: 26px !important;\n            }\n            /* ====== divider ====== */\n            .divider img {\n                width: 280px !important;\n            }\n            /*-------- container --------*/\n            .container590 {\n                width: 280px !important;\n            }\n            .container590 {\n                width: 280px !important;\n            }\n            .container580 {\n                width: 260px !important;\n            }\n            /*-------- secions ----------*/\n            .section-img img {\n                width: 280px !important;\n                height: auto !important;\n            }\n        }\n    </style>\n    <!--[if gte mso 9]><style type=”text/css”>\n        body {\n        font-family: arial, sans-serif!important;\n        }\n        </style>\n    <![endif]-->\n</head>\n\n\n<body class=\"respond\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n    <!-- pre-header -->\n    <!--<table style=\"display:none!important;\">\n        <tr>\n            <td>\n                <div style=\"overflow:hidden;display:none;font-size:1px;color:#ffffff;line-height:1px;font-family:Arial;maxheight:0px;max-width:0px;opacity:0;\">\n                    Welcome to MDB!\n                </div>\n            </td>\n        </tr>\n    </table>-->\n    <!-- pre-header end -->\n    <!-- header -->\n    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\">\n\n        <tr>\n            <td align=\"center\">\n                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                    <tr>\n                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"center\">\n\n                            <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                                <tr>\n                        <td align=\"center\" style=\"color: #343434; font-size: 40px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n                            class=\"main-header\">\n                            <!-- section text ======-->\n\n                            <div style=\"line-height: 35px\">\n\n                                e<span style=\"color: #008000;\">Cuti</span>\n\n                            </div>\n                                    </td>\n                                </tr>\n\n                                <tr>\n                                    <td align=\"center\">\n                                        <table width=\"360 \" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;\"\n                                            class=\"container590 hide\">\n                                            <tr>\n                                                <td width=\"120\" align=\"center\" style=\"font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                                    Sistem Permohonan Cuti Kakitangan LCSB\n                                                </td>\n                                            </tr>\n                                        </table>\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n                    </tr>\n\n                </table>\n            </td>\n        </tr>\n    </table>\n    <!-- end header -->\n\n    <!-- big image section -->\n\n    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\" class=\"bg_color\" >\n\n        <tr>\n            <td align=\"center\">\n                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\" style=\"background-color: #f0f5f5\">\n\n                    <tr>\n                        <td align=\"center\" style=\"color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n                            class=\"main-header\">\n                            <!-- section text ======-->\n                            \n                            <!--<div class=\"img-circular\"></div>-->\n                            <div style=\"line-height: 35px\"><h4>Assalamualaikum <span style=\"color: #008000;\"> "
                + coReceiver.getName()
                + " </span></h4>\n\n                            </div>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"center\">\n                            <table border=\"0\" width=\"40\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"eeeeee\">\n                                <tr>\n                                    <td height=\"2\" style=\"font-size: 2px; line-height: 2px;\">&nbsp;</td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"20\" style=\"font-size: 20px; line-height: 20px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"left\">\n                            <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n                                <tr>\n                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                        <!-- section text ======-->\n\n                                        <p style=\"line-height: 24px; margin-bottom:15px;\">\n\n                                            Tuan/Puan,\n\n                                        </p>\n                                        <p style=\"line-height: 24px;margin-bottom:15px;\">\n                                            Anda mempunyai satu permohonan cuti untuk "
                + actiontext
                + ".\n                                        </p>\n                                       \n                                        \n                                        \n                                    </td>\n                                </tr>\n                                <tr>\n                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                        <!-- section text ======-->\n\n                                       <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n                                        \n                                        <tr>\n                                                <td colspan=\"3\" align=\"center\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><div class=\"img-circular2\"></div></td>\n                                                \n                                            </tr>\n                                        \t<tr>\n                                \t\t\t\t<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Nama Pemohon</strong></p></td>\n                                \t\t\t\t<td align=\"center\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n                                \t\t\t\t<td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>"
                + co.getName()
                + "</strong></p></td>\n                                \t\t\t</tr>\n                                \t\t\t<tr>\n                                \t\t\t\t<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Jenis Cuti</strong></p></td>\n                                \t\t\t\t<td align=\"center\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n                                \t\t\t\t<td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>"
                + lm.getType() + " - " + leaveTypeService.getLeaveTypeData(lm.getType()).getName()
                + "</strong></p></td>\n                                \t\t\t</tr>\n                                            <tr>\n                                                <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Tarikh Bercuti</strong></p></td>\n                                                <td align=\"center\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n                                                <td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>"
                + lm.getDatestart() + " hingga " + lm.getDateend()
                + "</strong></p></td>\n                                            </tr>\n                                            <tr>\n                                                <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Sebab Bercuti</strong></p></td>\n                                                <td align=\"center\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n                                                <td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>"
                + lm.getReason()
                + "</strong></p></td>\n                                            </tr>\n                                            \n                                \t\t</table>\n                                \t\t<p>&nbsp;</p>\n                                \t\t <p style=\"line-height: 24px; margin-bottom:20px;\">\n                                            Anda boleh mengakses cuti dan rekod dengan menekan butang seperti di bawah.\n                                        </p>\n                                        \n                                        \n                                    </td>\n                                </tr>\n                                <tr>\n                                \t<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                \t\t<br>\n                                \t\t<table border=\"0\" align=\"center\" width=\"180\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#008000\" style=\"margin-bottom:20px;\">\n\n                                            <tr>\n                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n                                            </tr>\n\n                                            <tr>\n                                                <td align=\"center\" style=\"color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 22px; letter-spacing: 2px;\">\n                                                    <!-- main section button -->\n\n                                                    <div style=\"line-height: 22px;\">\n                                                        <a href=\"https://ecuti.lcsb.com.my/Login?linkTo=viewleaveapproval&leaveID="
                + lm.getLeaveId() + "\" style=\"color: #ffffff; text-decoration: none;\">" + buttonTitle
                + "</a>\n                                                    </div>\n                                                </td>\n                                            </tr>\n\n                                            <tr>\n                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n                                            </tr>\n\n                                        </table>\n                                \t\t<p>&nbsp;</p>\n                                \t\t<p style=\"line-height: 24px\">\n                                            Yang Benar,\n                                        </p>\n\t\t\t\t\t\t\t\t\t\t<p style=\"line-height: 24px\">\n                                            <strong>LCSB Bot</strong>\n                                        </p>\n                                        <p style=\"line-height: 24px\">\n                                            Pentadbir Maya Sistem eCuti\n                                        </p>\n\n                                \t</td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n\n\n\n\n                </table>\n\n            </td>\n        </tr>\n\n        <tr>\n            <td height=\"40\" style=\"font-size: 40px; line-height: 40px;\">&nbsp;</td>\n        </tr>\n\n    </table>\n\n    <!-- end section -->\n\n\n    <!-- main section -->\n    \n\n    <!-- end section -->\n\n    \n    <!-- end section -->\n\n    <!-- footer ====== -->\n    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;\"> <tr> <td align=\"center\" valign=\"top\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td align=\"center\" valign=\"top\"> <div style=\"height: 34px; line-height: 34px; font-size: 32px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#868686\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #868686; font-size: 17px; line-height: 20px;\">Copyright &copy; 2019 LCSB. All&nbsp;Rights&nbsp;Reserved.</span> </font> <div style=\"height: 3px; line-height: 3px; font-size: 1px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#1a1a1a\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px;\"><a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">sysadmin@lcsb.com.my</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">09-5165180</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"http://www.lcsb.com.my\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">www.lcsb.com.my</a></span> </font> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"> <tr> <td align=\"center\" valign=\"top\"> <a href=\"#\" target=\"_blank\" style=\"display: block; max-width: 19px;\"> <img src=\"https://docs.google.com/uc?id=1NomvumBJQqjFi0WYxP45K_RLRSXfZXLP\" alt=\"img\" width=\"20\" border=\"0\" style=\"display: block; width: 20;\" /> </a> </td> <td><span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif;font-size: 12px\"> &nbsp; LKPP CORPORATION SDN BHD</span></td> </tr> </table> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> </td> </tr> </table>\n    <!-- end footer ====== -->\n\n</body>\n\n</html>";
        return template;
    }

    public String emailTemplateDailyLeave(int sv, List<String> arrImg, CoStaffEntity coReceiver) {
        String template = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns:v=\"urn:schemas-microsoft-com:vml\">\n\n<head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n    <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0;\" />\n    <!--[if !mso]--><!-- -->\n    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel=\"stylesheet\">\n    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel=\"stylesheet\">\n    <!--<![endif]-->\n\n    <title>Material Design for Bootstrap</title>\n\n    <style type=\"text/css\">\n        body {\n            width: 100%;\n            background-color: #ffffff;\n            margin: 0;\n            padding: 0;\n            -webkit-font-smoothing: antialiased;\n            mso-margin-top-alt: 0px;\n            mso-margin-bottom-alt: 0px;\n            mso-padding-alt: 0px 0px 0px 0px;\n        }\n\n        p,\n        h1,\n        h2,\n        h3,\n        h4 {\n            margin-top: 0;\n            margin-bottom: 0;\n            padding-top: 0;\n            padding-bottom: 0;\n            margin-left: 20px;\n            margin-right: 0;\n\n        }\n\n        span.preheader {\n            display: none;\n            font-size: 1px;\n        }\n\n        html {\n            width: 100%;\n        }\n\n        table {\n            font-size: 14px;\n            border: 0;\n        }\n\n.img-circular{\n width: 60px;\n height: 60px;\n background-image: url('https://lh3.googleusercontent.com/a-/AOh14GhhkFS82FtQpzPRV4_qRpHLn39S3ftJ8z62Ehn2Nw=s96-c');\n background-size: cover;\n display: block;\n border-radius: 100px;\n -webkit-border-radius: 100px;\n -moz-border-radius: 100px;\n}\n\n.img-circular2{\n width: 80px;\n height: 80px;\n background-image: url('https://lh3.googleusercontent.com/a-/AOh14GhhkFS82FtQpzPRV4_qRpHLn39S3ftJ8z62Ehn2Nw=s96-c');\n background-size: cover;\n display: block;\n border-radius: 100px;\n -webkit-border-radius: 100px;\n -moz-border-radius: 100px;\n}\n\n        /* ----------- responsivity ----------- */\n\n        @media only screen and (max-width: 640px) {\n            /*------ top header ------ */\n            .main-header {\n                font-size: 20px !important;\n            }\n            .main-section-header {\n                font-size: 28px !important;\n            }\n            .show {\n                display: block !important;\n            }\n            .hide {\n                display: none !important;\n            }\n            .align-center {\n                text-align: center !important;\n            }\n            .no-bg {\n                background: none !important;\n            }\n            /*----- main image -------*/\n            .main-image img {\n                width: 440px !important;\n                height: auto !important;\n            }\n            /* ====== divider ====== */\n            .divider img {\n                width: 440px !important;\n            }\n            /*-------- container --------*/\n            .container590 {\n                width: 440px !important;\n            }\n            .container580 {\n                width: 400px !important;\n            }\n            .main-button {\n                width: 220px !important;\n            }\n            /*-------- secions ----------*/\n            .section-img img {\n                width: 320px !important;\n                height: auto !important;\n            }\n            .team-img img {\n                width: 100% !important;\n                height: auto !important;\n            }\n        }\n\n        @media only screen and (max-width: 479px) {\n            /*------ top header ------ */\n            .main-header {\n                font-size: 18px !important;\n            }\n            .main-section-header {\n                font-size: 26px !important;\n            }\n            /* ====== divider ====== */\n            .divider img {\n                width: 280px !important;\n            }\n            /*-------- container --------*/\n            .container590 {\n                width: 280px !important;\n            }\n            .container590 {\n                width: 280px !important;\n            }\n            .container580 {\n                width: 260px !important;\n            }\n            /*-------- secions ----------*/\n            .section-img img {\n                width: 280px !important;\n                height: auto !important;\n            }\n        }\n    </style>\n    <!--[if gte mso 9]><style type=”text/css”>\n        body {\n        font-family: arial, sans-serif!important;\n        }\n        </style>\n    <![endif]-->\n</head>\n\n\n<body class=\"respond\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n    <!-- pre-header -->\n    <!--<table style=\"display:none!important;\">\n        <tr>\n            <td>\n                <div style=\"overflow:hidden;display:none;font-size:1px;color:#ffffff;line-height:1px;font-family:Arial;maxheight:0px;max-width:0px;opacity:0;\">\n                    Welcome to MDB!\n                </div>\n            </td>\n        </tr>\n    </table>-->\n    <!-- pre-header end -->\n    <!-- header -->\n    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\">\n\n        <tr>\n            <td align=\"center\">\n                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                    <tr>\n                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"center\">\n\n                            <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                                <tr>\n                        <td align=\"center\" style=\"color: #343434; font-size: 40px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n                            class=\"main-header\">\n                            <!-- section text ======-->\n\n                            <div style=\"line-height: 35px\">\n\n                                e<span style=\"color: #008000;\">Cuti</span>\n\n                            </div>\n                                    </td>\n                                </tr>\n\n                                <tr>\n                                    <td align=\"center\">\n                                        <table width=\"360 \" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;\"\n                                            class=\"container590 hide\">\n                                            <tr>\n                                                <td width=\"120\" align=\"center\" style=\"font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                                    Sistem Permohonan Cuti Kakitangan LCSB\n                                                </td>\n                                            </tr>\n                                        </table>\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n                    </tr>\n\n                </table>\n            </td>\n        </tr>\n    </table>\n    <!-- end header -->\n\n    <!-- big image section -->\n\n    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\" class=\"bg_color\" >\n\n        <tr>\n            <td align=\"center\">\n                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\" >\n\n                    <tr>\n                        <td align=\"center\" style=\"color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n                            class=\"main-header\">\n                            <!-- section text ======-->\n                            \n                            <!--<div class=\"img-circular\"></div>-->\n                            <div style=\"line-height: 35px\"><h4>Assalamualaikum <span style=\"color: #008000;\"> "
                + coReceiver.getName()
                + " </span></h4>\n\n                            </div>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"center\">\n                            <table border=\"0\" width=\"40\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"eeeeee\">\n                                <tr>\n                                    <td height=\"2\" style=\"font-size: 2px; line-height: 2px;\">&nbsp;</td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"20\" style=\"font-size: 20px; line-height: 20px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"left\">\n                            <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n                                <tr>\n                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                        <!-- section text ======-->\n\n                                        <p style=\"line-height: 24px; margin-bottom:15px;\">\n\n                                            Tuan/Puan,\n\n                                        </p>\n                                        <p style=\"line-height: 24px;margin-bottom:15px;\">\n                                            Berikut adalah senarai anggota perkhidmatan yang bercuti pada hari ini.\n                                        </p>\n                                       \n                                        \n                                        \n                                    </td>\n                                </tr>\n                                <tr>\n                                    <td align=\"center\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                        <!-- section text ======-->\n\n                                       <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"10\" cellspacing=\"10\" class=\"container590\">\n                                        \n";
        int personLeave = sv;
        int maxPerRow = 4;
        int totalRow = 0;
        int rowCount = personLeave / maxPerRow;
        int lastRow = personLeave % maxPerRow;
        if (lastRow > 0) {
            totalRow = rowCount + 1;
        } else {
            totalRow = rowCount;
        }
        for (int i = 1; i <= totalRow; i++) {
            template = template + "                              <tr>\n";
            if (i == totalRow) {
                int cnt = 0;
                if (lastRow > 0) {
                    cnt = lastRow;
                } else {
                    cnt = maxPerRow;
                }
                for (int j = 1; j <= cnt; j++)
                    template = template
                            + "                                            <td align=\"center\" colspan=\"3\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\">\n                                                <div>\n                                                \t<img class=\"img-circular2\" src=\""
                            + (String) arrImg.get((i - 1) * maxPerRow + j - 1)
                            + "\">\n                                                \t </div>\n                                               \n                                            </td>\n";
            } else {
                for (int j = 1; j <= maxPerRow; j++)
                    template = template
                            + "                                            <td align=\"center\" colspan=\"3\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\">\n                                                <div>\n                                                \t<img class=\"img-circular2\" src=\""
                            + (String) arrImg.get((i - 1) * maxPerRow + j - 1)
                            + "\">\n                                                \t </div>\n                                               \n                                            </td>\n";
            }
            template = template + "                                            </tr>\n";
        }
        template = template
                + "                                        \t\n                                            \n                                \t\t</table>\n                                \t\t<p>&nbsp;</p>\n                                \t\t <p style=\"line-height: 24px; margin-bottom:20px;\">\n                                            Anda boleh melihat dan menguruskan cuti anda dengan menekan butang seperti di bawah.\n                                        </p>\n                                        \n                                        \n                                    </td>\n                                </tr>\n                                <tr>\n                                \t<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                \t\t<br>\n                                \t\t<table border=\"0\" align=\"center\" width=\"180\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#008000\" style=\"margin-bottom:20px;\">\n\n                                            <tr>\n                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n                                            </tr>\n\n                                            <tr>\n                                                <td align=\"center\" style=\"color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 22px; letter-spacing: 2px;\">\n                                                    <!-- main section button -->\n\n                                                    <div style=\"line-height: 22px;\">\n                                                        <a href=\"https://ecuti.lcsb.com.my\" style=\"color: #ffffff; text-decoration: none;\">Pergi ke eCuti</a>\n                                                    </div>\n                                                </td>\n                                            </tr>\n\n                                            <tr>\n                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n                                            </tr>\n\n                                        </table>\n                                \t\t<p>&nbsp;</p>\n                                \t\t<p style=\"line-height: 24px\">\n                                            Yang Benar,\n                                        </p>\n\t\t\t\t\t\t\t\t\t\t<p style=\"line-height: 24px\">\n                                            <strong>LCSB Bot</strong>\n                                        </p>\n                                        <p style=\"line-height: 24px\">\n                                            Pentadbir Maya Sistem eCuti\n                                        </p>\n\n                                \t</td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n\n\n\n\n                </table>\n\n            </td>\n        </tr>\n\n        <tr>\n            <td height=\"40\" style=\"font-size: 40px; line-height: 40px;\">&nbsp;</td>\n        </tr>\n\n    </table>\n\n    <!-- end section -->\n\n\n    <!-- main section -->\n    \n\n    <!-- end section -->\n\n    \n    <!-- end section -->\n\n    <!-- footer ====== -->\n    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;\"> <tr> <td align=\"center\" valign=\"top\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td align=\"center\" valign=\"top\"> <div style=\"height: 34px; line-height: 34px; font-size: 32px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#868686\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #868686; font-size: 17px; line-height: 20px;\">Copyright &copy; 2019 LCSB. All&nbsp;Rights&nbsp;Reserved.</span> </font> <div style=\"height: 3px; line-height: 3px; font-size: 1px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#1a1a1a\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px;\"><a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">sysadmin@lcsb.com.my</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">09-5165180</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"http://www.lcsb.com.my\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">www.lcsb.com.my</a></span> </font> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"> <tr> <td align=\"center\" valign=\"top\"> <a href=\"#\" target=\"_blank\" style=\"display: block; max-width: 19px;\"> <img src=\"https://docs.google.com/uc?id=1NomvumBJQqjFi0WYxP45K_RLRSXfZXLP\" alt=\"img\" width=\"20\" border=\"0\" style=\"display: block; width: 20;\" /> </a> </td> <td><span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif;font-size: 12px\"> &nbsp; LKPP CORPORATION SDN BHD</span></td> </tr> </table> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> </td> </tr> </table>\n    <!-- end footer ====== -->\n\n</body>\n\n</html>";
        return template;
    }

    //@Scheduled(fixedRate = 5000)
    @Scheduled(cron = "0 0 0 * * *")
    public void genapTahun() throws ParseException, AddressException, MessagingException, IOException {

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

        Date todayDate = dateFormatter.parse(dateFormatter.format(new Date()));

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        // log.info("today -- {}", formatter.parse(today));
        List<StaffJoinedEntity> sv = this.sj.findAll();
        int in = 0;
        for (StaffJoinedEntity d : sv) {

            // if (d.getDatejoined().equals("2020-11-17")) {
            // log.info("datejoined -- {}", d.getDatejoined());
            // log.info("today -- {}", today);

            Date datejoined = formatter.parse(d.getDatejoined());

            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            cal1.setTime(datejoined);
            cal2.setTime(todayDate);

            if (cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)
                    && cal1.get(Calendar.DATE) == cal2.get(Calendar.DATE)) {
                in++;
                log.info("sama -- {}", "yes");
                log.info("datejoined -- {}", d.getDatejoined());
                log.info("today -- {}", todayDate);

                int years = getDiffYears(datejoined, todayDate);
                log.info("year -- {}", years);
                // log.info("post -- {}", le.getType());

                CoStaffEntity co = this.st.findByStaffid(d.getStaffid());
                LeaveScheduleEntity le = lch.findByPostId(co.getPostId());

                int getMonthBefore = cal1.get(Calendar.MONTH);
                int getMonthAfter = 12 - cal1.get(Calendar.MONTH);

                double ab = 0;

                int oldbal = 0;
                int newbal = 0;

                if (le.getType() == 1) {

                    if (years == 2) {

                        oldbal = le.getLevel1();
                        newbal = le.getLevel2();

                        ab = getNewBalance(oldbal, newbal, getMonthBefore, getMonthAfter);

                    } else if (years == 10) {

                        oldbal = le.getLevel2();
                        newbal = le.getLevel3();

                        ab = getNewBalance(oldbal, newbal, getMonthBefore, getMonthAfter);

                    }

                }

                if (le.getType() == 2) {

                    if (years == 10) {

                        oldbal = le.getLevel2();
                        newbal = le.getLevel3();

                        ab = getNewBalance(oldbal, newbal, getMonthBefore, getMonthAfter);

                    }

                }

                if (le.getType() == 3) {

                    if (years == 5) {

                        oldbal = le.getLevel1();
                        newbal = le.getLevel2();

                        ab = getNewBalance(oldbal, newbal, getMonthBefore, getMonthAfter);

                    } else if (years == 10) {

                        oldbal = le.getLevel1();
                        newbal = le.getLevel2();

                        ab = getNewBalance(oldbal, newbal, getMonthBefore, getMonthAfter);

                    }

                }

                int IntValue = (int) ab;

                leaveinfoservice.updateNewBalance(IntValue, d.getStaffid(), String.valueOf(cal2.get(Calendar.YEAR)));
                leaveinfoupdate.insertTransactionBalance(d.getStaffid(), String.valueOf(cal2.get(Calendar.YEAR)), dateFormat.format(todayDate), oldbal, IntValue, years);

                log.info("{} ----------------------------------------- ", in);

                CoStaffEntity coReceiver = this.st.findByStaffid(d.getStaffid());
                //String body = emailTemplateNewBalance(d, co, coReceiver, d.getStatus());
                EmailMessage em = new EmailMessage();
                em.setSenderEmail("ecuti@lcsb.com.my");
                em.setSenderPassword("@dminEcuti");
                em.setSubject("[Peringatan] Permohonan Cuti");
                em.setTo_address(coReceiver.getEmail());
                //em.setBody(body);
                this.sendService.sendmail(em);
                //log.info("status -- {}", d.getStatus());
                log.info("Pemohon @ {}", co.getName());
                log.info("Superivise @ {}", coReceiver.getName());
                log.info("Email sent @ {}", coReceiver.getEmail());
            }

            // if (datejoined.compareTo(todayDate) == 0) {
            // }
            // }

        }
    }

    public static double getNewBalance(double lvl1, double lvl2, int getMonthBefore, int getMonthAfter) {

        double a = lvl1 * getMonthBefore / 12;
        double b = lvl2 * getMonthAfter / 12;
        double ab = a + b;

        log.info("getMonthBefore -- {}", getMonthBefore);
        log.info("a -- {}", df2.format(a));

        log.info("getMonthAfter -- {}", getMonthAfter);
        log.info("b -- {}", df2.format(b));

        log.info("ab -- {}", df3.format(ab));

        return Double.parseDouble(df3.format(ab));
    }

    public static int getDiffYears(Date first, Date last) {
        Calendar a = getCalendar(first);
        Calendar b = getCalendar(last);
        int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
        if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH)
                || (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
            diff--;
        }
        return diff;
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }




//	public String emailTemplateNewBalance(LeaveMasterEntity lm, , CoStaffEntity coReceiver, String action) {
//		String ayattindakan = "Anda mempunyai beberapa perkara yang perlu diambil tindakan :";
//		String buttonTitle = "";
//		String actiontext = "";
//		
//		String template = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns:v=\"urn:schemas-microsoft-com:vml\">\n\n<head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n    <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0;\" />\n    <!--[if !mso]--><!-- -->\n    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel=\"stylesheet\">\n    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel=\"stylesheet\">\n    <!--<![endif]-->\n\n    <title>Material Design for Bootstrap</title>\n\n    <style type=\"text/css\">\n        body {\n            width: 100%;\n            background-color: #ffffff;\n            margin: 0;\n            padding: 0;\n            -webkit-font-smoothing: antialiased;\n            mso-margin-top-alt: 0px;\n            mso-margin-bottom-alt: 0px;\n            mso-padding-alt: 0px 0px 0px 0px;\n        }\n\n        p,\n        h1,\n        h2,\n        h3,\n        h4 {\n            margin-top: 0;\n            margin-bottom: 0;\n            padding-top: 0;\n            padding-bottom: 0;\n            margin-left: 20px;\n            margin-right: 0;\n\n        }\n\n        span.preheader {\n            display: none;\n            font-size: 1px;\n        }\n\n        html {\n            width: 100%;\n        }\n\n        table {\n            font-size: 14px;\n            border: 0;\n        }\n\n.img-circular{\n width: 60px;\n height: 60px;\n background-image: url('https://lh3.googleusercontent.com/a-/AOh14GhhkFS82FtQpzPRV4_qRpHLn39S3ftJ8z62Ehn2Nw=s96-c');\n background-size: cover;\n display: block;\n border-radius: 100px;\n -webkit-border-radius: 100px;\n -moz-border-radius: 100px;\n}\n\n.img-circular2{\n width: 60px;\n height: 60px;\n background-image: url('"
//				+ co.getImageUrl()
//				+ "');\n background-size: cover;\n display: block;\n border-radius: 100px;\n -webkit-border-radius: 100px;\n -moz-border-radius: 100px;\n}\n\n        /* ----------- responsivity ----------- */\n\n        @media only screen and (max-width: 640px) {\n            /*------ top header ------ */\n            .main-header {\n                font-size: 20px !important;\n            }\n            .main-section-header {\n                font-size: 28px !important;\n            }\n            .show {\n                display: block !important;\n            }\n            .hide {\n                display: none !important;\n            }\n            .align-center {\n                text-align: center !important;\n            }\n            .no-bg {\n                background: none !important;\n            }\n            /*----- main image -------*/\n            .main-image img {\n                width: 440px !important;\n                height: auto !important;\n            }\n            /* ====== divider ====== */\n            .divider img {\n                width: 440px !important;\n            }\n            /*-------- container --------*/\n            .container590 {\n                width: 440px !important;\n            }\n            .container580 {\n                width: 400px !important;\n            }\n            .main-button {\n                width: 220px !important;\n            }\n            /*-------- secions ----------*/\n            .section-img img {\n                width: 320px !important;\n                height: auto !important;\n            }\n            .team-img img {\n                width: 100% !important;\n                height: auto !important;\n            }\n        }\n\n        @media only screen and (max-width: 479px) {\n            /*------ top header ------ */\n            .main-header {\n                font-size: 18px !important;\n            }\n            .main-section-header {\n                font-size: 26px !important;\n            }\n            /* ====== divider ====== */\n            .divider img {\n                width: 280px !important;\n            }\n            /*-------- container --------*/\n            .container590 {\n                width: 280px !important;\n            }\n            .container590 {\n                width: 280px !important;\n            }\n            .container580 {\n                width: 260px !important;\n            }\n            /*-------- secions ----------*/\n            .section-img img {\n                width: 280px !important;\n                height: auto !important;\n            }\n        }\n    </style>\n    <!--[if gte mso 9]><style type=”text/css”>\n        body {\n        font-family: arial, sans-serif!important;\n        }\n        </style>\n    <![endif]-->\n</head>\n\n\n<body class=\"respond\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n    <!-- pre-header -->\n    <!--<table style=\"display:none!important;\">\n        <tr>\n            <td>\n                <div style=\"overflow:hidden;display:none;font-size:1px;color:#ffffff;line-height:1px;font-family:Arial;maxheight:0px;max-width:0px;opacity:0;\">\n                    Welcome to MDB!\n                </div>\n            </td>\n        </tr>\n    </table>-->\n    <!-- pre-header end -->\n    <!-- header -->\n    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\">\n\n        <tr>\n            <td align=\"center\">\n                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                    <tr>\n                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"center\">\n\n                            <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                                <tr>\n                        <td align=\"center\" style=\"color: #343434; font-size: 40px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n                            class=\"main-header\">\n                            <!-- section text ======-->\n\n                            <div style=\"line-height: 35px\">\n\n                                e<span style=\"color: #008000;\">Cuti</span>\n\n                            </div>\n                                    </td>\n                                </tr>\n\n                                <tr>\n                                    <td align=\"center\">\n                                        <table width=\"360 \" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;\"\n                                            class=\"container590 hide\">\n                                            <tr>\n                                                <td width=\"120\" align=\"center\" style=\"font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                                    Sistem Permohonan Cuti Kakitangan LCSB\n                                                </td>\n                                            </tr>\n                                        </table>\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n                    </tr>\n\n                </table>\n            </td>\n        </tr>\n    </table>\n    <!-- end header -->\n\n    <!-- big image section -->\n\n    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\" class=\"bg_color\" >\n\n        <tr>\n            <td align=\"center\">\n                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\" style=\"background-color: #f0f5f5\">\n\n                    <tr>\n                        <td align=\"center\" style=\"color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n                            class=\"main-header\">\n                            <!-- section text ======-->\n                            \n                            <!--<div class=\"img-circular\"></div>-->\n                            <div style=\"line-height: 35px\"><h4>Assalamualaikum <span style=\"color: #008000;\"> "
//				+ coReceiver.getName()
//				+ " </span></h4>\n\n                            </div>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"center\">\n                            <table border=\"0\" width=\"40\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"eeeeee\">\n                                <tr>\n                                    <td height=\"2\" style=\"font-size: 2px; line-height: 2px;\">&nbsp;</td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"20\" style=\"font-size: 20px; line-height: 20px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"left\">\n                            <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n                                <tr>\n                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                        <!-- section text ======-->\n\n                                        <p style=\"line-height: 24px; margin-bottom:15px;\">\n\n                                            Tuan/Puan,\n\n                                        </p>\n                                        <p style=\"line-height: 24px;margin-bottom:15px;\">\n                                            Anda mempunyai satu permohonan cuti untuk "
//				+ actiontext
//				+ ".\n                                        </p>\n                                       \n                                        \n                                        \n                                    </td>\n                                </tr>\n                                <tr>\n                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                        <!-- section text ======-->\n\n                                       <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n                                        \n                                        <tr>\n                                                <td colspan=\"3\" align=\"center\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><div class=\"img-circular2\"></div></td>\n                                                \n                                            </tr>\n                                        \t<tr>\n                                \t\t\t\t<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Penggenapan</strong></p></td>\n                                \t\t\t\t<td align=\"center\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n                                \t\t\t\t<td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>"
//				+ co.getName()
//				+ "</strong></p></td>\n                                \t\t\t</tr>\n                                \t\t\t<tr>\n                                \t\t\t\t<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Jenis Cuti</strong></p></td>\n                                \t\t\t\t<td align=\"center\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n                                \t\t\t\t<td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>"
//				+ lm.getType()
//				+ "</strong></p></td>\n                                \t\t\t</tr>\n                                            <tr>\n                                                <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Tarikh Bercuti</strong></p></td>\n                                                <td align=\"center\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n                                                <td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>"
//				+ lm.getDatestart() + " hingga " + lm.getDateend()
//				+ "</strong></p></td>\n                                            </tr>\n                                            <tr>\n                                                <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 30px;\"><p><strong>Sebab Bercuti</strong></p></td>\n                                                <td align=\"center\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>:&nbsp;</strong></p></td>\n                                                <td align=\"left\" style=\"color: green; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\"><p><strong>"
//				+ lm.getReason()
//				+ "</strong></p></td>\n                                            </tr>\n                                            \n                                \t\t</table>\n                                \t\t<p>&nbsp;</p>\n                                \t\t <p style=\"line-height: 24px; margin-bottom:20px;\">\n                                            Anda boleh mengakses cuti dan rekod dengan menekan butang seperti di bawah.\n                                        </p>\n                                        \n                                        \n                                    </td>\n                                </tr>\n                                <tr>\n                                \t<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                \t\t<br>\n                                \t\t<table border=\"0\" align=\"center\" width=\"180\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#008000\" style=\"margin-bottom:20px;\">\n\n                                            <tr>\n                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n                                            </tr>\n\n                                            <tr>\n                                                <td align=\"center\" style=\"color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 22px; letter-spacing: 2px;\">\n                                                    <!-- main section button -->\n\n                                                    <div style=\"line-height: 22px;\">\n                                                        <a href=\"https://ecuti.lcsb.com.my/Login?linkTo=viewleaveapproval&leaveID="
//				+ lm.getLeaveId() + "\" style=\"color: #ffffff; text-decoration: none;\">" + buttonTitle
//				+ "</a>\n                                                    </div>\n                                                </td>\n                                            </tr>\n\n                                            <tr>\n                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n                                            </tr>\n\n                                        </table>\n                                \t\t<p>&nbsp;</p>\n                                \t\t<p style=\"line-height: 24px\">\n                                            Yang Benar,\n                                        </p>\n\t\t\t\t\t\t\t\t\t\t<p style=\"line-height: 24px\">\n                                            <strong>LCSB Bot</strong>\n                                        </p>\n                                        <p style=\"line-height: 24px\">\n                                            Pentadbir Maya Sistem eCuti\n                                        </p>\n\n                                \t</td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n\n\n\n\n                </table>\n\n            </td>\n        </tr>\n\n        <tr>\n            <td height=\"40\" style=\"font-size: 40px; line-height: 40px;\">&nbsp;</td>\n        </tr>\n\n    </table>\n\n    <!-- end section -->\n\n\n    <!-- main section -->\n    \n\n    <!-- end section -->\n\n    \n    <!-- end section -->\n\n    <!-- footer ====== -->\n    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;\"> <tr> <td align=\"center\" valign=\"top\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td align=\"center\" valign=\"top\"> <div style=\"height: 34px; line-height: 34px; font-size: 32px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#868686\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #868686; font-size: 17px; line-height: 20px;\">Copyright &copy; 2019 LCSB. All&nbsp;Rights&nbsp;Reserved.</span> </font> <div style=\"height: 3px; line-height: 3px; font-size: 1px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#1a1a1a\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px;\"><a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">sysadmin@lcsb.com.my</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">09-5165180</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"http://www.lcsb.com.my\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">www.lcsb.com.my</a></span> </font> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"> <tr> <td align=\"center\" valign=\"top\"> <a href=\"#\" target=\"_blank\" style=\"display: block; max-width: 19px;\"> <img src=\"https://docs.google.com/uc?id=1NomvumBJQqjFi0WYxP45K_RLRSXfZXLP\" alt=\"img\" width=\"20\" border=\"0\" style=\"display: block; width: 20;\" /> </a> </td> <td><span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif;font-size: 12px\"> &nbsp; LKPP CORPORATION SDN BHD</span></td> </tr> </table> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> </td> </tr> </table>\n    <!-- end footer ====== -->\n\n</body>\n\n</html>";
//		return template;
//	}

    //run setiap akhir tahun
    //@Scheduled(fixedRate = 100000)
    @Scheduled(cron = "0 20 12 1 1 *")
    public void genapTahunan() throws ParseException, AddressException, MessagingException, IOException {

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

        Date todayDate = dateFormatter.parse(dateFormatter.format(new Date()));

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        // log.info("today -- {}", formatter.parse(today));
        List<StaffJoinedEntity> sv = this.sj.findAll();
        int in = 0;
        for (StaffJoinedEntity d : sv) {

            CoStaffEntity co = st.findByStaffid(d.getStaffid());

            if (co != null && !co.getEmail().equals("") && co.getStatus().equals("Active")) {
                // if (d.getDatejoined().equals("2020-11-17")) {
                log.info("staff -- {}", d.getStaffid() + " - " + co.getName());
                log.info("datejoined -- {}", d.getDatejoined());

                if (d.getDatejoined() != null) {
                    Date datejoined = formatter.parse(d.getDatejoined());

                    Calendar cal1 = Calendar.getInstance();
                    Calendar cal2 = Calendar.getInstance();
                    cal1.setTime(datejoined);
                    cal2.setTime(todayDate);

                    int years = getDiffYears(datejoined, todayDate);
                    log.info("year -- {}", years);
                    // log.info("post -- {}", le.getType());

                    //CoStaffEntity co = this.st.findByStaffid(d.getStaffid());
                    LeaveScheduleEntity le = lch.findByPostId(co.getPostId());

                    if (le.getType() != null) {


                        int getMonthBefore = cal1.get(Calendar.MONTH);
                        int getMonthAfter = 12 - cal1.get(Calendar.MONTH);

                        double ab = 0;

                        int mc = 0;
                        int newbal = 0;

                        if (le.getType() == 1) {

                            if (years < 2) {


                                newbal = le.getLevel1();



                            } else if (years > 1 && years < 11) {

                                newbal = le.getLevel2();


                            } else if (years > 10) {

                                newbal = le.getLevel3();


                            }

                        }

                        if (le.getType() == 2) {

                            if (years < 10) {
                                newbal = le.getLevel2();
                            }else if (years > 9) {
                                newbal = le.getLevel3();
                            }

                        }

                        if (le.getType() == 3) {

                            if (years == 5) {

                                newbal = le.getLevel2();


                            } else if (years == 10) {

                                newbal = le.getLevel2();

                            }

                        }

                        if(years < 2){
                            mc = 14;
                        }else if(years > 1 && years < 5){
                            mc = 18;
                        }else if(years > 4){
                            mc = 22;
                        }

                        double bal = leaveinfoservice.getBalanceEligibleLeave(d.getStaffid(), "2020");
                        double baloftheyear = 0;

                        if(bal > 0){
                            baloftheyear = bal;
                        }

                        LeaveInfoEntity li = leaveinfoservice.findByStaffIdAndYear(d.getStaffid(), "2020");
                        log.info(" staffidx - {}", d.getStaffid());
                        if(li != null){

                            log.info(" al - {}", newbal);
                            log.info(" mc - {}", mc);
                            log.info(" balprev - {}", baloftheyear);

                            double halfOfEligibleLeave = li.getEligibleleave() / 2.0;

                            log.info(" elgiblecurrent - {}", li.getEligibleleave());
                            log.info(" halfof - {}", Math.round(halfOfEligibleLeave));

                            double bf = 0;

                            if(baloftheyear < halfOfEligibleLeave ){
                                bf = baloftheyear;
                            }else{
                                bf = Math.round(halfOfEligibleLeave);
                            }

                            if(years == 1){
                                bf = baloftheyear;
                            }

                            if(d.getStaffid().equals("P0833")){
                                newbal = (int) baloftheyear;
                                bf = 0;
                            }

                            log.info(" bf - {}", bf);

                            leaveinfoupdate.insertNewLeaveOfNewYear(d.getStaffid(), co.getName(), "2021", newbal, li.getEligibleleave(), (int) bf, (int) halfOfEligibleLeave, d.getDatejoined(), years, (int )baloftheyear, mc);

                            leaveinfoupdate.insertNewLeaveOfNewYearLeaveInfo(d.getStaffid(), newbal, "2021", (int) bf, mc, 60, 60);
                            LeaveInfoModel lm = new LeaveInfoModel();
                            lm.setStaffInfo(co);
                            lm.setBalPrevYearEligibleLeavel((int )baloftheyear);
                            lm.setBf((int) bf);
                            lm.setDatejoined(d.getDatejoined());
                            lm.setEligibleMC(mc);
                            lm.setHalfOfPrevEligibleLeave((int) halfOfEligibleLeave);
                            lm.setNewEligibleLeave(newbal);
                            lm.setYear("2021");
                            lm.setYearsOfService(years);
                            lm.setLeaveInfo(li);
                            lm.setTotalLeaveUseOfTheYear((int) ls.getTotalLeaveUseOfTheYear(d.getStaffid(), "2020"));
                            lm.setBalanceOfTheYearEligibleLeave((int) bal);
                            lm.setTotalLeaveUseMCOfTheYear((int) ls.getTotalSickLeaveUseOfTheYear(d.getStaffid(),"2020"));
                            lm.setTotalLeaveUseHospTheYear((int) ls.getTotalHospLeaveUseOfTheYear(d.getStaffid(), "2020"));



                            //String body = emailTemplateNewBalance(d, co, coReceiver, d.getStatus());
                            EmailMessage em = new EmailMessage();
                            em.setSenderEmail("ecuti@lcsb.com.my");
                            em.setSenderPassword("@dminEcuti");
                            em.setSubject("Tahun Baru 2021");
                            em.setTo_address(co.getEmail());
                            //em.setTo_address("fadhilfahmi@lcsb.com.my");
                            em.setBody(emailTemplateNewYear(co, lm));
                            this.sendService.sendmail(em);
                            //log.info("status -- {}", d.getStatus());
                            log.info("Email sent @ {}", co.getEmail());
                        }
                    }
                }
            }
        }
    }

    public String emailTemplateNewYear(CoStaffEntity co, LeaveInfoModel lm) {

        LeaveInfoEntity li = lm.getLeaveInfo();

        int totalNewEligibleLeave = lm.getNewEligibleLeave()+lm.getBf();
        int totalPrevEligibleLeave = li.getEligibleleave()+li.getBf();
        int balMCLeavePrevYear = li.getMc() - lm.getTotalLeaveUseMCOfTheYear();
        int balHospLeavePrevYear = li.getHosp() - lm.getTotalLeaveUseHospTheYear();

        String template = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns:v=\"urn:schemas-microsoft-com:vml\">\n\n<head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n    <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0;\" />\n    <!--[if !mso]--><!-- -->\n    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel=\"stylesheet\">\n    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel=\"stylesheet\">\n    <!--<![endif]-->\n\n    <title>Material Design for Bootstrap</title>\n\n    <style type=\"text/css\">\n        body {\n            width: 100%;\n            background-color: #ffffff;\n            margin: 0;\n            padding: 0;\n            -webkit-font-smoothing: antialiased;\n            mso-margin-top-alt: 0px;\n            mso-margin-bottom-alt: 0px;\n            mso-padding-alt: 0px 0px 0px 0px;\n        }\n\n        p,\n        h1,\n        h2,\n        h3,\n        h4 {\n            margin-top: 0;\n            margin-bottom: 0;\n            padding-top: 0;\n            padding-bottom: 0;\n            margin-left: 20px;\n            margin-right: 0;\n\n        }\n\n        span.preheader {\n            display: none;\n            font-size: 1px;\n        }\n\n        html {\n            width: 100%;\n        }\n\n        table {\n            font-size: 14px;\n            border: 0;\n        }\n\n.img-circular{\n width: 60px;\n height: 60px;\n background-image: url('https://lh3.googleusercontent.com/a-/AOh14GhhkFS82FtQpzPRV4_qRpHLn39S3ftJ8z62Ehn2Nw=s96-c');\n background-size: cover;\n display: block;\n border-radius: 100px;\n -webkit-border-radius: 100px;\n -moz-border-radius: 100px;\n}\n\n.img-circular2{\n width: 60px;\n height: 60px;\n background-image: url('"
                + co.getImageUrl()
                + "');\n background-size: cover;\n display: block;\n border-radius: 100px;\n -webkit-border-radius: 100px;\n -moz-border-radius: 100px;\n}\n\n        /* ----------- responsivity ----------- */\n\n        @media only screen and (max-width: 640px) {\n            /*------ top header ------ */\n            .main-header {\n                font-size: 20px !important;\n            }\n            .main-section-header {\n                font-size: 28px !important;\n            }\n            .show {\n                display: block !important;\n            }\n            .hide {\n                display: none !important;\n            }\n            .align-center {\n                text-align: center !important;\n            }\n            .no-bg {\n                background: none !important;\n            }\n            /*----- main image -------*/\n            .main-image img {\n                width: 440px !important;\n                height: auto !important;\n            }\n            /* ====== divider ====== */\n            .divider img {\n                width: 440px !important;\n            }\n            /*-------- container --------*/\n            .container590 {\n                width: 440px !important;\n            }\n            .container580 {\n                width: 400px !important;\n            }\n            .main-button {\n                width: 220px !important;\n            }\n            /*-------- secions ----------*/\n            .section-img img {\n                width: 320px !important;\n                height: auto !important;\n            }\n            .team-img img {\n                width: 100% !important;\n                height: auto !important;\n            }\n        }\n\n        @media only screen and (max-width: 479px) {\n            /*------ top header ------ */\n            .main-header {\n                font-size: 18px !important;\n            }\n            .main-section-header {\n                font-size: 26px !important;\n            }\n            /* ====== divider ====== */\n            .divider img {\n                width: 280px !important;\n            }\n            /*-------- container --------*/\n            .container590 {\n                width: 280px !important;\n            }\n            .container590 {\n                width: 280px !important;\n            }\n            .container580 {\n                width: 260px !important;\n            }\n            /*-------- secions ----------*/\n            .section-img img {\n                width: 280px !important;\n                height: auto !important;\n            }\n        }\n    </style>\n    <!--[if gte mso 9]><style type=”text/css”>\n        body {\n        font-family: arial, sans-serif!important;\n        }\n        </style>\n    <![endif]-->\n</head>\n\n\n<body class=\"respond\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">\n    <!-- pre-header -->\n    <!--<table style=\"display:none!important;\">\n        <tr>\n            <td>\n                <div style=\"overflow:hidden;display:none;font-size:1px;color:#ffffff;line-height:1px;font-family:Arial;maxheight:0px;max-width:0px;opacity:0;\">\n                    Welcome to MDB!\n                </div>\n            </td>\n        </tr>\n    </table>-->\n    <!-- pre-header end -->\n    <!-- header -->\n    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\">\n\n        <tr>\n            <td align=\"center\">\n                <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                    <tr>\n                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"center\">\n\n                            <table border=\"0\" align=\"center\" width=\"590\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n\n                                <tr>\n                        <td align=\"center\" style=\"color: #343434; font-size: 40px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n                            class=\"main-header\">\n                            <!-- section text ======-->\n\n                            <div style=\"line-height: 35px\">\n\n                                e<span style=\"color: #008000;\">Cuti</span>\n\n                            </div>\n                                    </td>\n                                </tr>\n\n                                <tr>\n                                    <td align=\"center\">\n                                        <table width=\"360 \" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;\"\n                                            class=\"container590 hide\">\n                                            <tr>\n                                                <td width=\"120\" align=\"center\" style=\"font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                                    Sistem Permohonan Cuti Kakitangan LCSB\n                                                </td>\n                                            </tr>\n                                        </table>\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"25\" style=\"font-size: 25px; line-height: 25px;\">&nbsp;</td>\n                    </tr>\n\n                </table>\n            </td>\n        </tr>\n    </table>\n    <!-- end header -->\n\n    <!-- big image section -->\n\n    <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"ffffff\" class=\"bg_color\" >\n\n        <tr>\n            <td align=\"center\">\n                <table border=\"0\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\" style=\"background-color: #fff\">\n\n                    <tr>\n                        <td align=\"center\" style=\"color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;\"\n                            class=\"main-header\">\n                            <!-- section text ======-->\n                            \n                            <div class=\"img-circular2\"></div>\n                            <div style=\"line-height: 35px\"><h4>Assalamualaikum <span style=\"color: #008000;\"> "
                + co.getName()
                + " </span></h4>\n\n                            </div>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"center\">\n                            <table border=\"0\" width=\"40\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"eeeeee\">\n                                <tr>\n                                    <td height=\"2\" style=\"font-size: 2px; line-height: 2px;\">&nbsp;</td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n                    <tr>\n                        <td height=\"20\" style=\"font-size: 20px; line-height: 20px;\">&nbsp;</td>\n                    </tr>\n\n                    <tr>\n                        <td align=\"left\">\n                            <table border=\"0\" width=\"100%\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n                                <tr>\n                                    <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                        <!-- section text ======-->\n\n                                        <p style=\"line-height: 24px; margin-bottom:15px;\">\n\n                                            Tuan/Puan,\n\n                                        </p>\n                                        <p style=\"line-height: 24px;margin-bottom:15px;\">\n                                            Selamat Tahun Baru "+lm.getYear()+". Semoga tahun ini lebih bermakna kepada kita semua. Berikut adalah jadual cuti anda pada tahun sebelum dan tahun ini. Cuti ini adalah dijana sepenuhnya dari sistem eCuti dan jika ada kesilapan kiraan sila maklumkan kepada kami.\n                                        </p>\n                                       \n                                        \n                                        \n                                    </td>\n                                </tr>\n                                <tr>\n                                    <td align=\"center\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                        <!-- section text ======-->\n\n                                      <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\">\n" +
                "                                            <tr>\n" +
                "                                                <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\" width=\"40%\">\n" +
                "                                                    Tarikh Lantikan\n" +
                "                                                </td>\n" +
                "                                                <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n" +
                "                                                    : &nbsp;&nbsp;<strong>"+lm.getDatejoined()+"</strong>\n" +
                "                                                </td>\n" +
                "                                            </tr>\n" +
                "                                            <tr>\n" +
                "                                                <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\" width=\"30%\">\n" +
                "                                                    Tahun Bekerja\n" +
                "                                                </td>\n" +
                "                                                <td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n" +
                "                                                    : &nbsp;&nbsp;<strong>"+lm.getYearsOfService()+" Tahun</strong>\n" +
                "                                                </td>\n" +
                "                                            </tr>\n" +
                "                                        </table>\n" +
                "<br>\n" +
                "                                       <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\" style=\"border: 1px solid #ccc;\">\n" +
                "                                        <tr>\n" +
                "                                                <td align=\"left\" colspan=\"4\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px; border-bottom: 1px solid #ccc;\" ><p>&nbsp;&nbsp;&nbsp;&nbsp;<strong>Ringkasan Tahun Lepas (2020)</strong></p><p></p><p></p></td>\n" +
                "                                            </tr>\n" +
                "                                \t\t\t<tr>\n" +
                "                                \t\t\t\t<td width=\"33%\" align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;border-bottom: 1px solid #ccc;\"><p>&nbsp;&nbsp;&nbsp;&nbsp;C. Tahunan</p></td>\n" +
                "                                \t\t\t\t<td width=\"33%\" align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;border-bottom: 1px solid #ccc;\"><p>&nbsp;&nbsp;&nbsp;&nbsp;C. Sakit</p></td>\n" +
                "                                \t\t\t\t<td width=\"33%\" align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;border-bottom: 1px solid #ccc;\"><p> &nbsp;&nbsp;&nbsp;&nbsp;C. Hospitalisasi</p></td>\n" +
                "                                \t\t\t\t\n" +
                "                                                \n" +
                "\n" +
                "                                \t\t\t</tr>\n" +
                "                                            <tr>\n" +
                "                                                <td align=\"center\" style=\"color: #888888; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n" +
                "\n" +
                "                                                    <table border=\"0\" width=\"90%\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\" >\n" +
                "                                                        <tr>\n" +
                "                                                            <td width=\"40%\">Layak</td>\n" +
                "                                                            <td>: <strong>"+li.getEligibleleave()+"</strong> </td>\n" +
                "                                                        </tr>\n" +
                "                                                        <tr>\n" +
                "                                                            <td>Dibawa</td>\n" +
                "                                                            <td>: <strong>"+li.getBf()+"</strong> </td>\n" +
                "                                                        </tr>\n" +
                "                                                        <tr>\n" +
                "                                                            <td>Jumlah</td>\n" +
                "                                                            <td>: <strong>"+totalPrevEligibleLeave+"</strong> </td>\n" +
                "                                                        </tr>\n" +
                "                                                        <tr>\n" +
                "                                                            <td>Guna</td>\n" +
                "                                                            <td>: <strong>"+lm.getTotalLeaveUseOfTheYear()+"</strong> </td>\n" +
                "                                                        </tr>\n" +
                "                                                        <tr>\n" +
                "                                                            <td>Baki</td>\n" +
                "                                                            <td>: <strong>"+lm.getBalanceOfTheYearEligibleLeave()+"</strong> </td>\n" +
                "                                                        </tr>\n" +
                "\n" +
                "                                                    </table>\n" +
                "\n" +
                "                                                    \n" +
                "                                                </td>\n" +
                "                                                <td align=\"left\" valign=\"top\" style=\"color: #888888; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n" +
                "                                                   \n" +
                "                                                    <table border=\"0\" width=\"90%\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\" >\n" +
                "                                                        <tr>\n" +
                "                                                            <td width=\"40%\">Jumlah</td>\n" +
                "                                                            <td>: <strong>"+li.getMc()+"</strong> </td>\n" +
                "                                                        </tr>\n" +
                "                                                        <tr>\n" +
                "                                                            <td>Guna</td>\n" +
                "                                                            <td>: <strong>"+lm.getTotalLeaveUseMCOfTheYear()+"</strong> </td>\n" +
                "                                                        </tr>\n" +
                "                                                        <tr>\n" +
                "                                                            <td>Baki</td>\n" +
                "                                                            <td>: <strong>"+balMCLeavePrevYear+"</strong> </td>\n" +
                "                                                        </tr>\n" +
                "                                                       \n" +
                "\n" +
                "                                                    </table>\n" +
                "                                                </td>\n" +
                "                                                <td align=\"left\" valign=\"top\" style=\"color: #888888; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n" +
                "                                                   \n" +
                "                                                    <table border=\"0\" width=\"90%\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\" >\n" +
                "                                                        <tr>\n" +
                "                                                            <td width=\"40%\">Jumlah</td>\n" +
                "                                                            <td>: <strong>"+li.getHosp()+"</strong> </td>\n" +
                "                                                        </tr>\n" +
                "                                                        <tr>\n" +
                "                                                            <td>Guna</td>\n" +
                "                                                            <td>: <strong>"+lm.getTotalLeaveUseHospTheYear()+"</strong> </td>\n" +
                "                                                        </tr>\n" +
                "                                                        <tr>\n" +
                "                                                            <td>Baki</td>\n" +
                "                                                            <td>: <strong>"+balHospLeavePrevYear+"</strong> </td>\n" +
                "                                                        </tr>\n" +
                "                                                       \n" +
                "\n" +
                "                                                    </table>\n" +
                "                                                </td>\n" +
                "                                                \n" +
                "                                                \n" +
                "\n" +
                "                                            </tr>\n" +
                "                                            <tr>\n" +
                "                                                <td colspan=\"3\" align=\"left\" valign=\"top\" style=\"color: red; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">&nbsp;* Baki maksimum dibawa kehadapan adalah separuh dari Cuti Layak Tahunan.</td>\n" +
                "                                            </tr>\n" +
                "                                \t\t</table>\n" +
                "                                        <br>\n" +
                "                                        <table border=\"0\" width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\" style=\"border: 1px solid #ccc;\">\n" +
                "                                        <tr>\n" +
                "                                                <td align=\"left\" colspan=\"4\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px; border-bottom: 1px solid #ccc;\" ><p>&nbsp;&nbsp;&nbsp;&nbsp;<strong>Tahun Baru (2021)</strong></p><p></p><p></p></td>\n" +
                "                                            </tr>\n" +
                "                                            <tr>\n" +
                "                                                <td width=\"33%\" align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;border-bottom: 1px solid #ccc;\"><p>&nbsp;&nbsp;&nbsp;&nbsp;C. Tahunan</p></td>\n" +
                "                                                <td width=\"33%\" align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;border-bottom: 1px solid #ccc;\"><p>&nbsp;&nbsp;&nbsp;&nbsp;C. Sakit</p></td>\n" +
                "                                                <td width=\"33%\" align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;border-bottom: 1px solid #ccc;\"><p> &nbsp;&nbsp;&nbsp;&nbsp;C. Hospitalisasi</p></td>\n" +
                "                                                \n" +
                "                                                \n" +
                "\n" +
                "                                            </tr>\n" +
                "                                            <tr>\n" +
                "                                                <td align=\"center\" style=\"color: #888888; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n" +
                "\n" +
                "                                                    <table border=\"0\" width=\"90%\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\" >\n" +
                "                                                        <tr>\n" +
                "                                                            <td width=\"40%\">Layak</td>\n" +
                "                                                            <td>: <strong>"+lm.getNewEligibleLeave()+"</strong> </td>\n" +
                "                                                        </tr>\n" +
                "                                                        <tr>\n" +
                "                                                            <td>Dibawa</td>\n" +
                "                                                            <td>: <strong>"+lm.getBf()+"</strong> </td>\n" +
                "                                                        </tr>\n" +
                "                                                        <tr>\n" +
                "                                                            <td>Jumlah</td>\n" +
                "                                                            <td>: <strong>"+totalNewEligibleLeave+"</strong> </td>\n" +
                "                                                        </tr>\n" +
                "\n" +
                "                                                    </table>\n" +
                "\n" +
                "                                                    \n" +
                "                                                </td>\n" +
                "                                                <td align=\"left\" valign=\"top\" style=\"color: #888888; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n" +
                "                                                   \n" +
                "                                                    <table border=\"0\" width=\"90%\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\" >\n" +
                "                                                        <tr>\n" +
                "                                                            <td width=\"40%\">Jumlah</td>\n" +
                "                                                            <td>: <strong>"+lm.getEligibleMC()+"</strong> </td>\n" +
                "                                                        </tr>\n" +
                "                                                       \n" +
                "\n" +
                "                                                    </table>\n" +
                "                                                </td>\n" +
                "                                                <td align=\"left\" valign=\"top\" style=\"color: #888888; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n" +
                "                                                   \n" +
                "                                                    <table border=\"0\" width=\"90%\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"container590\" >\n" +
                "                                                        <tr>\n" +
                "                                                            <td width=\"40%\">Jumlah</td>\n" +
                "                                                            <td>: <strong>"+li.getHosp()+"</strong> </td>\n" +
                "                                                        </tr>\n" +
                "                                                       \n" +
                "\n" +
                "                                                    </table>\n" +
                "                                                </td>\n" +
                "                                                \n" +
                "                                                \n" +
                "\n" +
                "                                            </tr>\n" +
                "                                        </table>\n                                \t\t<p>&nbsp;</p>\n                                \t\t <p style=\"line-height: 24px; margin-bottom:20px;\">\n                                            Anda boleh mengakses cuti dan rekod dengan menekan butang seperti di bawah.\n                                        </p>\n                                        \n                                        \n                                    </td>\n                                </tr>\n                                <tr>\n                                \t<td align=\"left\" style=\"color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;\">\n                                \t\t<br>\n                                \t\t<table border=\"0\" align=\"center\" width=\"180\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#008000\" style=\"margin-bottom:20px;\">\n\n                                            <tr>\n                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n                                            </tr>\n\n                                            <tr>\n                                                <td align=\"center\" style=\"color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 22px; letter-spacing: 2px;\">\n                                                    <!-- main section button -->\n\n                                                    <div style=\"line-height: 22px;\">\n                                                        <a href=\"https://ecuti.lcsb.com.my/\" style=\"color: #ffffff; text-decoration: none;\">Pergi ke eCuti</a>\n                                                    </div>\n                                                </td>\n                                            </tr>\n\n                                            <tr>\n                                                <td height=\"10\" style=\"font-size: 10px; line-height: 10px;\">&nbsp;</td>\n                                            </tr>\n\n                                        </table>\n                                \t\t<p>&nbsp;</p>\n                                \t\t<p style=\"line-height: 24px\">\n                                            Yang Benar,\n                                        </p>\n\t\t\t\t\t\t\t\t\t\t<p style=\"line-height: 24px\">\n                                            <strong>LCSB Bot</strong>\n                                        </p>\n                                        <p style=\"line-height: 24px\">\n                                            Pentadbir Maya Sistem eCuti\n                                        </p>\n\n                                \t</td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n\n\n\n\n\n                </table>\n\n            </td>\n        </tr>\n\n        <tr>\n            <td height=\"40\" style=\"font-size: 40px; line-height: 40px;\">&nbsp;</td>\n        </tr>\n\n    </table>\n\n    <!-- end section -->\n\n\n    <!-- main section -->\n    \n\n    <!-- end section -->\n\n    \n    <!-- end section -->\n\n    <!-- footer ====== -->\n    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;\"> <tr> <td align=\"center\" valign=\"top\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"88%\" style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"> <tr> <td align=\"center\" valign=\"top\"> <div style=\"height: 34px; line-height: 34px; font-size: 32px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#868686\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #868686; font-size: 17px; line-height: 20px;\">Copyright &copy; 2019 LCSB. All&nbsp;Rights&nbsp;Reserved.</span> </font> <div style=\"height: 3px; line-height: 3px; font-size: 1px;\">&nbsp;</div> <font face=\"'Source Sans Pro', sans-serif\" color=\"#1a1a1a\" style=\"font-size: 17px; line-height: 20px;\"> <span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px;\"><a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">sysadmin@lcsb.com.my</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"#\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">09-5165180</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href=\"http://www.lcsb.com.my\" target=\"_blank\" style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;\">www.lcsb.com.my</a></span> </font> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"> <tr> <td align=\"center\" valign=\"top\"> <a href=\"#\" target=\"_blank\" style=\"display: block; max-width: 19px;\"> <img src=\"https://docs.google.com/uc?id=1NomvumBJQqjFi0WYxP45K_RLRSXfZXLP\" alt=\"img\" width=\"20\" border=\"0\" style=\"display: block; width: 20;\" /> </a> </td> <td><span style=\"font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif;font-size: 12px\"> &nbsp; LKPP CORPORATION SDN BHD</span></td> </tr> </table> <div style=\"height: 35px; line-height: 35px; font-size: 33px;\">&nbsp;</div> </td> </tr> </table>\n    <!-- end footer ====== -->\n\n</body>\n\n</html>";
        return template;
    }
}
