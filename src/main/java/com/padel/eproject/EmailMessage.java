package com.padel.eproject;

import com.padel.eproject.EmailMessage;

public class EmailMessage {
	private String to_address;
	
	private String subject;
	
	private String body;
	
	private String senderEmail;
	
	private String senderPassword;
	
	public String getTo_address() { return this.to_address; }
	
	public void setTo_address(String to_address) { this.to_address = to_address; }
	
	public String getSubject() { return this.subject; }
	
	public void setSubject(String subject) { this.subject = subject; }
	
	public String getBody() { return this.body; }
	
	public void setBody(String body) { this.body = body; }
	
	public String getSenderEmail() { return this.senderEmail; }
	
	public void setSenderEmail(String senderEmail) { this.senderEmail = senderEmail; }
	
	public String getSenderPassword() { return this.senderPassword; }
	
	public void setSenderPassword(String senderPassword) { this.senderPassword = senderPassword; }
}

