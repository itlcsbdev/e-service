package com.padel.eproject.repository;

import com.padel.eproject.model.CoStaffEntity;
import java.util.List;
import org.springframework.data.repository.Repository;
import org.springframework.lang.Nullable;

public interface StaffRepository extends Repository<CoStaffEntity, Integer> {
	List<CoStaffEntity> findAll();

	@Nullable
	CoStaffEntity findByStaffid(String paramString);
	
	CoStaffEntity findByEmail(String paramString);
}
