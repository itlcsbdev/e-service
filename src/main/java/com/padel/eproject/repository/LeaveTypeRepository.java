package com.padel.eproject.repository;

import com.padel.eproject.model.LeaveTypeEntity;
import org.springframework.data.repository.Repository;

public interface LeaveTypeRepository  extends Repository<LeaveTypeEntity, Integer> {
    LeaveTypeEntity findByCode(String code);
}
