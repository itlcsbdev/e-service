package com.padel.eproject.repository;

import com.padel.eproject.model.CheckmasterEntity;
import com.padel.eproject.model.CoStaffEntity;
import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

public interface CheckRepository extends Repository<CheckmasterEntity, Integer> {

	@Query("SELECT p FROM  CoStaffEntity p WHERE p.email <> '' AND p.status = 'Active'")
	List<CoStaffEntity> getAllStaff();

	@Query("SELECT p FROM  CoStaffEntity p WHERE p.email <> '' AND p.status = 'Active' AND (p.location = 'IBU PEJABAT' OR p.location = 'CAWANGAN INDERA MAHKOTA') AND p.position <> 'PENYELIA'")
	List<CoStaffEntity> getAllHQStaff();
	
	@Query("SELECT p FROM  CoStaffEntity p WHERE p.email <> '' and p.status = 'Active' AND p.locId <> '9911'")
	List<CoStaffEntity> getAllStaffNonHQ();
	
	@Query("SELECT p FROM  CheckmasterEntity p WHERE p.staffId = ?1 AND p.date = ?2")
	CheckmasterEntity getCheckInfo(String paramString1, String paramString2);
	
	@Query("SELECT r.personCode FROM PersonEntity r WHERE r.id = ?1")
	String getStaffID(int paramInt);
	
	@Transactional
	@Modifying
	@Query("update CheckmasterEntity u set u.temperature = ?1, u.status = ?2, u.timevalid = ?3 where u.date = ?4 and u.staffId = ?5")
	void update(double paramDouble, String paramString1, String paramString2, String paramString3, String paramString4);
	
	CheckmasterEntity save(CheckmasterEntity paramCheckmasterEntity);
	
	@Query(value = "SELECT ifnull(concat(lpad(max(id)+1,10,'0')), '0000000001') as new from checkmaster ", nativeQuery = true)
	String getMaxID();
}
