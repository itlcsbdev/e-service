package com.padel.eproject.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.padel.eproject.model.LeaveInfoUpdateEntity;

public interface LeaveInfoUpdateRepository extends Repository<LeaveInfoUpdateEntity, Integer> {
	
//	@Transactional
//	@Modifying
//	@Query("INSERT LeaveInfoUpdateEntity u set u.eligibleleave = ?1 where u.staffId = ?2 and u.year = ?3")
//	void updateNewBalance(int paramInt, String paramString1, String paramString2);
	

//    @Transactional
//	@Modifying
//    @Query("insert into LeaveInfoUpdateEntity  (staffId,year) VALUES (?1,?2)")
//    void insertTransactionBalance(String paramString1, String paramString2);
	@Transactional
    @Modifying
    @Query(value = "insert into leave_info_update (staffId, year, datechanged, oldbal, newbal, yearof) VALUES (?1, ?2, ?3, ?4, ?5, ?6)", nativeQuery = true)
    void insertTransactionBalance(String staffId, String year, String datechanged, int oldbal, int newbal, int yearof);

    @Transactional
    @Modifying
    @Query(value = "insert into leave_info_history (staffId,name, year, eligibleleavenew, eligibleleaveold, bf, halfofeligible,datejoined,yearservice,balprev, mc) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11)", nativeQuery = true)
    void insertNewLeaveOfNewYear(String staffId,String name, String year, int eligibleleavenew, int eligibleleaveold, int bf, int halfofeligible, String datejoined, int yearservice, int balprev, int mc);

    @Transactional
    @Modifying
    @Query(value = "insert into leave_info (staffId,eligibleleave, year, bf, mc, hosp, maternity) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)", nativeQuery = true)
    void insertNewLeaveOfNewYearLeaveInfo(String staffId,int eligibleleave, String year, int bf, int mc, int hosp, int maternity);
    
    

}
