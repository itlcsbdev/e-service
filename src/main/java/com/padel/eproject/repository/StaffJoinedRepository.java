package com.padel.eproject.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.padel.eproject.model.StaffJoinedEntity;

public interface StaffJoinedRepository extends Repository<StaffJoinedEntity, Integer> {
	
	List<StaffJoinedEntity> findAll();

}
