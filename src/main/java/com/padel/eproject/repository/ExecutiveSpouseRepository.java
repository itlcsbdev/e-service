package com.padel.eproject.repository;

import com.padel.eproject.model.ExecutiveSpouseEntity;
import com.padel.eproject.model.LeaveMasterEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface ExecutiveSpouseRepository extends Repository<ExecutiveSpouseEntity, Integer> {

    @Query(nativeQuery = true, value = "SELECT * FROM executive_spouse p WHERE p.staffid = ?1 limit 1")
    ExecutiveSpouseEntity findByStaffid(String paramString);

    List<ExecutiveSpouseEntity> findAllByStaffid(String paramString);
}
