package com.padel.eproject.repository;
import com.padel.eproject.model.UserAccessEntity;
import org.springframework.data.repository.Repository;

public interface UserAccessRepository extends Repository<UserAccessEntity, Integer> {
	UserAccessEntity findByStaffId(String paramString);
}
