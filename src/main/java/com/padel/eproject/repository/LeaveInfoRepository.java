package com.padel.eproject.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import com.padel.eproject.model.LeaveInfoEntity;
import org.springframework.data.repository.Repository;
import com.padel.eproject.model.LeaveInfoEntity;

public interface LeaveInfoRepository extends Repository<LeaveInfoEntity, Integer> {
	LeaveInfoEntity findByStaffId(String paramString);
	
	LeaveInfoEntity findByStaffIdAndYear(String paramString1, String paramString2);
	
	@Transactional
	@Modifying
	@Query("update LeaveInfoEntity u set u.eligibleleave = ?1 where u.staffId = ?2 and u.year = ?3")
	void updateNewBalance(int paramInt, String paramString1, String paramString2);
	
//	@Transactional
//	@Modifying
//	@Query("update CheckmasterEntity u set  u.timeout = ?1 where u.date = ?2 and u.staffId = ?3")
//	void checkOut(String paramString1, String paramString2, String paramString3);
}
