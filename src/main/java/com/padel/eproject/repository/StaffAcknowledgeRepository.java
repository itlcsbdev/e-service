package com.padel.eproject.repository;

import com.padel.eproject.model.StaffAcknowledgementEntity;
import org.springframework.data.repository.Repository;

public interface StaffAcknowledgeRepository  extends Repository<StaffAcknowledgementEntity, Integer> {
    StaffAcknowledgementEntity findByStaffIdAndLatestIsTrue(String id);
}
