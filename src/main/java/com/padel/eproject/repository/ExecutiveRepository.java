package com.padel.eproject.repository;

import com.padel.eproject.model.ExecutiveEntity;
import org.springframework.data.repository.Repository;

public interface ExecutiveRepository extends Repository<ExecutiveEntity, Integer> {
    ExecutiveEntity findByStaffid(String paramString);
}
