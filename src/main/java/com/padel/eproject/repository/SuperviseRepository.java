package com.padel.eproject.repository;

import com.padel.eproject.model.SuperviseInfoEntity;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

public interface SuperviseRepository extends Repository<SuperviseInfoEntity, Integer> {
	@Query("SELECT p.staffId FROM SuperviseInfoEntity p WHERE p.supervisorId = ?1")
	List<String> getSuperviseeIDforSupervisor(String paramString);
	
	@Query("SELECT p.staffId FROM SuperviseInfoEntity p WHERE p.headId = ?1")
	List<String> getSuperviseeIDforHead(String paramString);
	
	@Query("SELECT p FROM SuperviseInfoEntity p WHERE p.staffId = ?1")
	SuperviseInfoEntity getSuperViseInfo(String paramString);
}
