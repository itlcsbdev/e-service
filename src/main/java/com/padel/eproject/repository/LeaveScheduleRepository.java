package com.padel.eproject.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.padel.eproject.model.LeaveScheduleEntity;

public interface LeaveScheduleRepository extends Repository<LeaveScheduleEntity, Integer> {

	List<LeaveScheduleEntity> findAll();
	
	LeaveScheduleEntity findByPostId(String paramString);
	
	

}
