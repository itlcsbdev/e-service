package com.padel.eproject.repository;

import com.padel.eproject.model.CheckformEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

public interface CheckFormRepository extends Repository<CheckformEntity, Integer> {
	@Query("SELECT p FROM  CheckformEntity p WHERE p.checkId = ?1")
	CheckformEntity getCheckFormInfo(String paramString);
	
	CheckformEntity save(CheckformEntity paramCheckformEntity);
}

