package com.padel.eproject.repository;

import com.padel.eproject.model.LeaveMasterEntity;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

public interface LeaveRepository extends Repository<LeaveMasterEntity, Integer> {
	List<LeaveMasterEntity> findAll();
	
	LeaveMasterEntity findByLeaveId(String paramString);
	
	List<LeaveMasterEntity> findByStaffId(String paramString);
	
	@Query("SELECT p FROM LeaveMasterEntity p")
	List<LeaveMasterEntity> findByYearandPeriod();
	
	@Query("SELECT p FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.status <> 'Approved'")
	List<LeaveMasterEntity> findLeaveNotApproved(String paramString);
	
	@Query("SELECT p FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.datestart >= CURDATE()")
	List<LeaveMasterEntity> getNewLeave(String paramString);
	
	@Query("SELECT count(p) FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.datestart >= CURDATE()")
	int getCountLeave(String paramString);

	@Query("SELECT COALESCE(SUM(p.days),0) FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.year =  ?2 AND p.status = 'Approved' AND p.type = 'C01'")
	double getTotalLeaveOfTheYear(String paramString1, String paramString2);

	@Query("SELECT COALESCE(SUM(p.days),0) FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.year =  ?2 AND p.status = 'Approved' AND p.type = 'C02'")
	double getTotalSickLeaveUseOfTheYear(String paramString1, String paramString2);

	@Query("SELECT COALESCE(SUM(p.days),0) FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.year =  ?2 AND p.status = 'Approved' AND p.type = 'C03'")
	double getTotalHospLeaveUseOfTheYear(String paramString1, String paramString2);
	
	@Query("SELECT p, r FROM LeaveMasterEntity p, CoStaffEntity r WHERE p.staffId = r.staffid AND p.staffId <> ?1 AND  p.staffId = ?2")
	List<LeaveMasterEntity> getAllLeaveExcludeYou(String paramString1, String paramString2);
	
	@Query("SELECT p FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.status = ?2")
	List<LeaveMasterEntity> getLeaveWithStatus(String paramString1, String paramString2);
	
	@Query("SELECT p FROM LeaveMasterEntity p WHERE p.status NOT IN ('Approved','Rejected')")
	List<LeaveMasterEntity> getUnapprovedLeave();
	
	@Query("SELECT p, r FROM LeaveMasterEntity p, CoStaffEntity r WHERE p.staffId = r.staffid AND (?1 BETWEEN p.datestart AND p.dateend) and p.status = 'Approved' AND r.location = 'IBU PEJABAT'")
	List<LeaveMasterEntity> getTodaysLeave(String paramString);

	@Query("SELECT COALESCE(SUM(p.days),0) FROM LeaveMasterEntity p WHERE p.staffId = ?1 AND p.year =  ?2 AND p.status = 'Approved' AND p.type = 'C01'")
	double getTotalLeaveUseOfTheYear(String paramString1, String paramString2);
}

