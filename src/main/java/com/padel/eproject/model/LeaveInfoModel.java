package com.padel.eproject.model;

import lombok.Data;

@Data
public class LeaveInfoModel {

    private CoStaffEntity staffInfo;
    private LeaveInfoEntity leaveInfo;
    private String year;
    private int newEligibleLeave;
    private int bf;
    private int halfOfPrevEligibleLeave;
    private String datejoined;
    private int yearsOfService;
    private int balPrevYearEligibleLeavel;
    private int eligibleMC;
    private int totalLeaveUseOfTheYear;
    private int balanceOfTheYearEligibleLeave;
    private int totalLeaveUseMCOfTheYear;
    private int totalLeaveUseHospTheYear;

    public int getTotalLeaveUseMCOfTheYear() {
        return totalLeaveUseMCOfTheYear;
    }

    public void setTotalLeaveUseMCOfTheYear(int totalLeaveUseMCOfTheYear) {
        this.totalLeaveUseMCOfTheYear = totalLeaveUseMCOfTheYear;
    }

    public int getTotalLeaveUseHospTheYear() {
        return totalLeaveUseHospTheYear;
    }

    public void setTotalLeaveUseHospTheYear(int totalLeaveUseHospTheYear) {
        this.totalLeaveUseHospTheYear = totalLeaveUseHospTheYear;
    }

    public int getBalanceOfTheYearEligibleLeave() {
        return balanceOfTheYearEligibleLeave;
    }

    public void setBalanceOfTheYearEligibleLeave(int balanceOfTheYearEligibleLeave) {
        this.balanceOfTheYearEligibleLeave = balanceOfTheYearEligibleLeave;
    }

    public int getTotalLeaveUseOfTheYear() {
        return totalLeaveUseOfTheYear;
    }

    public void setTotalLeaveUseOfTheYear(int totalLeaveUseOfTheYear) {
        this.totalLeaveUseOfTheYear = totalLeaveUseOfTheYear;
    }

    public LeaveInfoEntity getLeaveInfo() {
        return leaveInfo;
    }

    public void setLeaveInfo(LeaveInfoEntity leaveInfo) {
        this.leaveInfo = leaveInfo;
    }

    public CoStaffEntity getStaffInfo() {
        return staffInfo;
    }

    public void setStaffInfo(CoStaffEntity staffInfo) {
        this.staffInfo = staffInfo;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getNewEligibleLeave() {
        return newEligibleLeave;
    }

    public void setNewEligibleLeave(int newEligibleLeave) {
        this.newEligibleLeave = newEligibleLeave;
    }

    public int getBf() {
        return bf;
    }

    public void setBf(int bf) {
        this.bf = bf;
    }

    public int getHalfOfPrevEligibleLeave() {
        return halfOfPrevEligibleLeave;
    }

    public void setHalfOfPrevEligibleLeave(int halfOfPrevEligibleLeave) {
        this.halfOfPrevEligibleLeave = halfOfPrevEligibleLeave;
    }

    public String getDatejoined() {
        return datejoined;
    }

    public void setDatejoined(String datejoined) {
        this.datejoined = datejoined;
    }

    public int getYearsOfService() {
        return yearsOfService;
    }

    public void setYearsOfService(int yearsOfService) {
        this.yearsOfService = yearsOfService;
    }

    public int getBalPrevYearEligibleLeavel() {
        return balPrevYearEligibleLeavel;
    }

    public void setBalPrevYearEligibleLeavel(int balPrevYearEligibleLeavel) {
        this.balPrevYearEligibleLeavel = balPrevYearEligibleLeavel;
    }

    public int getEligibleMC() {
        return eligibleMC;
    }

    public void setEligibleMC(int eligibleMC) {
        this.eligibleMC = eligibleMC;
    }
}
