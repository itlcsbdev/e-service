package com.padel.eproject.model;

import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "checkform", schema = "eleave", catalog = "")
public class CheckformEntity {
	private int id;
	
	private String checkId;
	
	private String question1;
	
	private String question2;
	
	private String question3;
	
	@Id
	@Column(name = "id")
	public int getId() { return this.id; }
	
	public void setId(int id) { this.id = id; }
	
	@Basic
	@Column(name = "checkID")
	public String getCheckId() { return this.checkId; }
	
	public void setCheckId(String checkId) { this.checkId = checkId; }
	
	@Basic
	@Column(name = "question1")
	public String getQuestion1() { return this.question1; }
	
	public void setQuestion1(String question1) { this.question1 = question1; }
	
	@Basic
	@Column(name = "question2")
	public String getQuestion2() { return this.question2; }
	
	public void setQuestion2(String question2) { this.question2 = question2; }
	
	@Basic
	@Column(name = "question3")
	public String getQuestion3() { return this.question3; }
	
	public void setQuestion3(String question3) { this.question3 = question3; }
	
	public boolean equals(Object o) {
		if (this == o)
			return true; 
		if (o == null || getClass() != o.getClass())
			return false; 
		CheckformEntity that = (CheckformEntity)o;
		return (this.id == that.id && 
			Objects.equals(this.checkId, that.checkId) && 
			Objects.equals(this.question1, that.question1) && 
			Objects.equals(this.question2, that.question2) && 
			Objects.equals(this.question3, that.question3));
	}
	
	public int hashCode() { return Objects.hash(new Object[] { Integer.valueOf(this.id), this.checkId, this.question1, this.question2, this.question3 }); }
}