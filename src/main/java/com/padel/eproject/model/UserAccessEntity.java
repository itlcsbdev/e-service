package com.padel.eproject.model;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_access", schema = "eleave", catalog = "")
public class UserAccessEntity {
	private int id;
	
	private String staffId;
	
	private int level;
	
	@Id
	@Column(name = "id")
	public int getId() { return this.id; }
	
	public void setId(int id) { this.id = id; }
	
	@Basic
	@Column(name = "staffID")
	public String getStaffId() { return this.staffId; }
	
	public void setStaffId(String staffId) { this.staffId = staffId; }
	
	@Basic
	@Column(name = "level")
	public int getLevel() { return this.level; }
	
	public void setLevel(int level) { this.level = level; }
	
	public boolean equals(Object o) {
		if (this == o)
			return true; 
		if (o == null || getClass() != o.getClass())
			return false; 
		UserAccessEntity that = (UserAccessEntity)o;
		if (this.id != that.id)
			return false; 
		if (this.level != that.level)
			return false; 
		if ((this.staffId != null) ? !this.staffId.equals(that.staffId) : (that.staffId != null))
			return false; 
		return true;
	}
	
	public int hashCode() {
		int result = this.id;
		result = 31 * result + ((this.staffId != null) ? this.staffId.hashCode() : 0);
		result = 31 * result + this.level;
		return result;
	}
}

