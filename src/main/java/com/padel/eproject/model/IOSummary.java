package com.padel.eproject.model;

import com.padel.eproject.model.IOData;
import java.util.List;

public class IOSummary {
	private List<IOData> io;
	
	public String toString() { return "IOSummary(io=" + getIo() + ")"; }
	
//	public int hashCode() {
//		int PRIME = 59;
//		result = 1;
//		Object<IOData> $io = (Object<IOData>)getIo();
//		return result * 59 + (($io == null) ? 43 : $io.hashCode());
//	}
	
	protected boolean canEqual(Object other) { return other instanceof IOSummary; }
	
//	public boolean equals(Object o) { if (o == this)
//			return true; 
//		if (!(o instanceof IOSummary))
//			return false; 
//		IOSummary other = (IOSummary)o;
//		if (!other.canEqual(this))
//			return false; 
//		Object<IOData> this$io = (Object<IOData>)getIo(), other$io = (Object<IOData>)other.getIo();
//		return !((this$io == null) ? (other$io != null) : !this$io.equals(other$io)); }
	
	public void setIo(List<IOData> io) { this.io = io; }
	
	public List<IOData> getIo() { return this.io; }
}

