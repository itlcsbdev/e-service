package com.padel.eproject.model;

import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "checkmaster", schema = "eleave", catalog = "")
public class CheckmasterEntity {
	private String id;
	
	private String staffId;
	
	private String date;
	
	private String timescan;
	
	private String timevalid;
	
	private Double temperature;
	
	private String status;
	
	private String timeout;
	
	@Id
	@Column(name = "id")
	public String getId() { return this.id; }
	
	public void setId(String id) { this.id = id; }
	
	@Basic
	@Column(name = "staffID")
	public String getStaffId() { return this.staffId; }
	
	public void setStaffId(String staffId) { this.staffId = staffId; }
	
	@Basic
	@Column(name = "date")
	public String getDate() { return this.date; }
	
	public void setDate(String date) { this.date = date; }
	
	@Basic
	@Column(name = "timescan")
	public String getTimescan() { return this.timescan; }
	
	public void setTimescan(String timescan) { this.timescan = timescan; }
	
	@Basic
	@Column(name = "timevalid")
	public String getTimevalid() { return this.timevalid; }
	
	public void setTimevalid(String timevalid) { this.timevalid = timevalid; }
	
	@Basic
	@Column(name = "temperature")
	public Double getTemperature() { return this.temperature; }
	
	public void setTemperature(Double temperature) { this.temperature = temperature; }
	
	@Basic
	@Column(name = "status")
	public String getStatus() { return this.status; }
	
	public void setStatus(String status) { this.status = status; }
	
	@Basic
	@Column(name = "timeout")
	public String getTimeout() { return this.timeout; }
	
	public void setTimeout(String timeout) { this.timeout = timeout; }
	
	public boolean equals(Object o) {
		if (this == o)
			return true; 
		if (o == null || getClass() != o.getClass())
			return false; 
		CheckmasterEntity that = (CheckmasterEntity)o;
		return (Objects.equals(this.id, that.id) && 
			Objects.equals(this.staffId, that.staffId) && 
			Objects.equals(this.date, that.date) && 
			Objects.equals(this.timescan, that.timescan) && 
			Objects.equals(this.timevalid, that.timevalid) && 
			Objects.equals(this.temperature, that.temperature) && 
			Objects.equals(this.status, that.status) && 
			Objects.equals(this.timeout, that.timeout));
	}
	
	public int hashCode() { return Objects.hash(new Object[] { this.id, this.staffId, this.date, this.timescan, this.timevalid, this.temperature, this.status, this.timeout }); }
}
