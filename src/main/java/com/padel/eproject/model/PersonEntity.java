package com.padel.eproject.model;

import java.sql.Timestamp;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "person", schema = "eleave", catalog = "")
public class PersonEntity {
	private int id;
	
	private int personGroupId;
	
	private int personType;
	
	private String personCode;
	
	private String givenName;
	
	private String familyName;
	
	private String fullName;
	
	private int gender;
	
	private String password;
	
	private String email;
	
	private String phoneNumber;
	
	private String photoUrl;
	
	private int photoIndex;
	
	private String smallPhotoUrl;
	
	private Timestamp startValidDate;
	
	private Timestamp endValidDate;
	
	private int type;
	
	private String remark;
	
	private Timestamp updateTime;
	
	private Timestamp createTime;
	
	private int isDeleted;
	
	private String objectGuid;
	
	private String temper;
	
	private int temperStatus;
	
	private int personFrom;
	
	private String rdn;
	
	private String dn;
	
	private long usnChanged;
	
	private int startTimeDiffer;
	
	private int endTimeDiffer;
	
	@Id
	@Column(name = "id")
	public int getId() { return this.id; }
	
	public void setId(int id) { this.id = id; }
	
	@Basic
	@Column(name = "person_group_id")
	public int getPersonGroupId() { return this.personGroupId; }
	
	public void setPersonGroupId(int personGroupId) { this.personGroupId = personGroupId; }
	
	@Basic
	@Column(name = "person_type")
	public int getPersonType() { return this.personType; }
	
	public void setPersonType(int personType) { this.personType = personType; }
	
	@Basic
	@Column(name = "person_code")
	public String getPersonCode() { return this.personCode; }
	
	public void setPersonCode(String personCode) { this.personCode = personCode; }
	
	@Basic
	@Column(name = "given_name")
	public String getGivenName() { return this.givenName; }
	
	public void setGivenName(String givenName) { this.givenName = givenName; }
	
	@Basic
	@Column(name = "family_name")
	public String getFamilyName() { return this.familyName; }
	
	public void setFamilyName(String familyName) { this.familyName = familyName; }
	
	@Basic
	@Column(name = "full_name")
	public String getFullName() { return this.fullName; }
	
	public void setFullName(String fullName) { this.fullName = fullName; }
	
	@Basic
	@Column(name = "gender")
	public int getGender() { return this.gender; }
	
	public void setGender(int gender) { this.gender = gender; }
	
	@Basic
	@Column(name = "password")
	public String getPassword() { return this.password; }
	
	public void setPassword(String password) { this.password = password; }
	
	@Basic
	@Column(name = "email")
	public String getEmail() { return this.email; }
	
	public void setEmail(String email) { this.email = email; }
	
	@Basic
	@Column(name = "phone_number")
	public String getPhoneNumber() { return this.phoneNumber; }
	
	public void setPhoneNumber(String phoneNumber) { this.phoneNumber = phoneNumber; }
	
	@Basic
	@Column(name = "photo_url")
	public String getPhotoUrl() { return this.photoUrl; }
	
	public void setPhotoUrl(String photoUrl) { this.photoUrl = photoUrl; }
	
	@Basic
	@Column(name = "photo_index")
	public int getPhotoIndex() { return this.photoIndex; }
	
	public void setPhotoIndex(int photoIndex) { this.photoIndex = photoIndex; }
	
	@Basic
	@Column(name = "small_photo_url")
	public String getSmallPhotoUrl() { return this.smallPhotoUrl; }
	
	public void setSmallPhotoUrl(String smallPhotoUrl) { this.smallPhotoUrl = smallPhotoUrl; }
	
	@Basic
	@Column(name = "start_valid_date")
	public Timestamp getStartValidDate() { return this.startValidDate; }
	
	public void setStartValidDate(Timestamp startValidDate) { this.startValidDate = startValidDate; }
	
	@Basic
	@Column(name = "end_valid_date")
	public Timestamp getEndValidDate() { return this.endValidDate; }
	
	public void setEndValidDate(Timestamp endValidDate) { this.endValidDate = endValidDate; }
	
	@Basic
	@Column(name = "type")
	public int getType() { return this.type; }
	
	public void setType(int type) { this.type = type; }
	
	@Basic
	@Column(name = "remark")
	public String getRemark() { return this.remark; }
	
	public void setRemark(String remark) { this.remark = remark; }
	
	@Basic
	@Column(name = "update_time")
	public Timestamp getUpdateTime() { return this.updateTime; }
	
	public void setUpdateTime(Timestamp updateTime) { this.updateTime = updateTime; }
	
	@Basic
	@Column(name = "create_time")
	public Timestamp getCreateTime() { return this.createTime; }
	
	public void setCreateTime(Timestamp createTime) { this.createTime = createTime; }
	
	@Basic
	@Column(name = "is_deleted")
	public int getIsDeleted() { return this.isDeleted; }
	
	public void setIsDeleted(int isDeleted) { this.isDeleted = isDeleted; }
	
	@Basic
	@Column(name = "object_guid")
	public String getObjectGuid() { return this.objectGuid; }
	
	public void setObjectGuid(String objectGuid) { this.objectGuid = objectGuid; }
	
	@Basic
	@Column(name = "temper")
	public String getTemper() { return this.temper; }
	
	public void setTemper(String temper) { this.temper = temper; }
	
	@Basic
	@Column(name = "temper_status")
	public int getTemperStatus() { return this.temperStatus; }
	
	public void setTemperStatus(int temperStatus) { this.temperStatus = temperStatus; }
	
	@Basic
	@Column(name = "person_from")
	public int getPersonFrom() { return this.personFrom; }
	
	public void setPersonFrom(int personFrom) { this.personFrom = personFrom; }
	
	@Basic
	@Column(name = "rdn")
	public String getRdn() { return this.rdn; }
	
	public void setRdn(String rdn) { this.rdn = rdn; }
	
	@Basic
	@Column(name = "dn")
	public String getDn() { return this.dn; }
	
	public void setDn(String dn) { this.dn = dn; }
	
	@Basic
	@Column(name = "usn_changed")
	public long getUsnChanged() { return this.usnChanged; }
	
	public void setUsnChanged(long usnChanged) { this.usnChanged = usnChanged; }
	
	@Basic
	@Column(name = "start_time_differ")
	public int getStartTimeDiffer() { return this.startTimeDiffer; }
	
	public void setStartTimeDiffer(int startTimeDiffer) { this.startTimeDiffer = startTimeDiffer; }
	
	@Basic
	@Column(name = "end_time_differ")
	public int getEndTimeDiffer() { return this.endTimeDiffer; }
	
	public void setEndTimeDiffer(int endTimeDiffer) { this.endTimeDiffer = endTimeDiffer; }
	
	public boolean equals(Object o) {
		if (this == o)
			return true; 
		if (o == null || getClass() != o.getClass())
			return false; 
		PersonEntity that = (PersonEntity)o;
		return (this.id == that.id && this.personGroupId == that.personGroupId && this.personType == that.personType && this.gender == that.gender && this.photoIndex == that.photoIndex && this.type == that.type && this.isDeleted == that.isDeleted && this.temperStatus == that.temperStatus && this.personFrom == that.personFrom && this.usnChanged == that.usnChanged && this.startTimeDiffer == that.startTimeDiffer && this.endTimeDiffer == that.endTimeDiffer && 










			
			Objects.equals(this.personCode, that.personCode) && 
			Objects.equals(this.givenName, that.givenName) && 
			Objects.equals(this.familyName, that.familyName) && 
			Objects.equals(this.fullName, that.fullName) && 
			Objects.equals(this.password, that.password) && 
			Objects.equals(this.email, that.email) && 
			Objects.equals(this.phoneNumber, that.phoneNumber) && 
			Objects.equals(this.photoUrl, that.photoUrl) && 
			Objects.equals(this.smallPhotoUrl, that.smallPhotoUrl) && 
			Objects.equals(this.startValidDate, that.startValidDate) && 
			Objects.equals(this.endValidDate, that.endValidDate) && 
			Objects.equals(this.remark, that.remark) && 
			Objects.equals(this.updateTime, that.updateTime) && 
			Objects.equals(this.createTime, that.createTime) && 
			Objects.equals(this.objectGuid, that.objectGuid) && 
			Objects.equals(this.temper, that.temper) && 
			Objects.equals(this.rdn, that.rdn) && 
			Objects.equals(this.dn, that.dn));
	}
	
	public int hashCode() { return Objects.hash(new Object[] { 
					Integer.valueOf(this.id), Integer.valueOf(this.personGroupId), Integer.valueOf(this.personType), this.personCode, this.givenName, this.familyName, this.fullName, Integer.valueOf(this.gender), this.password, this.email, 
					this.phoneNumber, this.photoUrl, Integer.valueOf(this.photoIndex), this.smallPhotoUrl, this.startValidDate, this.endValidDate, Integer.valueOf(this.type), this.remark, this.updateTime, this.createTime, 
					Integer.valueOf(this.isDeleted), this.objectGuid, this.temper, Integer.valueOf(this.temperStatus), Integer.valueOf(this.personFrom), this.rdn, this.dn, Long.valueOf(this.usnChanged), Integer.valueOf(this.startTimeDiffer), Integer.valueOf(this.endTimeDiffer) }); }
}
