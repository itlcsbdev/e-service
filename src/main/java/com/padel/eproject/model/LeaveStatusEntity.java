package com.padel.eproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "leave_status", schema = "eleave", catalog = "")
public class LeaveStatusEntity {
	private int id;
	
	@Id
	@Column(name = "id")
	public int getId() { return this.id; }
	
	public void setId(int id) { this.id = id; }
	
	public boolean equals(Object o) {
		if (this == o)
			return true; 
		if (o == null || getClass() != o.getClass())
			return false; 
		LeaveStatusEntity that = (LeaveStatusEntity)o;
		if (this.id != that.id)
			return false; 
		return true;
	}
	
	public int hashCode() { return this.id; }
}

