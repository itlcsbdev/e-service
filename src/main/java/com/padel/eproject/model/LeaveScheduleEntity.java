package com.padel.eproject.model;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "leave_schedule", schema = "eleave", catalog = "")
public class LeaveScheduleEntity {
    private int id;
    private String postId;
    private String position;
    private Integer type;
    private Integer level1;
    private Integer level2;
    private Integer level3;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "postID")
    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    @Basic
    @Column(name = "position")
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Basic
    @Column(name = "type")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Basic
    @Column(name = "level1")
    public Integer getLevel1() {
        return level1;
    }

    public void setLevel1(Integer level1) {
        this.level1 = level1;
    }

    @Basic
    @Column(name = "level2")
    public Integer getLevel2() {
        return level2;
    }

    public void setLevel2(Integer level2) {
        this.level2 = level2;
    }

    @Basic
    @Column(name = "level3")
    public Integer getLevel3() {
        return level3;
    }

    public void setLevel3(Integer level3) {
        this.level3 = level3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LeaveScheduleEntity that = (LeaveScheduleEntity) o;
        return id == that.id &&
                Objects.equals(postId, that.postId) &&
                Objects.equals(position, that.position) &&
                Objects.equals(type, that.type) &&
                Objects.equals(level1, that.level1) &&
                Objects.equals(level2, that.level2) &&
                Objects.equals(level3, that.level3);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, postId, position, type, level1, level2, level3);
    }
}
