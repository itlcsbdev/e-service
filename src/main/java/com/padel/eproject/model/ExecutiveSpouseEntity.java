package com.padel.eproject.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "executive_spouse", schema = "eleave", catalog = "")
public class ExecutiveSpouseEntity {
    private int id;
    private String namespouse;
    private String nokpspouse;
    private String spousework;
    private String staffid;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "namespouse")
    public String getNamespouse() {
        return namespouse;
    }

    public void setNamespouse(String namespouse) {
        this.namespouse = namespouse;
    }

    @Basic
    @Column(name = "nokpspouse")
    public String getNokpspouse() {
        return nokpspouse;
    }

    public void setNokpspouse(String nokpspouse) {
        this.nokpspouse = nokpspouse;
    }

    @Basic
    @Column(name = "spousework")
    public String getSpousework() {
        return spousework;
    }

    public void setSpousework(String spousework) {
        this.spousework = spousework;
    }

    @Basic
    @Column(name = "staffid")
    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExecutiveSpouseEntity that = (ExecutiveSpouseEntity) o;
        return id == that.id &&
                Objects.equals(namespouse, that.namespouse) &&
                Objects.equals(nokpspouse, that.nokpspouse) &&
                Objects.equals(spousework, that.spousework) &&
                Objects.equals(staffid, that.staffid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, namespouse, nokpspouse, spousework, staffid);
    }
}
