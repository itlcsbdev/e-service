package com.padel.eproject.model;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "staff_taxinfo", schema = "eleave", catalog = "")
public class StaffTaxinfoEntity {
    private int id;
    private String name;
    private String incomeno;
    private String ic2;
    private String kosong;
    private String anak;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "incomeno")
    public String getIncomeno() {
        return incomeno;
    }

    public void setIncomeno(String incomeno) {
        this.incomeno = incomeno;
    }

    @Basic
    @Column(name = "ic2")
    public String getIc2() {
        return ic2;
    }

    public void setIc2(String ic2) {
        this.ic2 = ic2;
    }

    @Basic
    @Column(name = "kosong")
    public String getKosong() {
        return kosong;
    }

    public void setKosong(String kosong) {
        this.kosong = kosong;
    }

    @Basic
    @Column(name = "anak")
    public String getAnak() {
        return anak;
    }

    public void setAnak(String anak) {
        this.anak = anak;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StaffTaxinfoEntity that = (StaffTaxinfoEntity) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(incomeno, that.incomeno) &&
                Objects.equals(ic2, that.ic2) &&
                Objects.equals(kosong, that.kosong) &&
                Objects.equals(anak, that.anak);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, incomeno, ic2, kosong, anak);
    }
}
