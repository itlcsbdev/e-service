package com.padel.eproject.model;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "staff_joined_tmb", schema = "eleave", catalog = "")
public class StaffJoinedTmbEntity {
    private int id;
    private String staffId;
    private String datejoined;
    private String name;
    private String position;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "staffId")
    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    @Basic
    @Column(name = "datejoined")
    public String getDatejoined() {
        return datejoined;
    }

    public void setDatejoined(String datejoined) {
        this.datejoined = datejoined;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "position")
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StaffJoinedTmbEntity that = (StaffJoinedTmbEntity) o;
        return id == that.id &&
                Objects.equals(staffId, that.staffId) &&
                Objects.equals(datejoined, that.datejoined) &&
                Objects.equals(name, that.name) &&
                Objects.equals(position, that.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, staffId, datejoined, name, position);
    }
}
