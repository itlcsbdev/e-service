package com.padel.eproject.model;

public class LeaveInfoWithBalance {
	private CoStaffEntity staffInfo;
	
	private double bal;
	
	private double balSickLeave;
	
	private double eligibleMonth;
	
	private LeaveInfoEntity leaveInfo;
	
	public void setStaffInfo(CoStaffEntity staffInfo) { this.staffInfo = staffInfo; }
	
	public void setBal(double bal) { this.bal = bal; }
	
	public void setBalSickLeave(double balSickLeave) { this.balSickLeave = balSickLeave; }
	
	public void setEligibleMonth(double eligibleMonth) { this.eligibleMonth = eligibleMonth; }
	
	public void setLeaveInfo(LeaveInfoEntity leaveInfo) { this.leaveInfo = leaveInfo; }
	
	public boolean equals(Object o) { if (o == this)
			return true; 
		if (!(o instanceof LeaveInfoWithBalance))
			return false; 
		LeaveInfoWithBalance other = (LeaveInfoWithBalance)o;
		if (!other.canEqual(this))
			return false; 
		Object this$staffInfo = getStaffInfo(), other$staffInfo = other.getStaffInfo();
		if ((this$staffInfo == null) ? (other$staffInfo != null) : !this$staffInfo.equals(other$staffInfo))
			return false; 
		if (Double.compare(getBal(), other.getBal()) != 0)
			return false; 
		if (Double.compare(getBalSickLeave(), other.getBalSickLeave()) != 0)
			return false; 
		if (Double.compare(getEligibleMonth(), other.getEligibleMonth()) != 0)
			return false; 
		Object this$leaveInfo = getLeaveInfo(), other$leaveInfo = other.getLeaveInfo();
		return !((this$leaveInfo == null) ? (other$leaveInfo != null) : !this$leaveInfo.equals(other$leaveInfo)); }
	
	protected boolean canEqual(Object other) { return other instanceof LeaveInfoWithBalance; }
	
//	public int hashCode() {
//		int PRIME = 59;
//		result = 1;
//		Object $staffInfo = getStaffInfo();
//		result = result * 59 + (($staffInfo == null) ? 43 : $staffInfo.hashCode());
//		long $bal = Double.doubleToLongBits(getBal());
//		result = result * 59 + (int)($bal >>> 32L ^ $bal);
//		long $balSickLeave = Double.doubleToLongBits(getBalSickLeave());
//		result = result * 59 + (int)($balSickLeave >>> 32L ^ $balSickLeave);
//		long $eligibleMonth = Double.doubleToLongBits(getEligibleMonth());
//		result = result * 59 + (int)($eligibleMonth >>> 32L ^ $eligibleMonth);
//		Object $leaveInfo = getLeaveInfo();
//		return result * 59 + (($leaveInfo == null) ? 43 : $leaveInfo.hashCode());
//	}
	
	public String toString() { return "LeaveInfoWithBalance(staffInfo=" + getStaffInfo() + ", bal=" + getBal() + ", balSickLeave=" + getBalSickLeave() + ", eligibleMonth=" + getEligibleMonth() + ", leaveInfo=" + getLeaveInfo() + ")"; }
	
	public CoStaffEntity getStaffInfo() { return this.staffInfo; }
	
	public double getBal() { return this.bal; }
	
	public double getBalSickLeave() { return this.balSickLeave; }
	
	public double getEligibleMonth() { return this.eligibleMonth; }
	
	public LeaveInfoEntity getLeaveInfo() { return this.leaveInfo; }
}
