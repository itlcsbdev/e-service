package com.padel.eproject.model;


public class TempData {
	private int slot_card_records_id;
	
	private int physical_id;
	
	private int person_id;
	
	private int person_btype;
	
	private int card_read_id;
	
	private String card_number;
	
	private int event_type;
	
	private String record_storage_key;
	
	private int auth_result;
	
	private String swip_card_time;
	
	private String update_time;
	
	private String create_time;
	
	private int is_face_terminal_event;
	
	private String is_deleted;
	
	private int is_cleared;
	
	private int is_allow_cleared;
	
	private String swip_card_rev_time;
	
	private int time_offset;
	
	private int attendance_status;
	
	private int source_type;
	
	private String snap_pic_url;
	
	private String temp_data;
	
	private int temp_status;
	
	private int mask_status;
	
	public void setSlot_card_records_id(int slot_card_records_id) { this.slot_card_records_id = slot_card_records_id; }
	
	public void setPhysical_id(int physical_id) { this.physical_id = physical_id; }
	
	public void setPerson_id(int person_id) { this.person_id = person_id; }
	
	public void setPerson_btype(int person_btype) { this.person_btype = person_btype; }
	
	public void setCard_read_id(int card_read_id) { this.card_read_id = card_read_id; }
	
	public void setCard_number(String card_number) { this.card_number = card_number; }
	
	public void setEvent_type(int event_type) { this.event_type = event_type; }
	
	public void setRecord_storage_key(String record_storage_key) { this.record_storage_key = record_storage_key; }
	
	public void setAuth_result(int auth_result) { this.auth_result = auth_result; }
	
	public void setSwip_card_time(String swip_card_time) { this.swip_card_time = swip_card_time; }
	
	public void setUpdate_time(String update_time) { this.update_time = update_time; }
	
	public void setCreate_time(String create_time) { this.create_time = create_time; }
	
	public void setIs_face_terminal_event(int is_face_terminal_event) { this.is_face_terminal_event = is_face_terminal_event; }
	
	public void setIs_deleted(String is_deleted) { this.is_deleted = is_deleted; }
	
	public void setIs_cleared(int is_cleared) { this.is_cleared = is_cleared; }
	
	public void setIs_allow_cleared(int is_allow_cleared) { this.is_allow_cleared = is_allow_cleared; }
	
	public void setSwip_card_rev_time(String swip_card_rev_time) { this.swip_card_rev_time = swip_card_rev_time; }
	
	public void setTime_offset(int time_offset) { this.time_offset = time_offset; }
	
	public void setAttendance_status(int attendance_status) { this.attendance_status = attendance_status; }
	
	public void setSource_type(int source_type) { this.source_type = source_type; }
	
	public void setSnap_pic_url(String snap_pic_url) { this.snap_pic_url = snap_pic_url; }
	
	public void setTemp_data(String temp_data) { this.temp_data = temp_data; }
	
	public void setTemp_status(int temp_status) { this.temp_status = temp_status; }
	
	public void setMask_status(int mask_status) { this.mask_status = mask_status; }
	
	public boolean equals(Object o) { if (o == this)
			return true; 
		if (!(o instanceof TempData))
			return false; 
		TempData other = (TempData)o;
		if (!other.canEqual(this))
			return false; 
		if (getSlot_card_records_id() != other.getSlot_card_records_id())
			return false; 
		if (getPhysical_id() != other.getPhysical_id())
			return false; 
		if (getPerson_id() != other.getPerson_id())
			return false; 
		if (getPerson_btype() != other.getPerson_btype())
			return false; 
		if (getCard_read_id() != other.getCard_read_id())
			return false; 
		Object this$card_number = getCard_number(), other$card_number = other.getCard_number();
		if ((this$card_number == null) ? (other$card_number != null) : !this$card_number.equals(other$card_number))
			return false; 
		if (getEvent_type() != other.getEvent_type())
			return false; 
		Object this$record_storage_key = getRecord_storage_key(), other$record_storage_key = other.getRecord_storage_key();
		if ((this$record_storage_key == null) ? (other$record_storage_key != null) : !this$record_storage_key.equals(other$record_storage_key))
			return false; 
		if (getAuth_result() != other.getAuth_result())
			return false; 
		Object this$swip_card_time = getSwip_card_time(), other$swip_card_time = other.getSwip_card_time();
		if ((this$swip_card_time == null) ? (other$swip_card_time != null) : !this$swip_card_time.equals(other$swip_card_time))
			return false; 
		Object this$update_time = getUpdate_time(), other$update_time = other.getUpdate_time();
		if ((this$update_time == null) ? (other$update_time != null) : !this$update_time.equals(other$update_time))
			return false; 
		Object this$create_time = getCreate_time(), other$create_time = other.getCreate_time();
		if ((this$create_time == null) ? (other$create_time != null) : !this$create_time.equals(other$create_time))
			return false; 
		if (getIs_face_terminal_event() != other.getIs_face_terminal_event())
			return false; 
		Object this$is_deleted = getIs_deleted(), other$is_deleted = other.getIs_deleted();
		if ((this$is_deleted == null) ? (other$is_deleted != null) : !this$is_deleted.equals(other$is_deleted))
			return false; 
		if (getIs_cleared() != other.getIs_cleared())
			return false; 
		if (getIs_allow_cleared() != other.getIs_allow_cleared())
			return false; 
		Object this$swip_card_rev_time = getSwip_card_rev_time(), other$swip_card_rev_time = other.getSwip_card_rev_time();
		if ((this$swip_card_rev_time == null) ? (other$swip_card_rev_time != null) : !this$swip_card_rev_time.equals(other$swip_card_rev_time))
			return false; 
		if (getTime_offset() != other.getTime_offset())
			return false; 
		if (getAttendance_status() != other.getAttendance_status())
			return false; 
		if (getSource_type() != other.getSource_type())
			return false; 
		Object this$snap_pic_url = getSnap_pic_url(), other$snap_pic_url = other.getSnap_pic_url();
		if ((this$snap_pic_url == null) ? (other$snap_pic_url != null) : !this$snap_pic_url.equals(other$snap_pic_url))
			return false; 
		Object this$temp_data = getTemp_data(), other$temp_data = other.getTemp_data();
		return ((this$temp_data == null) ? (other$temp_data != null) : !this$temp_data.equals(other$temp_data)) ? false : ((getTemp_status() != other.getTemp_status()) ? false : (!(getMask_status() != other.getMask_status()))); }
	
	protected boolean canEqual(Object other) { return other instanceof TempData; }
	
//	public int hashCode() {
//		int PRIME = 59;
//		result = 1;
//		result = result * 59 + getSlot_card_records_id();
//		result = result * 59 + getPhysical_id();
//		result = result * 59 + getPerson_id();
//		result = result * 59 + getPerson_btype();
//		result = result * 59 + getCard_read_id();
//		Object $card_number = getCard_number();
//		result = result * 59 + (($card_number == null) ? 43 : $card_number.hashCode());
//		result = result * 59 + getEvent_type();
//		Object $record_storage_key = getRecord_storage_key();
//		result = result * 59 + (($record_storage_key == null) ? 43 : $record_storage_key.hashCode());
//		result = result * 59 + getAuth_result();
//		Object $swip_card_time = getSwip_card_time();
//		result = result * 59 + (($swip_card_time == null) ? 43 : $swip_card_time.hashCode());
//		Object $update_time = getUpdate_time();
//		result = result * 59 + (($update_time == null) ? 43 : $update_time.hashCode());
//		Object $create_time = getCreate_time();
//		result = result * 59 + (($create_time == null) ? 43 : $create_time.hashCode());
//		result = result * 59 + getIs_face_terminal_event();
//		Object $is_deleted = getIs_deleted();
//		result = result * 59 + (($is_deleted == null) ? 43 : $is_deleted.hashCode());
//		result = result * 59 + getIs_cleared();
//		result = result * 59 + getIs_allow_cleared();
//		Object $swip_card_rev_time = getSwip_card_rev_time();
//		result = result * 59 + (($swip_card_rev_time == null) ? 43 : $swip_card_rev_time.hashCode());
//		result = result * 59 + getTime_offset();
//		result = result * 59 + getAttendance_status();
//		result = result * 59 + getSource_type();
//		Object $snap_pic_url = getSnap_pic_url();
//		result = result * 59 + (($snap_pic_url == null) ? 43 : $snap_pic_url.hashCode());
//		Object $temp_data = getTemp_data();
//		result = result * 59 + (($temp_data == null) ? 43 : $temp_data.hashCode());
//		result = result * 59 + getTemp_status();
//		return result * 59 + getMask_status();
//	}
	
	public String toString() { return "TempData(slot_card_records_id=" + getSlot_card_records_id() + ", physical_id=" + getPhysical_id() + ", person_id=" + getPerson_id() + ", person_btype=" + getPerson_btype() + ", card_read_id=" + getCard_read_id() + ", card_number=" + getCard_number() + ", event_type=" + getEvent_type() + ", record_storage_key=" + getRecord_storage_key() + ", auth_result=" + getAuth_result() + ", swip_card_time=" + getSwip_card_time() + ", update_time=" + getUpdate_time() + ", create_time=" + getCreate_time() + ", is_face_terminal_event=" + getIs_face_terminal_event() + ", is_deleted=" + getIs_deleted() + ", is_cleared=" + getIs_cleared() + ", is_allow_cleared=" + getIs_allow_cleared() + ", swip_card_rev_time=" + getSwip_card_rev_time() + ", time_offset=" + getTime_offset() + ", attendance_status=" + getAttendance_status() + ", source_type=" + getSource_type() + ", snap_pic_url=" + getSnap_pic_url() + ", temp_data=" + getTemp_data() + ", temp_status=" + getTemp_status() + ", mask_status=" + getMask_status() + ")"; }
	
	public int getSlot_card_records_id() { return this.slot_card_records_id; }
	
	public int getPhysical_id() { return this.physical_id; }
	
	public int getPerson_id() { return this.person_id; }
	
	public int getPerson_btype() { return this.person_btype; }
	
	public int getCard_read_id() { return this.card_read_id; }
	
	public String getCard_number() { return this.card_number; }
	
	public int getEvent_type() { return this.event_type; }
	
	public String getRecord_storage_key() { return this.record_storage_key; }
	
	public int getAuth_result() { return this.auth_result; }
	
	public String getSwip_card_time() { return this.swip_card_time; }
	
	public String getUpdate_time() { return this.update_time; }
	
	public String getCreate_time() { return this.create_time; }
	
	public int getIs_face_terminal_event() { return this.is_face_terminal_event; }
	
	public String getIs_deleted() { return this.is_deleted; }
	
	public int getIs_cleared() { return this.is_cleared; }
	
	public int getIs_allow_cleared() { return this.is_allow_cleared; }
	
	public String getSwip_card_rev_time() { return this.swip_card_rev_time; }
	
	public int getTime_offset() { return this.time_offset; }
	
	public int getAttendance_status() { return this.attendance_status; }
	
	public int getSource_type() { return this.source_type; }
	
	public String getSnap_pic_url() { return this.snap_pic_url; }
	
	public String getTemp_data() { return this.temp_data; }
	
	public int getTemp_status() { return this.temp_status; }
	
	public int getMask_status() { return this.mask_status; }
}
