package com.padel.eproject.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "leave_info", schema = "eleave", catalog = "")
public class LeaveInfoEntity {
    private int id;
    private String staffId;
    private Integer eligibleleave;
    private String year;
    private Integer bf;
    private Integer mc;
    private Integer hosp;
    private Integer maternity;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "staffID")
    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    @Basic
    @Column(name = "eligibleleave")
    public Integer getEligibleleave() {
        return eligibleleave;
    }

    public void setEligibleleave(Integer eligibleleave) {
        this.eligibleleave = eligibleleave;
    }

    @Basic
    @Column(name = "year")
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Basic
    @Column(name = "bf")
    public Integer getBf() {
        return bf;
    }

    public void setBf(Integer bf) {
        this.bf = bf;
    }

    @Basic
    @Column(name = "mc")
    public Integer getMc() {
        return mc;
    }

    public void setMc(Integer mc) {
        this.mc = mc;
    }

    @Basic
    @Column(name = "hosp")
    public Integer getHosp() {
        return hosp;
    }

    public void setHosp(Integer hosp) {
        this.hosp = hosp;
    }

    @Basic
    @Column(name = "maternity")
    public Integer getMaternity() {
        return maternity;
    }

    public void setMaternity(Integer maternity) {
        this.maternity = maternity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LeaveInfoEntity that = (LeaveInfoEntity) o;
        return id == that.id &&
                Objects.equals(staffId, that.staffId) &&
                Objects.equals(eligibleleave, that.eligibleleave) &&
                Objects.equals(year, that.year) &&
                Objects.equals(bf, that.bf) &&
                Objects.equals(mc, that.mc) &&
                Objects.equals(hosp, that.hosp) &&
                Objects.equals(maternity, that.maternity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, staffId, eligibleleave, year, bf, mc, hosp, maternity);
    }
}
