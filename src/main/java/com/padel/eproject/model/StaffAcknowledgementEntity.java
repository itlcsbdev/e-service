package com.padel.eproject.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "staff_acknowledgement", schema = "eleave", catalog = "")
public class StaffAcknowledgementEntity {
    private int id;
    private String staffId;
    private String dateack;
    private String timeack;
    private boolean latest;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "staffID")
    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    @Basic
    @Column(name = "dateack")
    public String getDateack() {
        return dateack;
    }

    public void setDateack(String dateack) {
        this.dateack = dateack;
    }

    @Basic
    @Column(name = "timeack")
    public String getTimeack() {
        return timeack;
    }

    public void setTimeack(String timeack) {
        this.timeack = timeack;
    }

    @Basic
    @Column(name = "latest")
    public boolean getLatest() {
        return latest;
    }

    public void setLatest(boolean latest) {
        this.latest = latest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StaffAcknowledgementEntity that = (StaffAcknowledgementEntity) o;
        return id == that.id &&
                Objects.equals(staffId, that.staffId) &&
                Objects.equals(dateack, that.dateack) &&
                Objects.equals(timeack, that.timeack) &&
                Objects.equals(latest, that.latest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, staffId, dateack, timeack, latest);
    }
}
