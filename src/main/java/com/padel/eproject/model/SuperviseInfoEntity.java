package com.padel.eproject.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "supervise_info", schema = "eleave", catalog = "")
public class SuperviseInfoEntity {
	private int id;
	
	private String staffId;
	
	private String supervisorId;
	
	private String headId;
	
	@Id
	@Column(name = "id")
	public int getId() { return this.id; }
	
	public void setId(int id) { this.id = id; }
	
	@Basic
	@Column(name = "staffID")
	public String getStaffId() { return this.staffId; }
	
	public void setStaffId(String staffId) { this.staffId = staffId; }
	
	@Basic
	@Column(name = "supervisorID")
	public String getSupervisorId() { return this.supervisorId; }
	
	public void setSupervisorId(String supervisorId) { this.supervisorId = supervisorId; }
	
	@Basic
	@Column(name = "headID")
	public String getHeadId() { return this.headId; }
	
	public void setHeadId(String headId) { this.headId = headId; }
	
	public boolean equals(Object o) {
		if (this == o)
			return true; 
		if (o == null || getClass() != o.getClass())
			return false; 
		SuperviseInfoEntity that = (SuperviseInfoEntity)o;
		if (this.id != that.id)
			return false; 
		if ((this.staffId != null) ? !this.staffId.equals(that.staffId) : (that.staffId != null))
			return false; 
		if ((this.supervisorId != null) ? !this.supervisorId.equals(that.supervisorId) : (that.supervisorId != null))
			return false; 
		if ((this.headId != null) ? !this.headId.equals(that.headId) : (that.headId != null))
			return false; 
		return true;
	}
	
	public int hashCode() {
		int result = this.id;
		result = 31 * result + ((this.staffId != null) ? this.staffId.hashCode() : 0);
		result = 31 * result + ((this.supervisorId != null) ? this.supervisorId.hashCode() : 0);
		result = 31 * result + ((this.headId != null) ? this.headId.hashCode() : 0);
		return result;
	}
}
