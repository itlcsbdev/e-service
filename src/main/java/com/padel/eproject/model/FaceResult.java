package com.padel.eproject.model;

import com.padel.eproject.model.TempData;

public class FaceResult {
	private String table;
	
	private String action;
	
	private TempData data;
	
	public void setTable(String table) { this.table = table; }
	
	public void setAction(String action) { this.action = action; }
	
	public void setData(TempData data) { this.data = data; }
	
	public boolean equals(Object o) { if (o == this)
			return true; 
		if (!(o instanceof FaceResult))
			return false; 
		FaceResult other = (FaceResult)o;
		if (!other.canEqual(this))
			return false; 
		Object this$table = getTable(), other$table = other.getTable();
		if ((this$table == null) ? (other$table != null) : !this$table.equals(other$table))
			return false; 
		Object this$action = getAction(), other$action = other.getAction();
		if ((this$action == null) ? (other$action != null) : !this$action.equals(other$action))
			return false; 
		Object this$data = getData(), other$data = other.getData();
		return !((this$data == null) ? (other$data != null) : !this$data.equals(other$data)); }
	
	protected boolean canEqual(Object other) { return other instanceof FaceResult; }
	
//	public int hashCode() {
//		int PRIME = 59;
//		result = 1;
//		Object $table = getTable();
//		result = result * 59 + (($table == null) ? 43 : $table.hashCode());
//		Object $action = getAction();
//		result = result * 59 + (($action == null) ? 43 : $action.hashCode());
//		Object $data = getData();
//		return result * 59 + (($data == null) ? 43 : $data.hashCode());
//	}
	
	public String toString() { return "FaceResult(table=" + getTable() + ", action=" + getAction() + ", data=" + getData() + ")"; }
	
	public String getTable() { return this.table; }
	
	public String getAction() { return this.action; }
	
	public TempData getData() { return this.data; }
}
