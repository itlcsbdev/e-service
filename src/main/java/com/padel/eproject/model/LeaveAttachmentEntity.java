package com.padel.eproject.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "leave_attachment", schema = "eleave", catalog = "")
public class LeaveAttachmentEntity {
	private int id;
	
	private String leaveId;
	
	private String filename;
	
	private String staffId;
	
	private String sessionId;
	
	@Id
	@Column(name = "id")
	public int getId() { return this.id; }
	
	public void setId(int id) { this.id = id; }
	
	@Basic
	@Column(name = "leaveID")
	public String getLeaveId() { return this.leaveId; }
	
	public void setLeaveId(String leaveId) { this.leaveId = leaveId; }
	
	@Basic
	@Column(name = "filename")
	public String getFilename() { return this.filename; }
	
	public void setFilename(String filename) { this.filename = filename; }
	
	@Basic
	@Column(name = "staffID")
	public String getStaffId() { return this.staffId; }
	
	public void setStaffId(String staffId) { this.staffId = staffId; }
	
	@Basic
	@Column(name = "sessionID")
	public String getSessionId() { return this.sessionId; }
	
	public void setSessionId(String sessionId) { this.sessionId = sessionId; }
	
	public boolean equals(Object o) {
		if (this == o)
			return true; 
		if (o == null || getClass() != o.getClass())
			return false; 
		LeaveAttachmentEntity that = (LeaveAttachmentEntity)o;
		if (this.id != that.id)
			return false; 
		if ((this.leaveId != null) ? !this.leaveId.equals(that.leaveId) : (that.leaveId != null))
			return false; 
		if ((this.filename != null) ? !this.filename.equals(that.filename) : (that.filename != null))
			return false; 
		if ((this.staffId != null) ? !this.staffId.equals(that.staffId) : (that.staffId != null))
			return false; 
		if ((this.sessionId != null) ? !this.sessionId.equals(that.sessionId) : (that.sessionId != null))
			return false; 
		return true;
	}
	
	public int hashCode() {
		int result = this.id;
		result = 31 * result + ((this.leaveId != null) ? this.leaveId.hashCode() : 0);
		result = 31 * result + ((this.filename != null) ? this.filename.hashCode() : 0);
		result = 31 * result + ((this.staffId != null) ? this.staffId.hashCode() : 0);
		result = 31 * result + ((this.sessionId != null) ? this.sessionId.hashCode() : 0);
		return result;
	}
}
