package com.padel.eproject.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "executive", schema = "eleave", catalog = "")
public class ExecutiveEntity {
    private int id;
    private String dob;
    private String nokp;
    private String birthplace;
    private String marital;
    private String blood;
    private String department;
    private String address;
    private String fixaddress;
    private String race;
    private String religion;
    private String taxno;
    private String noepf;
    private String nosocso;
    private String position;
    private String workdate;
    private String spouse;
    private String nokpspouse;
    private String spousework;
    private String namawaris1;
    private String hubunganwaris1;
    private String notelwaris1;
    private String alamatwaris1;
    private String namawaris2;
    private String hubunganwaris2;
    private String notelwaris2;
    private String alamatwaris2;
    private Integer bilanak;
    private Integer anakover18;
    private Integer anakbelow18;
    private Integer anakipt18;
    private Integer anakoku;
    private Integer anakokuipt;
    private String staffid;
    private String educationlevel;
    private String bank;
    private String noakaun;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "dob")
    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    @Basic
    @Column(name = "nokp")
    public String getNokp() {
        return nokp;
    }

    public void setNokp(String nokp) {
        this.nokp = nokp;
    }

    @Basic
    @Column(name = "birthplace")
    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    @Basic
    @Column(name = "marital")
    public String getMarital() {
        return marital;
    }

    public void setMarital(String marital) {
        this.marital = marital;
    }

    @Basic
    @Column(name = "blood")
    public String getBlood() {
        return blood;
    }

    public void setBlood(String blood) {
        this.blood = blood;
    }

    @Basic
    @Column(name = "department")
    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Basic
    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "fixaddress")
    public String getFixaddress() {
        return fixaddress;
    }

    public void setFixaddress(String fixaddress) {
        this.fixaddress = fixaddress;
    }

    @Basic
    @Column(name = "race")
    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    @Basic
    @Column(name = "religion")
    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    @Basic
    @Column(name = "taxno")
    public String getTaxno() {
        return taxno;
    }

    public void setTaxno(String taxno) {
        this.taxno = taxno;
    }

    @Basic
    @Column(name = "noepf")
    public String getNoepf() {
        return noepf;
    }

    public void setNoepf(String noepf) {
        this.noepf = noepf;
    }

    @Basic
    @Column(name = "nosocso")
    public String getNosocso() {
        return nosocso;
    }

    public void setNosocso(String nosocso) {
        this.nosocso = nosocso;
    }

    @Basic
    @Column(name = "position")
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Basic
    @Column(name = "workdate")
    public String getWorkdate() {
        return workdate;
    }

    public void setWorkdate(String workdate) {
        this.workdate = workdate;
    }

    @Basic
    @Column(name = "spouse")
    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    @Basic
    @Column(name = "nokpspouse")
    public String getNokpspouse() {
        return nokpspouse;
    }

    public void setNokpspouse(String nokpspouse) {
        this.nokpspouse = nokpspouse;
    }

    @Basic
    @Column(name = "spousework")
    public String getSpousework() {
        return spousework;
    }

    public void setSpousework(String spousework) {
        this.spousework = spousework;
    }

    @Basic
    @Column(name = "namawaris1")
    public String getNamawaris1() {
        return namawaris1;
    }

    public void setNamawaris1(String namawaris1) {
        this.namawaris1 = namawaris1;
    }

    @Basic
    @Column(name = "hubunganwaris1")
    public String getHubunganwaris1() {
        return hubunganwaris1;
    }

    public void setHubunganwaris1(String hubunganwaris1) {
        this.hubunganwaris1 = hubunganwaris1;
    }

    @Basic
    @Column(name = "notelwaris1")
    public String getNotelwaris1() {
        return notelwaris1;
    }

    public void setNotelwaris1(String notelwaris1) {
        this.notelwaris1 = notelwaris1;
    }

    @Basic
    @Column(name = "alamatwaris1")
    public String getAlamatwaris1() {
        return alamatwaris1;
    }

    public void setAlamatwaris1(String alamatwaris1) {
        this.alamatwaris1 = alamatwaris1;
    }

    @Basic
    @Column(name = "namawaris2")
    public String getNamawaris2() {
        return namawaris2;
    }

    public void setNamawaris2(String namawaris2) {
        this.namawaris2 = namawaris2;
    }

    @Basic
    @Column(name = "hubunganwaris2")
    public String getHubunganwaris2() {
        return hubunganwaris2;
    }

    public void setHubunganwaris2(String hubunganwaris2) {
        this.hubunganwaris2 = hubunganwaris2;
    }

    @Basic
    @Column(name = "notelwaris2")
    public String getNotelwaris2() {
        return notelwaris2;
    }

    public void setNotelwaris2(String notelwaris2) {
        this.notelwaris2 = notelwaris2;
    }

    @Basic
    @Column(name = "alamatwaris2")
    public String getAlamatwaris2() {
        return alamatwaris2;
    }

    public void setAlamatwaris2(String alamatwaris2) {
        this.alamatwaris2 = alamatwaris2;
    }

    @Basic
    @Column(name = "bilanak")
    public Integer getBilanak() {
        return bilanak;
    }

    public void setBilanak(Integer bilanak) {
        this.bilanak = bilanak;
    }

    @Basic
    @Column(name = "anakover18")
    public Integer getAnakover18() {
        return anakover18;
    }

    public void setAnakover18(Integer anakover18) {
        this.anakover18 = anakover18;
    }

    @Basic
    @Column(name = "anakbelow18")
    public Integer getAnakbelow18() {
        return anakbelow18;
    }

    public void setAnakbelow18(Integer anakbelow18) {
        this.anakbelow18 = anakbelow18;
    }

    @Basic
    @Column(name = "anakipt18")
    public Integer getAnakipt18() {
        return anakipt18;
    }

    public void setAnakipt18(Integer anakipt18) {
        this.anakipt18 = anakipt18;
    }

    @Basic
    @Column(name = "anakoku")
    public Integer getAnakoku() {
        return anakoku;
    }

    public void setAnakoku(Integer anakoku) {
        this.anakoku = anakoku;
    }

    @Basic
    @Column(name = "anakokuipt")
    public Integer getAnakokuipt() {
        return anakokuipt;
    }

    public void setAnakokuipt(Integer anakokuipt) {
        this.anakokuipt = anakokuipt;
    }

    @Basic
    @Column(name = "staffid")
    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    @Basic
    @Column(name = "educationlevel")
    public String getEducationlevel() {
        return educationlevel;
    }

    public void setEducationlevel(String educationlevel) {
        this.educationlevel = educationlevel;
    }

    @Basic
    @Column(name = "bank")
    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    @Basic
    @Column(name = "noakaun")
    public String getNoakaun() {
        return noakaun;
    }

    public void setNoakaun(String noakaun) {
        this.noakaun = noakaun;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExecutiveEntity that = (ExecutiveEntity) o;
        return id == that.id &&
                Objects.equals(dob, that.dob) &&
                Objects.equals(nokp, that.nokp) &&
                Objects.equals(birthplace, that.birthplace) &&
                Objects.equals(marital, that.marital) &&
                Objects.equals(blood, that.blood) &&
                Objects.equals(department, that.department) &&
                Objects.equals(address, that.address) &&
                Objects.equals(fixaddress, that.fixaddress) &&
                Objects.equals(race, that.race) &&
                Objects.equals(religion, that.religion) &&
                Objects.equals(taxno, that.taxno) &&
                Objects.equals(noepf, that.noepf) &&
                Objects.equals(nosocso, that.nosocso) &&
                Objects.equals(position, that.position) &&
                Objects.equals(workdate, that.workdate) &&
                Objects.equals(spouse, that.spouse) &&
                Objects.equals(nokpspouse, that.nokpspouse) &&
                Objects.equals(spousework, that.spousework) &&
                Objects.equals(namawaris1, that.namawaris1) &&
                Objects.equals(hubunganwaris1, that.hubunganwaris1) &&
                Objects.equals(notelwaris1, that.notelwaris1) &&
                Objects.equals(alamatwaris1, that.alamatwaris1) &&
                Objects.equals(namawaris2, that.namawaris2) &&
                Objects.equals(hubunganwaris2, that.hubunganwaris2) &&
                Objects.equals(notelwaris2, that.notelwaris2) &&
                Objects.equals(alamatwaris2, that.alamatwaris2) &&
                Objects.equals(bilanak, that.bilanak) &&
                Objects.equals(anakover18, that.anakover18) &&
                Objects.equals(anakbelow18, that.anakbelow18) &&
                Objects.equals(anakipt18, that.anakipt18) &&
                Objects.equals(anakoku, that.anakoku) &&
                Objects.equals(anakokuipt, that.anakokuipt) &&
                Objects.equals(staffid, that.staffid) &&
                Objects.equals(educationlevel, that.educationlevel) &&
                Objects.equals(bank, that.bank) &&
                Objects.equals(noakaun, that.noakaun);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dob, nokp, birthplace, marital, blood, department, address, fixaddress, race, religion, taxno, noepf, nosocso, position, workdate, spouse, nokpspouse, spousework, namawaris1, hubunganwaris1, notelwaris1, alamatwaris1, namawaris2, hubunganwaris2, notelwaris2, alamatwaris2, bilanak, anakover18, anakbelow18, anakipt18, anakoku, anakokuipt, staffid, educationlevel, bank, noakaun);
    }
}
