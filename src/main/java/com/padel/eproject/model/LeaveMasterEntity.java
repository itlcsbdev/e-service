package com.padel.eproject.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "leave_master", schema = "eleave", catalog = "")
public class LeaveMasterEntity {
	private String leaveId;
	
	private String staffId;
	
	private String type;
	
	private String reason;
	
	private Integer days;
	
	private String dateapply;
	
	private String datestart;
	
	private String dateend;
	
	private String supervisorId;
	
	private String supervisorDate;
	
	private String headId;
	
	private String headDate;
	
	private String hrId;
	
	private String hrDate;
	
	private String checkId;
	
	private String checkDate;
	
	private String status;
	
	private String year;
	
	private String period;
	
	private Integer stafflevel;
	
	@Id
	@Column(name = "leaveID")
	public String getLeaveId() { return this.leaveId; }
	
	public void setLeaveId(String leaveId) { this.leaveId = leaveId; }
	
	@Basic
	@Column(name = "staffID")
	public String getStaffId() { return this.staffId; }
	
	public void setStaffId(String staffId) { this.staffId = staffId; }
	
	@Basic
	@Column(name = "type")
	public String getType() { return this.type; }
	
	public void setType(String type) { this.type = type; }
	
	@Basic
	@Column(name = "reason")
	public String getReason() { return this.reason; }
	
	public void setReason(String reason) { this.reason = reason; }
	
	@Basic
	@Column(name = "days")
	public Integer getDays() { return this.days; }
	
	public void setDays(Integer days) { this.days = days; }
	
	@Basic
	@Column(name = "dateapply")
	public String getDateapply() { return this.dateapply; }
	
	public void setDateapply(String dateapply) { this.dateapply = dateapply; }
	
	@Basic
	@Column(name = "datestart")
	public String getDatestart() { return this.datestart; }
	
	public void setDatestart(String datestart) { this.datestart = datestart; }
	
	@Basic
	@Column(name = "dateend")
	public String getDateend() { return this.dateend; }
	
	public void setDateend(String dateend) { this.dateend = dateend; }
	
	@Basic
	@Column(name = "supervisorID")
	public String getSupervisorId() { return this.supervisorId; }
	
	public void setSupervisorId(String supervisorId) { this.supervisorId = supervisorId; }
	
	@Basic
	@Column(name = "supervisor_date")
	public String getSupervisorDate() { return this.supervisorDate; }
	
	public void setSupervisorDate(String supervisorDate) { this.supervisorDate = supervisorDate; }
	
	@Basic
	@Column(name = "headID")
	public String getHeadId() { return this.headId; }
	
	public void setHeadId(String headId) { this.headId = headId; }
	
	@Basic
	@Column(name = "head_date")
	public String getHeadDate() { return this.headDate; }
	
	public void setHeadDate(String headDate) { this.headDate = headDate; }
	
	@Basic
	@Column(name = "hrID")
	public String getHrId() { return this.hrId; }
	
	public void setHrId(String hrId) { this.hrId = hrId; }
	
	@Basic
	@Column(name = "hr_date")
	public String getHrDate() { return this.hrDate; }
	
	public void setHrDate(String hrDate) { this.hrDate = hrDate; }
	
	@Basic
	@Column(name = "checkID")
	public String getCheckId() { return this.checkId; }
	
	public void setCheckId(String checkId) { this.checkId = checkId; }
	
	@Basic
	@Column(name = "check_date")
	public String getCheckDate() { return this.checkDate; }
	
	public void setCheckDate(String checkDate) { this.checkDate = checkDate; }
	
	@Basic
	@Column(name = "status")
	public String getStatus() { return this.status; }
	
	public void setStatus(String status) { this.status = status; }
	
	@Basic
	@Column(name = "year")
	public String getYear() { return this.year; }
	
	public void setYear(String year) { this.year = year; }
	
	@Basic
	@Column(name = "period")
	public String getPeriod() { return this.period; }
	
	public void setPeriod(String period) { this.period = period; }
	
	@Basic
	@Column(name = "stafflevel")
	public Integer getStafflevel() { return this.stafflevel; }
	
	public void setStafflevel(Integer stafflevel) { this.stafflevel = stafflevel; }
	
	public boolean equals(Object o) {
		if (this == o)
			return true; 
		if (o == null || getClass() != o.getClass())
			return false; 
		LeaveMasterEntity that = (LeaveMasterEntity)o;
		if ((this.leaveId != null) ? !this.leaveId.equals(that.leaveId) : (that.leaveId != null))
			return false; 
		if ((this.staffId != null) ? !this.staffId.equals(that.staffId) : (that.staffId != null))
			return false; 
		if ((this.type != null) ? !this.type.equals(that.type) : (that.type != null))
			return false; 
		if ((this.reason != null) ? !this.reason.equals(that.reason) : (that.reason != null))
			return false; 
		if ((this.days != null) ? !this.days.equals(that.days) : (that.days != null))
			return false; 
		if ((this.dateapply != null) ? !this.dateapply.equals(that.dateapply) : (that.dateapply != null))
			return false; 
		if ((this.datestart != null) ? !this.datestart.equals(that.datestart) : (that.datestart != null))
			return false; 
		if ((this.dateend != null) ? !this.dateend.equals(that.dateend) : (that.dateend != null))
			return false; 
		if ((this.supervisorId != null) ? !this.supervisorId.equals(that.supervisorId) : (that.supervisorId != null))
			return false; 
		if ((this.supervisorDate != null) ? !this.supervisorDate.equals(that.supervisorDate) : (that.supervisorDate != null))
			return false; 
		if ((this.headId != null) ? !this.headId.equals(that.headId) : (that.headId != null))
			return false; 
		if ((this.headDate != null) ? !this.headDate.equals(that.headDate) : (that.headDate != null))
			return false; 
		if ((this.hrId != null) ? !this.hrId.equals(that.hrId) : (that.hrId != null))
			return false; 
		if ((this.hrDate != null) ? !this.hrDate.equals(that.hrDate) : (that.hrDate != null))
			return false; 
		if ((this.checkId != null) ? !this.checkId.equals(that.checkId) : (that.checkId != null))
			return false; 
		if ((this.checkDate != null) ? !this.checkDate.equals(that.checkDate) : (that.checkDate != null))
			return false; 
		if ((this.status != null) ? !this.status.equals(that.status) : (that.status != null))
			return false; 
		if ((this.year != null) ? !this.year.equals(that.year) : (that.year != null))
			return false; 
		if ((this.period != null) ? !this.period.equals(that.period) : (that.period != null))
			return false; 
		if ((this.stafflevel != null) ? !this.stafflevel.equals(that.stafflevel) : (that.stafflevel != null))
			return false; 
		return true;
	}
	
	public int hashCode() {
		int result = (this.leaveId != null) ? this.leaveId.hashCode() : 0;
		result = 31 * result + ((this.staffId != null) ? this.staffId.hashCode() : 0);
		result = 31 * result + ((this.type != null) ? this.type.hashCode() : 0);
		result = 31 * result + ((this.reason != null) ? this.reason.hashCode() : 0);
		result = 31 * result + ((this.days != null) ? this.days.hashCode() : 0);
		result = 31 * result + ((this.dateapply != null) ? this.dateapply.hashCode() : 0);
		result = 31 * result + ((this.datestart != null) ? this.datestart.hashCode() : 0);
		result = 31 * result + ((this.dateend != null) ? this.dateend.hashCode() : 0);
		result = 31 * result + ((this.supervisorId != null) ? this.supervisorId.hashCode() : 0);
		result = 31 * result + ((this.supervisorDate != null) ? this.supervisorDate.hashCode() : 0);
		result = 31 * result + ((this.headId != null) ? this.headId.hashCode() : 0);
		result = 31 * result + ((this.headDate != null) ? this.headDate.hashCode() : 0);
		result = 31 * result + ((this.hrId != null) ? this.hrId.hashCode() : 0);
		result = 31 * result + ((this.hrDate != null) ? this.hrDate.hashCode() : 0);
		result = 31 * result + ((this.checkId != null) ? this.checkId.hashCode() : 0);
		result = 31 * result + ((this.checkDate != null) ? this.checkDate.hashCode() : 0);
		result = 31 * result + ((this.status != null) ? this.status.hashCode() : 0);
		result = 31 * result + ((this.year != null) ? this.year.hashCode() : 0);
		result = 31 * result + ((this.period != null) ? this.period.hashCode() : 0);
		result = 31 * result + ((this.stafflevel != null) ? this.stafflevel.hashCode() : 0);
		return result;
	}
}
