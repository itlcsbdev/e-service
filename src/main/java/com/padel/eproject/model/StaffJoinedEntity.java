package com.padel.eproject.model;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "staff_joined", schema = "eleave", catalog = "")
public class StaffJoinedEntity {
    private int id;
    private String staffid;
    private String datejoined;
    private String ic;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "staffid")
    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    @Basic
    @Column(name = "datejoined")
    public String getDatejoined() {
        return datejoined;
    }

    public void setDatejoined(String datejoined) {
        this.datejoined = datejoined;
    }

    @Basic
    @Column(name = "IC")
    public String getIc() {
        return ic;
    }

    public void setIc(String ic) {
        this.ic = ic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StaffJoinedEntity that = (StaffJoinedEntity) o;
        return id == that.id &&
                Objects.equals(staffid, that.staffid) &&
                Objects.equals(datejoined, that.datejoined) &&
                Objects.equals(ic, that.ic);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, staffid, datejoined, ic);
    }
}

