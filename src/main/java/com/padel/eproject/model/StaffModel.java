package com.padel.eproject.model;

import com.padel.eproject.model.CoStaffEntity;

public class StaffModel {
	private int userLevel;
	
	private CoStaffEntity coStaff;
	
	public void setUserLevel(int userLevel) { this.userLevel = userLevel; }
	
	public void setCoStaff(CoStaffEntity coStaff) { this.coStaff = coStaff; }
	
	public boolean equals(Object o) { if (o == this)
			return true; 
		if (!(o instanceof StaffModel))
			return false; 
		StaffModel other = (StaffModel)o;
		if (!other.canEqual(this))
			return false; 
		if (getUserLevel() != other.getUserLevel())
			return false; 
		Object this$coStaff = getCoStaff(), other$coStaff = other.getCoStaff();
		return !((this$coStaff == null) ? (other$coStaff != null) : !this$coStaff.equals(other$coStaff)); }
	
	protected boolean canEqual(Object other) { return other instanceof StaffModel; }
	
//	public int hashCode() {
//		int PRIME = 59;
//		result = 1;
//		result = result * 59 + getUserLevel();
//		Object $coStaff = getCoStaff();
//		return result * 59 + (($coStaff == null) ? 43 : $coStaff.hashCode());
//	}
	
	public String toString() { return "StaffModel(userLevel=" + getUserLevel() + ", coStaff=" + getCoStaff() + ")"; }
	
	public int getUserLevel() { return this.userLevel; }
	
	public CoStaffEntity getCoStaff() { return this.coStaff; }
}

