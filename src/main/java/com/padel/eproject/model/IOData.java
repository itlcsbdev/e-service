package com.padel.eproject.model;

public class IOData {
	private String date;
	
	private String timein;
	
	private String timout;
	
	private String duration;
	
	private String colorduration;
	
	private String colortimein;
	
	private String colortimout;
	
	private boolean completeHour;
	
	private boolean lateIn;
	
	private boolean earlyOut;
	
	public void setDate(String date) { this.date = date; }
	
	public void setTimein(String timein) { this.timein = timein; }
	
	public void setTimout(String timout) { this.timout = timout; }
	
	public void setDuration(String duration) { this.duration = duration; }
	
	public void setColorduration(String colorduration) { this.colorduration = colorduration; }
	
	public void setColortimein(String colortimein) { this.colortimein = colortimein; }
	
	public void setColortimout(String colortimout) { this.colortimout = colortimout; }
	
	public void setCompleteHour(boolean completeHour) { this.completeHour = completeHour; }
	
	public void setLateIn(boolean lateIn) { this.lateIn = lateIn; }
	
	public void setEarlyOut(boolean earlyOut) { this.earlyOut = earlyOut; }
	
	public boolean equals(Object o) { if (o == this)
			return true; 
		if (!(o instanceof IOData))
			return false; 
		IOData other = (IOData)o;
		if (!other.canEqual(this))
			return false; 
		Object this$date = getDate(), other$date = other.getDate();
		if ((this$date == null) ? (other$date != null) : !this$date.equals(other$date))
			return false; 
		Object this$timein = getTimein(), other$timein = other.getTimein();
		if ((this$timein == null) ? (other$timein != null) : !this$timein.equals(other$timein))
			return false; 
		Object this$timout = getTimout(), other$timout = other.getTimout();
		if ((this$timout == null) ? (other$timout != null) : !this$timout.equals(other$timout))
			return false; 
		Object this$duration = getDuration(), other$duration = other.getDuration();
		if ((this$duration == null) ? (other$duration != null) : !this$duration.equals(other$duration))
			return false; 
		Object this$colorduration = getColorduration(), other$colorduration = other.getColorduration();
		if ((this$colorduration == null) ? (other$colorduration != null) : !this$colorduration.equals(other$colorduration))
			return false; 
		Object this$colortimein = getColortimein(), other$colortimein = other.getColortimein();
		if ((this$colortimein == null) ? (other$colortimein != null) : !this$colortimein.equals(other$colortimein))
			return false; 
		Object this$colortimout = getColortimout(), other$colortimout = other.getColortimout();
		return ((this$colortimout == null) ? (other$colortimout != null) : !this$colortimout.equals(other$colortimout)) ? false : ((isCompleteHour() != other.isCompleteHour()) ? false : ((isLateIn() != other.isLateIn()) ? false : (!(isEarlyOut() != other.isEarlyOut())))); }
	
	protected boolean canEqual(Object other) { return other instanceof IOData; }
	
//	public int hashCode() {
//		int PRIME = 59;
//		result = 1;
//		Object $date = getDate();
//		result = result * 59 + (($date == null) ? 43 : $date.hashCode());
//		Object $timein = getTimein();
//		result = result * 59 + (($timein == null) ? 43 : $timein.hashCode());
//		Object $timout = getTimout();
//		result = result * 59 + (($timout == null) ? 43 : $timout.hashCode());
//		Object $duration = getDuration();
//		result = result * 59 + (($duration == null) ? 43 : $duration.hashCode());
//		Object $colorduration = getColorduration();
//		result = result * 59 + (($colorduration == null) ? 43 : $colorduration.hashCode());
//		Object $colortimein = getColortimein();
//		result = result * 59 + (($colortimein == null) ? 43 : $colortimein.hashCode());
//		Object $colortimout = getColortimout();
//		result = result * 59 + (($colortimout == null) ? 43 : $colortimout.hashCode());
//		result = result * 59 + (isCompleteHour() ? 79 : 97);
//		result = result * 59 + (isLateIn() ? 79 : 97);
//		return result * 59 + (isEarlyOut() ? 79 : 97);
//	}
	
	public String toString() { return "IOData(date=" + getDate() + ", timein=" + getTimein() + ", timout=" + getTimout() + ", duration=" + getDuration() + ", colorduration=" + getColorduration() + ", colortimein=" + getColortimein() + ", colortimout=" + getColortimout() + ", completeHour=" + isCompleteHour() + ", lateIn=" + isLateIn() + ", earlyOut=" + isEarlyOut() + ")"; }
	
	public String getDate() { return this.date; }
	
	public String getTimein() { return this.timein; }
	
	public String getTimout() { return this.timout; }
	
	public String getDuration() { return this.duration; }
	
	public String getColorduration() { return this.colorduration; }
	
	public String getColortimein() { return this.colortimein; }
	
	public String getColortimout() { return this.colortimout; }
	
	public boolean isCompleteHour() { return this.completeHour; }
	
	public boolean isLateIn() { return this.lateIn; }
	
	public boolean isEarlyOut() { return this.earlyOut; }
}
