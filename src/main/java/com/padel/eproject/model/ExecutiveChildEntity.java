package com.padel.eproject.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "executive_child", schema = "eleave", catalog = "")
public class ExecutiveChildEntity {
    private int id;
    private String staffid;
    private String namaanak;
    private String nokpanak;
    private Integer umur;
    private String oku;
    private String educationlevel;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "staffid")
    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    @Basic
    @Column(name = "namaanak")
    public String getNamaanak() {
        return namaanak;
    }

    public void setNamaanak(String namaanak) {
        this.namaanak = namaanak;
    }

    @Basic
    @Column(name = "nokpanak")
    public String getNokpanak() {
        return nokpanak;
    }

    public void setNokpanak(String nokpanak) {
        this.nokpanak = nokpanak;
    }

    @Basic
    @Column(name = "umur")
    public Integer getUmur() {
        return umur;
    }

    public void setUmur(Integer umur) {
        this.umur = umur;
    }

    @Basic
    @Column(name = "oku")
    public String getOku() {
        return oku;
    }

    public void setOku(String oku) {
        this.oku = oku;
    }

    @Basic
    @Column(name = "educationlevel")
    public String getEducationlevel() {
        return educationlevel;
    }

    public void setEducationlevel(String educationlevel) {
        this.educationlevel = educationlevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExecutiveChildEntity that = (ExecutiveChildEntity) o;
        return id == that.id &&
                Objects.equals(staffid, that.staffid) &&
                Objects.equals(namaanak, that.namaanak) &&
                Objects.equals(nokpanak, that.nokpanak) &&
                Objects.equals(umur, that.umur) &&
                Objects.equals(oku, that.oku) &&
                Objects.equals(educationlevel, that.educationlevel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, staffid, namaanak, nokpanak, umur, oku, educationlevel);
    }
}
