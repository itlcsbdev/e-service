package com.padel.eproject.model;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "leave_info_update", schema = "eleave", catalog = "")
public class LeaveInfoUpdateEntity {
    private int id;
    private String staffId;
    private String year;
    private String datechanged;
    private Integer oldbal;
    private Integer newbal;
    private Integer yearof;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "staffID")
    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    @Basic
    @Column(name = "year")
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Basic
    @Column(name = "datechanged")
    public String getDatechanged() {
        return datechanged;
    }

    public void setDatechanged(String datechanged) {
        this.datechanged = datechanged;
    }

    @Basic
    @Column(name = "oldbal")
    public Integer getOldbal() {
        return oldbal;
    }

    public void setOldbal(Integer oldbal) {
        this.oldbal = oldbal;
    }

    @Basic
    @Column(name = "newbal")
    public Integer getNewbal() {
        return newbal;
    }

    public void setNewbal(Integer newbal) {
        this.newbal = newbal;
    }

    @Basic
    @Column(name = "yearof")
    public Integer getYearof() {
        return yearof;
    }

    public void setYearof(Integer yearof) {
        this.yearof = yearof;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LeaveInfoUpdateEntity that = (LeaveInfoUpdateEntity) o;
        return id == that.id &&
                Objects.equals(staffId, that.staffId) &&
                Objects.equals(year, that.year) &&
                Objects.equals(datechanged, that.datechanged) &&
                Objects.equals(oldbal, that.oldbal) &&
                Objects.equals(newbal, that.newbal) &&
                Objects.equals(yearof, that.yearof);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, staffId, year, datechanged, oldbal, newbal, yearof);
    }
}
