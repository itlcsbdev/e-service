package com.padel.eproject.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "leave_comment", schema = "eleave", catalog = "")
public class LeaveCommentEntity {
	private int id;
	
	private String leaveId;
	
	private String comment;
	
	private String date;
	
	private String time;
	
	private String staffId;
	
	@Id
	@Column(name = "id")
	public int getId() { return this.id; }
	
	public void setId(int id) { this.id = id; }
	
	@Basic
	@Column(name = "leaveID")
	public String getLeaveId() { return this.leaveId; }
	
	public void setLeaveId(String leaveId) { this.leaveId = leaveId; }
	
	@Basic
	@Column(name = "comment")
	public String getComment() { return this.comment; }
	
	public void setComment(String comment) { this.comment = comment; }
	
	@Basic
	@Column(name = "date")
	public String getDate() { return this.date; }
	
	public void setDate(String date) { this.date = date; }
	
	@Basic
	@Column(name = "time")
	public String getTime() { return this.time; }
	
	public void setTime(String time) { this.time = time; }
	
	@Basic
	@Column(name = "staffID")
	public String getStaffId() { return this.staffId; }
	
	public void setStaffId(String staffId) { this.staffId = staffId; }
	
	public boolean equals(Object o) {
		if (this == o)
			return true; 
		if (o == null || getClass() != o.getClass())
			return false; 
		LeaveCommentEntity that = (LeaveCommentEntity)o;
		if (this.id != that.id)
			return false; 
		if ((this.leaveId != null) ? !this.leaveId.equals(that.leaveId) : (that.leaveId != null))
			return false; 
		if ((this.comment != null) ? !this.comment.equals(that.comment) : (that.comment != null))
			return false; 
		if ((this.date != null) ? !this.date.equals(that.date) : (that.date != null))
			return false; 
		if ((this.time != null) ? !this.time.equals(that.time) : (that.time != null))
			return false; 
		if ((this.staffId != null) ? !this.staffId.equals(that.staffId) : (that.staffId != null))
			return false; 
		return true;
	}
	
	public int hashCode() {
		int result = this.id;
		result = 31 * result + ((this.leaveId != null) ? this.leaveId.hashCode() : 0);
		result = 31 * result + ((this.comment != null) ? this.comment.hashCode() : 0);
		result = 31 * result + ((this.date != null) ? this.date.hashCode() : 0);
		result = 31 * result + ((this.time != null) ? this.time.hashCode() : 0);
		result = 31 * result + ((this.staffId != null) ? this.staffId.hashCode() : 0);
		return result;
	}
}
