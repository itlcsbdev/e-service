package com.padel.eproject.model;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "co_staff", schema = "eleave", catalog = "")
public class CoStaffEntity {
    private int id;
    private String staffid;
    private String title;
    private String name;
    private String status;
    private String ic;
    private String birth;
    private String sex;
    private String marital;
    private String religion;
    private String citizen;
    private String address;
    private String city;
    private String state;
    private String postcode;
    private String phone;
    private String hp;
    private String fax;
    private String email;
    private String pposition;
    private String remarks;
    private String race;
    private String flag;
    private String category;
    private String workertype;
    private String oldic;
    private String datejoin;
    private String pobirth;
    private String department;
    private String position;
    private String location;
    private String imageUrl;
    private String departmentId;
    private String locId;
    private String postId;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "staffid")
    public String getStaffid() {
        return staffid;
    }

    public void setStaffid(String staffid) {
        this.staffid = staffid;
    }

    @Basic
    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "ic")
    public String getIc() {
        return ic;
    }

    public void setIc(String ic) {
        this.ic = ic;
    }

    @Basic
    @Column(name = "birth")
    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    @Basic
    @Column(name = "sex")
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "marital")
    public String getMarital() {
        return marital;
    }

    public void setMarital(String marital) {
        this.marital = marital;
    }

    @Basic
    @Column(name = "religion")
    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    @Basic
    @Column(name = "citizen")
    public String getCitizen() {
        return citizen;
    }

    public void setCitizen(String citizen) {
        this.citizen = citizen;
    }

    @Basic
    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "state")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Basic
    @Column(name = "postcode")
    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    @Basic
    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "hp")
    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    @Basic
    @Column(name = "fax")
    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "pposition")
    public String getPposition() {
        return pposition;
    }

    public void setPposition(String pposition) {
        this.pposition = pposition;
    }

    @Basic
    @Column(name = "remarks")
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Basic
    @Column(name = "race")
    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    @Basic
    @Column(name = "flag")
    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Basic
    @Column(name = "category")
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Basic
    @Column(name = "workertype")
    public String getWorkertype() {
        return workertype;
    }

    public void setWorkertype(String workertype) {
        this.workertype = workertype;
    }

    @Basic
    @Column(name = "oldic")
    public String getOldic() {
        return oldic;
    }

    public void setOldic(String oldic) {
        this.oldic = oldic;
    }

    @Basic
    @Column(name = "datejoin")
    public String getDatejoin() {
        return datejoin;
    }

    public void setDatejoin(String datejoin) {
        this.datejoin = datejoin;
    }

    @Basic
    @Column(name = "pobirth")
    public String getPobirth() {
        return pobirth;
    }

    public void setPobirth(String pobirth) {
        this.pobirth = pobirth;
    }

    @Basic
    @Column(name = "department")
    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Basic
    @Column(name = "position")
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Basic
    @Column(name = "location")
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Basic
    @Column(name = "imageURL")
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Basic
    @Column(name = "departmentID")
    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    @Basic
    @Column(name = "locID")
    public String getLocId() {
        return locId;
    }

    public void setLocId(String locId) {
        this.locId = locId;
    }

    @Basic
    @Column(name = "postID")
    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CoStaffEntity that = (CoStaffEntity) o;
        return id == that.id &&
                Objects.equals(staffid, that.staffid) &&
                Objects.equals(title, that.title) &&
                Objects.equals(name, that.name) &&
                Objects.equals(status, that.status) &&
                Objects.equals(ic, that.ic) &&
                Objects.equals(birth, that.birth) &&
                Objects.equals(sex, that.sex) &&
                Objects.equals(marital, that.marital) &&
                Objects.equals(religion, that.religion) &&
                Objects.equals(citizen, that.citizen) &&
                Objects.equals(address, that.address) &&
                Objects.equals(city, that.city) &&
                Objects.equals(state, that.state) &&
                Objects.equals(postcode, that.postcode) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(hp, that.hp) &&
                Objects.equals(fax, that.fax) &&
                Objects.equals(email, that.email) &&
                Objects.equals(pposition, that.pposition) &&
                Objects.equals(remarks, that.remarks) &&
                Objects.equals(race, that.race) &&
                Objects.equals(flag, that.flag) &&
                Objects.equals(category, that.category) &&
                Objects.equals(workertype, that.workertype) &&
                Objects.equals(oldic, that.oldic) &&
                Objects.equals(datejoin, that.datejoin) &&
                Objects.equals(pobirth, that.pobirth) &&
                Objects.equals(department, that.department) &&
                Objects.equals(position, that.position) &&
                Objects.equals(location, that.location) &&
                Objects.equals(imageUrl, that.imageUrl) &&
                Objects.equals(departmentId, that.departmentId) &&
                Objects.equals(locId, that.locId) &&
                Objects.equals(postId, that.postId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, staffid, title, name, status, ic, birth, sex, marital, religion, citizen, address, city, state, postcode, phone, hp, fax, email, pposition, remarks, race, flag, category, workertype, oldic, datejoin, pobirth, department, position, location, imageUrl, departmentId, locId, postId);
    }
}


