package com.padel.eproject.serviceImpl;

import com.padel.eproject.repository.LeaveInfoRepository;
import com.padel.eproject.repository.LeaveRepository;
import com.padel.eproject.service.LeaveInfoService;
import com.padel.eproject.service.LeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.padel.eproject.model.LeaveInfoEntity;

@Service
public class LeaveInfoServiceImpl implements LeaveInfoService {
	@Autowired
	private LeaveInfoRepository repository;
	
	@Autowired
	private LeaveRepository repositoryLeaveMaster;
	
	@Autowired
	private LeaveService serviceLeaveMaster;
	
	public LeaveInfoEntity findByStaffId(String code) { return this.repository.findByStaffId(code); }
	
	public double getBalanceEligibleLeave(String code, String year) {
		double cnt = 0.0D;
		LeaveInfoEntity l = this.repository.findByStaffIdAndYear(code, year);
		if(l != null){

			int bal = 0;


			bal = (int) this.repositoryLeaveMaster.getTotalLeaveOfTheYear(code, year);

			cnt = (l.getEligibleleave().intValue() + l.getBf().intValue()) - bal;
		}

		return cnt;
	}
	
	public LeaveInfoEntity findByStaffIdAndYear(String code, String year) { return this.repository.findByStaffIdAndYear(code, year); }
	
	public double getEligibleLeaveForTheMonth(String code, String year, String date) {
		double cnt = 0.0D;
		LeaveInfoEntity l = this.repository.findByStaffIdAndYear(code, year);
		double CL = l.getEligibleleave().intValue();
		double curMonth = 3.0D;
		double beforeAddCF = CL / 12.0D * curMonth;
		double afterAddCF = beforeAddCF + l.getBf().intValue();
		double plusLeaveUse = afterAddCF - this.repositoryLeaveMaster.getTotalLeaveOfTheYear(code, year);
		cnt = plusLeaveUse;
		return cnt;
	}
	
	public void updateNewBalance(int bal, String staffId, String year) { this.repository.updateNewBalance(bal, staffId, year); }
	
}
