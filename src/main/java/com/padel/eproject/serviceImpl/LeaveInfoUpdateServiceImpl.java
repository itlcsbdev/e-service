package com.padel.eproject.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.padel.eproject.repository.LeaveInfoUpdateRepository;
import com.padel.eproject.service.LeaveInfoUpdateService;

@Service
public class LeaveInfoUpdateServiceImpl  implements LeaveInfoUpdateService {

	@Autowired
	private LeaveInfoUpdateRepository repository;
	
	@Override
	public void insertTransactionBalance(String paramString1, String paramString2, String paramString3, int oldbal, int newbal, int yearof) {
		// TODO Auto-generated method stub
		 this.repository.insertTransactionBalance(paramString1, paramString2, paramString3, oldbal, newbal, yearof); 
		
	}

	@Override
	public void insertNewLeaveOfNewYear(String staffId,String name, String year, int eligibleleavenew, int eligibleleaveold, int bf, int halfofeligible, String datejoined, int yearservice, int balprev, int mc) {
		// TODO Auto-generated method stub
		this.repository.insertNewLeaveOfNewYear(staffId, name,  year,  eligibleleavenew,  eligibleleaveold,  bf,  halfofeligible,  datejoined,  yearservice,  balprev,  mc);

	}

	@Override
	public void insertNewLeaveOfNewYearLeaveInfo(String staffId,int eligibleleave, String year, int bf, int mc, int hosp, int maternity) {
		// TODO Auto-generated method stub
		this.repository.insertNewLeaveOfNewYearLeaveInfo(staffId, eligibleleave, year,  bf,  mc,  hosp,  maternity);

	}

}
