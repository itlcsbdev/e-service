package com.padel.eproject.serviceImpl;

import com.padel.eproject.model.CoStaffEntity;
import com.padel.eproject.model.StaffModel;
import com.padel.eproject.repository.StaffRepository;
import com.padel.eproject.service.StaffService;
import com.padel.eproject.service.UserAccessService;
import com.padel.eproject.serviceImpl.StaffServiceImpl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StaffServiceImpl implements StaffService {
	@Autowired
	private StaffRepository repository;
	
	@Autowired
	private UserAccessService serviceUserAccess;
	
	public List<CoStaffEntity> findAll() { return this.repository.findAll(); }
	
	public CoStaffEntity findByStaffid(String code) { return this.repository.findByStaffid(code); }
	
	public CoStaffEntity findByEmail(String code) { return this.repository.findByEmail(code); }
	
	public StaffModel getStaffInfoWithUserAccess(String code) {
		StaffModel sm = new StaffModel();
		sm.setCoStaff(this.repository.findByStaffid(code));
		sm.setUserLevel(this.serviceUserAccess.findByStaffId(code).getLevel());
		return sm;
	}
}
