package com.padel.eproject.serviceImpl;

import com.padel.eproject.model.SuperviseInfoEntity;
import com.padel.eproject.repository.SuperviseRepository;
import com.padel.eproject.service.SuperviseService;
import com.padel.eproject.serviceImpl.SuperviseServiceImpl;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SuperviseServiceImpl implements SuperviseService {
	@Autowired
	private SuperviseRepository repositorySupervise;
	
	public List<String> getSuperviseeIDs(String code, int level) {
		List<String> listA = new ArrayList<>();
		if (level == 2) {
			listA = this.repositorySupervise.getSuperviseeIDforSupervisor(code);
		} else if (level == 3) {
			listA = this.repositorySupervise.getSuperviseeIDforHead(code);
		} 
		return listA;
	}
	
	public SuperviseInfoEntity getSuperViseInfo(String code) { return this.repositorySupervise.getSuperViseInfo(code); }
}
