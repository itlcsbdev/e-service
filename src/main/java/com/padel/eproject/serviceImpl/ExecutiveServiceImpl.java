package com.padel.eproject.serviceImpl;

import com.padel.eproject.model.ExecutiveEntity;
import com.padel.eproject.model.ExecutiveSpouseEntity;
import com.padel.eproject.repository.ExecutiveRepository;
import com.padel.eproject.repository.ExecutiveSpouseRepository;
import com.padel.eproject.service.ExecutiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExecutiveServiceImpl implements ExecutiveService {

    @Autowired
    private ExecutiveRepository repository;

    @Autowired
    private ExecutiveSpouseRepository spouseRepository;

    public ExecutiveEntity findByStaffid(String paramString) { return this.repository.findByStaffid(paramString); }

    public ExecutiveSpouseEntity findSpouseByStaffid(String paramString) { return this.spouseRepository.findByStaffid(paramString); }

}
