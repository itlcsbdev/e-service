package com.padel.eproject.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.padel.eproject.model.LeaveScheduleEntity;
import com.padel.eproject.repository.LeaveScheduleRepository;
import com.padel.eproject.service.LeaveScheduleService;

@Service
public class LeaveScheduleServiceImpl implements LeaveScheduleService {
	
	@Autowired
	private LeaveScheduleRepository repository;
	
	public List<LeaveScheduleEntity> findAll() { return this.repository.findAll(); }
	
	public LeaveScheduleEntity findByPostId(String code) { return this.repository.findByPostId(code); }

}

