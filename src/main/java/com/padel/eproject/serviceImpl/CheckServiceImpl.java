package com.padel.eproject.serviceImpl;

import com.padel.eproject.model.CheckmasterEntity;
import com.padel.eproject.model.CoStaffEntity;
import com.padel.eproject.repository.CheckRepository;
import com.padel.eproject.service.CheckService;
import com.padel.eproject.serviceImpl.CheckServiceImpl;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

@Service
@Configurable
public class CheckServiceImpl implements CheckService {
	private static final Logger log = LoggerFactory.getLogger(CheckServiceImpl.class);
	
	@Autowired
	private CheckService checkService;
	
	@Autowired
	private CheckRepository repository;

	public List<CoStaffEntity> getAllStaff() { return this.repository.getAllStaff(); }
	
	public List<CoStaffEntity> getAllHQStaff() { return this.repository.getAllHQStaff(); }
	
	public List<CoStaffEntity> getAllStaffNonHQ() { return this.repository.getAllStaffNonHQ(); }
	
	
	public CheckmasterEntity getCheckInfo(String staffId, String date) { return this.repository.getCheckInfo(staffId, date); }
	
	public String getStaffID(int id) { return this.repository.getStaffID(id); }
	
	public void update(double temp, String status, String time, String date, String staffId) { this.repository.update(temp, status, time, date, staffId); }
	
	public CheckmasterEntity create(CheckmasterEntity cm) { return this.repository.save(cm); }
	
	public String getMaxID() { return this.repository.getMaxID(); }
}
