package com.padel.eproject.serviceImpl;
import com.padel.eproject.model.CheckformEntity;
import com.padel.eproject.repository.CheckFormRepository;
import com.padel.eproject.service.CheckInfoService;
import com.padel.eproject.serviceImpl.CheckFormServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CheckFormServiceImpl implements CheckInfoService {
	@Autowired
	private CheckFormRepository repository;
	
	public CheckformEntity getCheckFormInfo(String checkId) { return this.repository.getCheckFormInfo(checkId); }
	
	public CheckformEntity create(CheckformEntity cf) { return this.repository.save(cf); }
}
