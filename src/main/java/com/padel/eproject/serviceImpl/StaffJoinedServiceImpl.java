package com.padel.eproject.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.padel.eproject.model.StaffJoinedEntity;
import com.padel.eproject.repository.StaffJoinedRepository;
import com.padel.eproject.service.StaffJoinedService;

@Service
public class StaffJoinedServiceImpl implements StaffJoinedService {
	
	@Autowired
	private StaffJoinedRepository repository;
	
	public List<StaffJoinedEntity> findAll() { return this.repository.findAll(); }

}
