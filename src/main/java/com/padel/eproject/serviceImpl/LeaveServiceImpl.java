package com.padel.eproject.serviceImpl;

import com.padel.eproject.model.LeaveMasterEntity;
import com.padel.eproject.repository.LeaveRepository;
import com.padel.eproject.service.LeaveService;
import com.padel.eproject.service.SuperviseService;
import com.padel.eproject.service.UserAccessService;
import com.padel.eproject.serviceImpl.LeaveServiceImpl;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LeaveServiceImpl implements LeaveService {
	@Autowired
	private LeaveRepository repository;
	
	@Autowired
	private SuperviseService superviseService;
	
	@Autowired
	private UserAccessService uaService;
	
	public List<LeaveMasterEntity> findAll() { return this.repository.findAll(); }
	
	public LeaveMasterEntity findByLeaveId(String code) { return this.repository.findByLeaveId(code); }
	
	public List<LeaveMasterEntity> findByStaffId(String code) { return this.repository.findByStaffId(code); }
	
	public List<LeaveMasterEntity> findLeaveNotApproved(String staffId) { return this.repository.findLeaveNotApproved(staffId); }
	
	public List<LeaveMasterEntity> getNewLeave(String staffId) { return this.repository.getNewLeave(staffId); }
	
	public List<LeaveMasterEntity> getLeaveWithStatus(String staffId, String status) { return this.repository.getLeaveWithStatus(staffId, status); }
	
	public double getTotalLeaveOfTheYear(String staffId, String year) { return this.repository.getTotalLeaveOfTheYear(staffId, year); }
	
	public double getTotalSickLeaveUseOfTheYear(String staffId, String year) { return this.repository.getTotalSickLeaveUseOfTheYear(staffId, year); }

	public double getTotalHospLeaveUseOfTheYear(String staffId, String year) { return this.repository.getTotalHospLeaveUseOfTheYear(staffId, year); }
	
	public List<LeaveMasterEntity> getAllLeaveExcludeYou(String staffId) {
		List<LeaveMasterEntity> lm = new ArrayList<>();
		List<String> sv = this.superviseService.getSuperviseeIDs(staffId, this.uaService.findByStaffId(staffId).getLevel());
		for (int j = 0; j < sv.size(); j++) {
			System.out.print(sv.get(j));
			lm.addAll(this.repository.getAllLeaveExcludeYou(staffId, sv.get(j)));
		} 
		return lm;
	}
	
	public List<LeaveMasterEntity> getUnapproveLeave() { return this.repository.getUnapprovedLeave(); }
	
	public List<LeaveMasterEntity> getTodaysLeave(String date) { return this.repository.getTodaysLeave(date); }

	public double getTotalLeaveUseOfTheYear(String staffId, String year) { return this.repository.getTotalLeaveUseOfTheYear(staffId, year); }
}
