package com.padel.eproject.serviceImpl;

import com.padel.eproject.model.UserAccessEntity;
import com.padel.eproject.repository.UserAccessRepository;
import com.padel.eproject.service.UserAccessService;
import com.padel.eproject.serviceImpl.UserAccessServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserAccessServiceImpl implements UserAccessService {
	@Autowired
	private UserAccessRepository repository;
	
	public UserAccessEntity findByStaffId(String code) { return this.repository.findByStaffId(code); }
}
