package com.padel.eproject.serviceImpl;

import com.padel.eproject.model.LeaveTypeEntity;
import com.padel.eproject.repository.LeaveTypeRepository;
import com.padel.eproject.service.LeaveTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LeaveTypeServiceImpl implements LeaveTypeService{

    @Autowired
    private LeaveTypeRepository leaveTypeRepository;

    public LeaveTypeEntity getLeaveTypeData(String code) { return this.leaveTypeRepository.findByCode(code); }
}
