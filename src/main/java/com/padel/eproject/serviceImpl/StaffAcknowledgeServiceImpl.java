package com.padel.eproject.serviceImpl;

import com.padel.eproject.model.StaffAcknowledgementEntity;
import com.padel.eproject.repository.StaffAcknowledgeRepository;
import com.padel.eproject.service.StaffAcknowledgementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StaffAcknowledgeServiceImpl implements StaffAcknowledgementService {

    @Autowired
    private StaffAcknowledgeRepository ackRepo;


    @Override
    public boolean isLatestAcknowledge(String id) {
        boolean c = false;
        if(ackRepo.findByStaffIdAndLatestIsTrue(id) == null){
            c = false;
        }else{
            c = true;
        }

        return c;
    }
}
